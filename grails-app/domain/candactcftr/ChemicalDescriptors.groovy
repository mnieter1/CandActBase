package candactcftr

class ChemicalDescriptors {
    // this class / table will contain the descriptor values for the chemical compounds in the Compound domain model
    // these can be anything deemed describing the compound as a chemical entity as far as coordinates in a mapping of chemical space like a pca dimension used for depicting differences in the data sets; set appart from this should be the biological descriptors which would reference the specific activity in a specific assay condition, those are context specific and not soley describing the structure <- purpose of these columns here is the similarity definition between compound structures
    static belongsTo = Compound
    Double pca1
    Double pca2
    Double pca3
    
    static constraints = {
        // no entry does have to be provided by default; even though it should be in order to allow the viszualisation of the chemical space to work
        pca1(nullable: true)
        pca2(nullable: true)
        pca3(nullable: true)
    
    }
}
