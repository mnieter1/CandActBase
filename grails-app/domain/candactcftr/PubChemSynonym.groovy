package candactcftr

class PubChemSynonym {
    
    static belongsTo = PubChemRecord
    
    String synonym
    
    static mapping = {
		synonym type:'text'
		
	}
    
    static constraints = {
        synonym nullable: false
    
    }
}
