package candactcftr

class CompoundSynonym {

    static belongsTo = Compound
    
    String synonym
    
    static mapping = {
		synonym type:'text'
		
	}
    
    static constraints = {
        synonym nullable: false
    
    }
    
	
}
