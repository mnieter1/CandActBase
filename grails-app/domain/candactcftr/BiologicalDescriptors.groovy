package candactcftr

class BiologicalDescriptors {
    // this class / table will contain the biological values / aktivity tags for the chemical compounds in the Compound domain model
    // these can be anything deemed describing the compound as a biological entity as far as activity groups; set appart from this should be the biological descriptors which would reference the specific activity in a specific assay condition, those are context specific and not soley describing the structure <- purpose of these columns here is the similarity definition between compound structures
    static belongsTo = Compound
    
    String cftrRelevance
    //Category
    String influenceOnCftrFunction
    String orderOfInteraction
    String subcellularCompartment
    
    
    static constraints = {
        cftrRelevance(nullable: true)
        influenceOnCftrFunction(nullable: true)
        orderOfInteraction(nullable: true)
        subcellularCompartment(nullable: true)
    }
}
