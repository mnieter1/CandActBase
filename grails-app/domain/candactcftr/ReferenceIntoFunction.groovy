package candactcftr

class ReferenceIntoFunction {

    String substanceReferenceIntoFunction

    static belongsTo = CitationReference // temporary changing to broad referencing just paper not Compound
    static hasMany = [citationReferences: CitationReference, compounds: Compound]
    
	
	
    static constraints = {
	
    compounds(nullable: true)
    citationReferences(nullable: true)
    substanceReferenceIntoFunction(nullable: false)
    substanceReferenceIntoFunction unique: true
	}
	static mapping = {
		substanceReferenceIntoFunction type:'text'
	}
    
}
