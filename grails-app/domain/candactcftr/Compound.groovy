package candactcftr

class Compound {

	int id
	PubChemRecord pubChem
	String smiles
	String inChI
	String inChIKey
	
    static hasMany = [citationReferences: CitationReference, compoundSynonym: CompoundSynonym, chemicalDescriptors: ChemicalDescriptors, biologicalDescriptors: BiologicalDescriptors]
	
	
	
    static constraints = {
	
    compoundSynonym nullable: true
	id nullable: false
	smiles unique: true
    inChI(nullable: true)
	inChIKey(nullable: true)
	pubChem(nullable: true)
	citationReferences(nullable: true)
	chemicalDescriptors(nullable: true)
	biologicalDescriptors(nullable: true)
	}
	static mapping = {
		smiles type:'text'
	}
	
}
