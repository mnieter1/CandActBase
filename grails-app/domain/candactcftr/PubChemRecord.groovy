package candactcftr

class PubChemRecord {

    int cid //external bei PubChem
    static hasMany = [synonyms: PubChemSynonym]
    static belongsTo = Compound
    
    
    static constraints = {
        synonyms nullable: true
        cid nullable: false
        cid unique: false
    }
}
