package candactcftr

class Author {
    static hasMany = [citationReferences: CitationReference]
    static belongsTo = CitationReference
    
    String family
    String given
    String droppingParticle
    String nonDroppingParticle
    String suffix
    String commaSuffix
    String staticOrdering
    String literal
    String parseNames
    
   
    
    static constraints = {
        
        citationReferences(nullable: true)
        family(nullable: true)
        given(nullable: true)
        droppingParticle(nullable: true)
        nonDroppingParticle(nullable: true)
        suffix(nullable: true)
        commaSuffix(nullable: true)
        staticOrdering(nullable: true)
        literal(nullable: true)
        parseNames(nullable: true)
    }
}
