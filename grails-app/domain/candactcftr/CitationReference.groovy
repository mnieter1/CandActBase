package candactcftr

class CitationReference {
// implementation of the CSL citation style language as groovy domain classes (CitationReference and Author) based on the following json reference
// date 20170831
//    "description": "JSON schema (draft 3) for CSL input data",
//    "id": "https://github.com/citation-style-language/schema/raw/master/csl-data.json",
    String refType // type
    String categories
    String refLanguage
    String journalAbbreviation
    String shortTitle
    
    //static hasMany = [authors:Author] //has many author
    static belongsTo = Compound

    
    // since this implementation is done for a chemisty based database
    // we will add a compounds linking entry here
    // the owner is a compound as the base root
    static hasMany = [compounds:Compound, authors:Author, rifs:ReferenceIntoFunction] //has many author
    
    String collectionEditor // has many   collection-editor
    String composer // has many
    String containerAuthor // container-author
    String director
    String editor
    String editorialDirector // editorial-director
    String interviewer
    String illustrator
    String originalAuthor // original-author
    String recipient
    String reviewedAuthor // reviewed-author
    String translator
    
    String accessed //date
    String container //date
    String eventDate //date
    String issued //date
    String originalDate //date
    String submitted //date
    
    String refAbstract // abstract
    String annote
    String archive
    String archiveLocation // archive_location
    String archivePlace // archive_place
    
    
    String authority
    String callNumber // call-number
    String chapterNumber // chapter-number
    String citationNumber // citation-number
    String citationLabel // citation-label
    String collectionNumber // collection-number
    String collectionTitle // collection-title
    String containerTitle // container-title
    String containerTitleShort // container-title-short
    String dimensions
    String doi
    String edition
    String event
    String eventPlace // event-place
    String firstReferenceNoteNumber // first-reference-note-number
    String genre
    String isbn
    String issn
    String issue
    String jurisdiction
    String keyword
    String locator
    String medium
    String note
    String refNumber
    String numberOfPages // number-of-pages
    String numberOfVolumes // number-of-volumes
    String originalPublisher // original-publisher
    String originalPublisherPlace // original-publisher-place
    String originalTitle // original-title
    String pageString
    String pageFirst // page-first
    String pmcid //PMCID
    String pmid // PMID
    String publisher
    String publisherPlace // publisher-place
    String refReferences
    String reviewedTitle // reviewed-title
    String refScale
    String section
    String refSource
    String refStatus
    String refTitle // title
    String titleShort // title-short
    String refURL// URL
    String version
    String volume
    String yearSuffix // year-suffix
    
    
    
    static mapping = {
		refAbstract type:'text'
		refTitle type:'text'
	}
    
    
    static constraints = {
        
        compounds(nullable: true)
        authors(nullable: true)
        
        
        
    refType(nullable: true) // type
    categories(nullable: true)
    refLanguage(nullable: true)
    journalAbbreviation(nullable: true)
    shortTitle(nullable: true)
    
    collectionEditor(nullable: true) // has many   collection-editor
    composer(nullable: true) // has many
    containerAuthor(nullable: true) // container-author
    director(nullable: true)
    editor(nullable: true)
    editorialDirector(nullable: true) // editorial-director
    interviewer(nullable: true)
    illustrator(nullable: true)
    originalAuthor(nullable: true) // original-author
    recipient(nullable: true)
    reviewedAuthor(nullable: true) // reviewed-author
    translator(nullable: true)
    
    accessed(nullable: true) //date
    container(nullable: true) //date
    eventDate(nullable: true) //date
    issued(nullable: true) //date
    originalDate(nullable: true) //date
    submitted(nullable: true) //date
    
    refAbstract(nullable: true) // abstract
    annote(nullable: true)
    archive(nullable: true)
    archiveLocation(nullable: true) // archive_location
    archivePlace(nullable: true) // archive_place
    
    
    authority(nullable: true)
    callNumber(nullable: true) // call-number
    chapterNumber(nullable: true) // chapter-number
    citationNumber(nullable: true) // citation-number
    citationLabel(nullable: true) // citation-label
    collectionNumber(nullable: true) // collection-number
    collectionTitle(nullable: true) // collection-title
    containerTitle(nullable: true) // container-title
    containerTitleShort(nullable: true) // container-title-short
    dimensions(nullable: true)
    doi(nullable: true)
    edition(nullable: true)
    event(nullable: true)
    eventPlace(nullable: true) // event-place
    firstReferenceNoteNumber(nullable: true) // first-reference-note-number
    genre(nullable: true)
    isbn(nullable: true)
    issn(nullable: true)
    issue(nullable: true)
    jurisdiction(nullable: true)
    keyword(nullable: true)
    locator(nullable: true)
    medium(nullable: true)
    note(nullable: true)
    refNumber(nullable: true)
    numberOfPages(nullable: true) // number-of-pages
    numberOfVolumes(nullable: true) // number-of-volumes
    originalPublisher(nullable: true) // original-publisher
    originalPublisherPlace(nullable: true) // original-publisher-place
    originalTitle(nullable: true) // original-title
    pageString(nullable: true)
    pageFirst(nullable: true) // page-first
    pmcid(nullable: true)
    pmid(nullable: true)
    publisher(nullable: true)
    publisherPlace(nullable: true) // publisher-place
    refReferences(nullable: true)
    reviewedTitle(nullable: true) // reviewed-title
    refScale(nullable: true)
    section(nullable: true)
    refSource(nullable: true)
    refStatus(nullable: true)
    refTitle(nullable: true) // title
    titleShort(nullable: true) // title-short
    refURL(nullable: true)// URL
    version(nullable: true)
    volume(nullable: true)
    yearSuffix(nullable: true) // year-suffix
        
        refType inList: ["article",
                    "article-journal",
                    "article-magazine",
                    "article-newspaper",
                    "bill",
                    "book",
                    "broadcast",
                    "chapter",
                    "dataset",
                    "entry",
                    "entry-dictionary",
                    "entry-encyclopedia",
                    "figure",
                    "graphic",
                    "interview",
                    "legal_case",
                    "legislation",
                    "manuscript",
                    "map",
                    "motion_picture",
                    "musical_score",
                    "pamphlet",
                    "paper-conference",
                    "patent",
                    "personal_communication",
                    "post",
                    "post-weblog",
                    "report",
                    "review",
                    "review-book",
                    "song",
                    "speech",
                    "thesis",
                    "treaty",
                    "webpage" ]
    }
}
