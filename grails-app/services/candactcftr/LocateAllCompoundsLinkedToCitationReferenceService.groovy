package candactcftr

import grails.gorm.transactions.Transactional

@Transactional
class LocateAllCompoundsLinkedToCitationReferenceService {

    def serviceMethod() {

    }
    def locateAllCompoundsLinkedToCitationReference(long citationReferenceID) {
            // check in the reference list, which entry has this exact title
            
            
 //           def compoundHits = "from Compound as b where b.citationReferences.id='${citationReferenceID}' order by b.id"
            def compoundHits = Compound.where {citationReferences.id == citationReferenceID}
           return compoundHits
    }
}
