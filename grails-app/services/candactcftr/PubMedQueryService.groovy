package candactcftr

import grails.gorm.transactions.Transactional

import grails.converters.*
import groovy.json.JsonSlurper
import java.net.URLEncoder;

import java.util.regex.*

@Transactional
class PubMedQueryService {

    def serviceMethod() {

    }
    
    def lookUpPMID(String titleOfReference) {
    
        println "looking for PMID of: '" + titleOfReference + "'"
        def pmid = -1
        
        def slurper = new JsonSlurper()
        
        def myJson = null
        def titleOfReferenceForPubMedSearch = titleOfReference//.replaceAll("\\+", "")//.replaceAll("-", " ").replaceAll(",", "")
//         def url = new URL("https://www.ncbi.nlm.nih.gov/pubmed/?term=${titleOfReference}&report=uilist&format=text")
//         def titleUrled = URLEncoder.encode("${titleOfReferenceForPubMedSearch}");
//         def url = new URL("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&retmode=json&sort=relevance&field=title&term=${titleUrled}")
        def titleUrled = '"' + titleOfReferenceForPubMedSearch + '"'
        titleUrled = URLEncoder.encode(titleUrled);
//         def urlstring = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&retmode=json&sort=relevance&field=title&term=".concat(titleUrled)
        def urlstring = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&retmode=json&sort=relevance&term=".concat(titleUrled)
        
        def url = new URL(urlstring)
        
//         println url
//         println urlstring
        

//         URL apiUrl = new URL("https://api.zotero.org/groups/1632179/items/top?v=1&format=csljson&limit=100")
//         myJson = slurper.parse( url )
        
         def content 
         
         try {
             content = url.openConnection().with { conn ->
                 readTimeout = 10000
                 if( responseCode == 404 ) {
                     throw new Exception( 'Not Ok' )
                 }
                 println "Reference Title Exists"
                  conn.content.withReader { r ->
                      r.text
                 myJson = slurper.parse( url )
                  }
             }
         }
         catch( e ) {
            print e
             println "reference title can not be found"
             content = "SORRY PubMed HAS NO RECORD AVAILABLE"
             println content
         }
    
//         myJson = slurper.parse( url)
//         print myJson
//         render myJson
        
        
        if(myJson){
//             println myJson.esearchresult.idlist
            def idlist = myJson.esearchresult.idlist
            if(idlist){
                pmid = Integer.valueOf(idlist[0])        
                if(idlist.size>1){
                    println "TOO MANY RESULTS WHERE FOUND IN PUBCHEM; NOT UNIQUE"
                }
            }
        }

        
        if(pmid > 0){
        
        return pmid
        }else{
            return null
        }
    }
    
}
