package candactcftr

import grails.gorm.transactions.Transactional
import groovy.json.JsonSlurper
import grails.converters.*

@Transactional
class PubChemQueryService {

    def serviceMethod() {

    }
    
    
    def lookUpSynonyms(String InChIKey) {
    
        println InChIKey
        
        def myJson = null
        def slurper = new JsonSlurper()

        URL url = new URL("https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/InChIKey/${InChIKey}/synonyms/JSON")
        println url
        
        def content 
        try {
            content = url.openConnection().with { conn ->
                readTimeout = 10000
                if( responseCode == 404 ) {
                    throw new Exception( 'Not Ok' )
                }
                println "Exists"
                conn.content.withReader { r ->
                    r.text
                myJson = slurper.parse( url )
                }
            }
        }
        catch( e ) {
        
            println "synonym doesn't exist"
            content="SORRY PubChem HAS NO Synonyms in RECORD AVAILABLE"
        }
        
        return myJson
    }
    
    
    def lookUpCID(String InChIKey) {
    
        println InChIKey
        def cid = -1
        
        def slurper = new JsonSlurper()
        
        def myJson = null
        
        
        def url = new URL("https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/InChIKey/${InChIKey}/cids/JSON")
        println url
        def content 
        try {
            content = url.openConnection().with { conn ->
                readTimeout = 10000
                if( responseCode == 404 ) {
                    throw new Exception( 'Not Ok' )
                }
                println "Exists"
                conn.content.withReader { r ->
                    r.text
                myJson = slurper.parse( url )
                }
            }
        }
        catch( e ) {
        
            println "doesn't exist"
            content="SORRY PubChem HAS NO RECORD AVAILABLE"
        }
    
        
        if(myJson){
            cid = myJson.IdentifierList.CID[0]        
        }

        
        
        
        
        
        if(cid > 0){
        
        return cid
        }else{
            return null
        }
    }
    
    def lookUpSMILESByCID(int cid) {
    
        println cid
//         def cid = -1
        
        def slurper = new JsonSlurper()
        
        def myJson = null
        
        
        def url = new URL("https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/${cid}/property/CanonicalSMILES/JSON")
        println url
        def content 
        def smiles = "no smiles found"
        try {
            content = url.openConnection().with { conn ->
                readTimeout = 10000
                if( responseCode == 404 ) {
                    throw new Exception( 'Not Ok' )
                }
                println "Exists"
                conn.content.withReader { r ->
                    r.text
                myJson = slurper.parse( url )
                }
            }
        }
        catch( e ) {
        
            println "doesn't exist"
            content="SORRY PubChem HAS NO RECORD AVAILABLE"
        }
    
        
        if(myJson){
            cid = myJson.PropertyTable.Properties.CID[0]
            smiles = myJson.PropertyTable.Properties.CanonicalSMILES[0]
            
        }

        
        
        
        
        
        if(cid > 0){
        
        return smiles
        }else{
            return null
        }
    }
    
        def lookUpIsomericSMILESByCID(int cid) {
    
        println cid
//         def cid = -1
        
        def slurper = new JsonSlurper()
        
        def myJson = null
        
        
        def url = new URL("https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/${cid}/property/IsomericSMILES/JSON")
        println url
        def content 
        def smiles = "no smiles found"
        try {
            content = url.openConnection().with { conn ->
                readTimeout = 10000
                if( responseCode == 404 ) {
                    throw new Exception( 'Not Ok' )
                }
                println "Exists"
                conn.content.withReader { r ->
                    r.text
                myJson = slurper.parse( url )
                }
            }
        }
        catch( e ) {
        
            println "doesn't exist"
            content="SORRY PubChem HAS NO RECORD AVAILABLE"
        }
    
        
        if(myJson){
            cid = myJson.PropertyTable.Properties.CID[0]
            smiles = myJson.PropertyTable.Properties.IsomericSMILES[0]
            
        }

        
        
        
        
        
        if(cid > 0){
        
        return smiles
        }else{
            return null
        }
    }    
        

    
}
