package candactcftr

import grails.gorm.transactions.Transactional

@Transactional
class ReferenceLookUpService {

    def serviceMethod() {

    }
    
    def lookUpByAuthor() {
    
    }
    def lookUpByTitle(String titleQuery) {
            // check in the reference list, which entry has this exact title
            def referenceHits = CitationReference.where {refTitle =~ "%${titleQuery}%"}
            
           return referenceHits
    }
}
