package candactcftr

import grails.gorm.transactions.*

@Transactional
class OpenBabelSmilesToInChIKeyService {

    def serviceMethod() {

    }
    
    def smilesToInChIKey(String smilesQuery) {
    
        def tempIn = File
        def tempOut = File 
        def command = "do this"
        def InChIKey = "Some InChIKey"


        // call openbabel to translate the smiles to an InChI key
        // first create the in file with the smiles
        tempIn = File.createTempFile("tempIn",".tmp")
        tempIn.write("${smilesQuery}")
    //     println tempIn.absolutePath
        // create a blank one for the InChI key and SMILES
        tempOut = File.createTempFile('tempOut', '.txt') 
        tempOut.write("${smilesQuery}")  
    //     println tempOut.absolutePath    
        // call openbabel
    //     println "babel -ismi ${tempIn.absolutePath} -oinchikey ${tempOut.absolutePath}"
        command = "babel -ismi ${tempIn.absolutePath} -oinchikey ${tempOut.absolutePath}"
         println command.execute().text    
        InChIKey = tempOut.text
//         println "START" + InChIKey + "ENDE"
        InChIKey = InChIKey.take(InChIKey.length() - 1)
//          println "START" + InChIKey + "ENDE"
    
    
        return InChIKey
    }
}
