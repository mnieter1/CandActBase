<!doctype html>
<html lang="en" class="no-js">
<head>
    <asset:stylesheet src="application.css"/>
    

    <asset:javascript src="jquery-3.3.1.min.js"/>
        <asset:stylesheet src="kekule/kekule.css"/>

        <asset:javascript src="kekule/kekule.min.js"/>
    
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Grails"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" href="${createLinkTo(dir:'images',file:'favicon.ico')}" type="image/x-icon" /> 
    <g:layoutHead/>


    
</head>
<body>

    <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="../"><i class="fa grails-icon">
                        <asset:image src="grails-cupsonly-logo-white.svg"/>
                    </i>CandActBase</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a class="nav-item nav-link active" href="../">Home <span class="sr-only">(current)</span></a></li>
      
      <li><a class="nav-item nav-link disabled" href="/LibraryReferences"><b>Public</b>ations <b>Reference list</b></a></li>
        
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Compounds <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="/Compound/index">Compounds Seed Content</a></li>
          <li><a href="/Compound/cycleCompounds">cycle compounds</a></li>
          <li><a href="/Compound/showFullSet">full set</a></li>
          <li><a href="/Compound/searchCompounds">search compounds</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
    
    <g:layoutBody/>

    <div class="footer" role="contentinfo"></div>

    <div id="spinner" class="spinner" style="display:none;">
        <g:message code="spinner.alt" default="Loading&hellip;"/>
    </div>


</body>
</html>
