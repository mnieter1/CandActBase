<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
        <g:layoutTitle default="Grails"/>
    </title>
    
    <link rel="shortcut icon" href="${createLinkTo(dir:'images',file:'favicon.ico')}" type="image/x-icon" /> 
    <asset:stylesheet src="application.css"/>
    <asset:javascript src="application.js"/>

    <g:layoutHead/>
    
    <style>
        body {
            padding-top:70px;
        }
    </style>

    
</head>
<body>
  
<nav class="navbar navbar-default navbar-expand-lg bg-default fixed-top">
    <a class="navbar-brand" href="../"><i class="fa grails-icon">
                        <asset:image src="grails-cupsonly-logo-white.svg"></asset:image>
                    </i>CandActCFTR</a>
    <button class="navbar-toggler bg-dark" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active"><a class="nav-link" href="../">Home <span class="sr-only">(current)</span></a></li>
            
            <li class="nav-item"><a class="nav-item nav-link disabled" href="/LibraryReferences"><b>Public</b>ations <b>Reference list</b></a></li>
            

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Compounds
        </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                    <a class="dropdown-item" href="/Compound/index">Overview Compound Content  </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/Compound/cycleCompounds">cycle compounds PubChemViewer</a>
                    <a class="dropdown-item" href="/Compound/cycleCompoundsKekuleStyle">cycle compounds KekuleViewer</a>
                    <a class="dropdown-item" href="/Compound/showFullSet">full set</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/Compound/searchCompounds">search compounds</a>
                </div>
            </li>

        </ul>
    </div>

</nav>



    <g:layoutBody/>

    <div class="footer" role="contentinfo"></div>

    <div id="spinner" class="spinner" style="display:none;">
        <g:message code="spinner.alt" default="Loading&hellip;"/>
    </div>

    
    
    
</body>
</html>
