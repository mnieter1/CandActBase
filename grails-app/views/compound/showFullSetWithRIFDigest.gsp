<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>CandActCFTR Seed Content Compound List</title>
	</head>
	<body>

		<H1>CandActCFTR Seed Compounds with RIFs</H1>

<hr>



<g:each var="currentCompound" in="${allCompounds}">
    
    <div id="div2" style="width:800px;height:8px;background-color:green;"></div><br>
    <hr>
    <b>ID:</b> <%= currentCompound.id %>
    <br>
    <b>InChIKey:</b> ${currentCompound.inChIKey}</br> 
    <b>SMILES:</b> ${currentCompound.smiles}  
    

    <% if(currentCompound.citationReferences){ %>
    <h1><strong>reference list:</strong></h1>

    <g:each var="oneReference" in="${currentCompound.citationReferences}">
                <div id="pending_list" class="onTop">
                        <g:render template="/libraryReferences/ReferenceRendering" model="['citationReference':oneReference]" />
                </div>
                
                <g:each var="oneRIFReference" in="${oneReference.rifs}">
                <div id="pending_list_RIFs" class="onTop">
                    <g:render template="/libraryReferences/RIFReferenceRendering" model="['referenceIntoFunction':oneRIFReference]" />
                </g:each>    
            
            </g:each>
    <% } %>



    <div id="PubChemPane">
        <br/>
        <%-- <img src="https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/smiles/${currentCompound.smiles}/PNG" alt="chemical compound" style="width:304px;height:228px;"> --%>
        <br/>
        <p></br></p>
        <hr>
    </div>


    <br/>
    <% if(currentCompound.pubChem){ %>
    <g:render template="/compound/showPubChemRecord" model="['currentPubChem':currentCompound.pubChem]" />
    <% } %>

</g:each>

<br>



	</body>
</html>
