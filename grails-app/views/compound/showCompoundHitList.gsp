<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Matching CandActCFTR Compound List</title>
		
	</head>
	<body>

		<H1>Multiple Matching Entries for <strong>${queryString}</strong> in CandActCFTR Seed Compounds: <strong>${queryString} found ${nrOfHits}x</strong></H1>

<hr>
use the seach page function of your browser to see the synonyms highlighted which got found by the queryString
</br>
generally it's activated with the shortcut "Ctrl+F", where you can start and type ahead and the search term gets highlighed within the page
</br>



<g:each in="${currentCompounds}">
    

    </br> 
    <hr>
    <b>ID:</b> <a href="/Compound/cycleCompoundsKekuleStyle/${it.id}">${it.id}</a></br>
<b>InChIKey:</b> ${it.inChIKey}</br> 
<b>SMILES:</b> ${it.smiles}  


<div id="PubChemPane">
    <br/>
    <img src="https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/smiles/${it.smiles}/PNG" alt="chemical compound" style="width:304px;height:228px;">
    <br/>
    <p></br></p>
</div>



 <g:if test="${ it.pubChem }">   
    
    <g:render template="/compound/showPubChemRecord" model="['currentPubChem':it.pubChem]" />
</g:if>



</g:each>

	</body>
</html>
