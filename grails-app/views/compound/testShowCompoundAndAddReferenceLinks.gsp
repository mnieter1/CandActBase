<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>cycle compounds view</title>


<script>


	function myJSONQuest_Mod_synonyms_lookup (myQuerySmile) {
				var webQueryPath = 'https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/smiles/' + encodeURIComponent(myQuerySmile) + '/synonyms/JSON';
				
			var Httpreq = new XMLHttpRequest(); // a new request
			Httpreq.open("GET",webQueryPath,false);
			Httpreq.send(null);
			return Httpreq.responseText;
				
				}
				
	function myJSONQuest_Mod_CIDs_lookup (myQuerySmile) {
				var webQueryPath = 'https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/smiles/' + encodeURIComponent(myQuerySmile) + '/cids/JSON';
				
			var Httpreq = new XMLHttpRequest(); // a new request
			Httpreq.open("GET",webQueryPath,false);
			Httpreq.send(null);
			return Httpreq.responseText;
				
				}


				
    function getJSMolScript(myQuerySmile)  {
				var webQueryPath = 'https://chemapps.stolaf.edu/jmol/jmol.php?model=' + myQuerySmile + '&inline';
				
			
			return webQueryPath;
				
				}
				
				
    
    function searchReferenceForJoinWithCompound(){
				window.location = '/LibraryReferences/searchReferenceForJoinWithCompound?compoundID=${currentCompound.id}&newQuery=true';
    }

</script>


</head>
<body>


<a href="/Compound/cycleCompounds/${currentCompound.id -1}" class="btn btn-default">previous compound</a>
<b>ID:</b> ${currentCompound.id}
<a href="/Compound/cycleCompounds/${currentCompound.id +1}" class="btn btn-default">next compound</a>

</br>
<b>InChIKey:</b> ${currentCompound.inChIKey}</br> 
<b>SMILES:</b> ${currentCompound.smiles}  
 

<h1>reference list:</h1>
<input type="button" style="margin:10px" value="add reference" onclick="searchReferenceForJoinWithCompound()"></input><br/>

 <g:each var="oneReference" in="${currentCompound.citationReferences}">
            <div id="pending_list" class="onTop">
                    <g:render template="/libraryReferences/ReferenceRendering" model="['citationReference':oneReference]" />
            </div>
            
        </g:each>



<div id="PubChemPane">
    <br/>
    <img src="https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/smiles/${currentCompound.smiles}/PNG" alt="chemical compound" style="width:304px;height:228px;">
    <br/>
    <p></br></p>
    <hr>
</div>


<br/>

<g:render template="PubChemRecord" model="['currentCompound':currentCompound]" />



</body>
</html>
