<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>cycle compounds view</title>



</head>
<body>


<a href="/Compound/cycleCompounds/${currentCompound.id -1}" class="btn btn-default">previous compound</a>
<b>ID:</b> ${currentCompound.id}
<a href="/Compound/cycleCompounds/${currentCompound.id +1}" class="btn btn-default">next compound</a>

</br>
<b>InChIKey:</b> ${currentCompound.inChIKey}</br> 
<b>SMILES:</b> ${currentCompound.smiles}</br>  


<g:if test="${ currentCompound.biologicalDescriptors }">   
    
    <g:render template="/compound/showBiologicalDescriptorsInfoTags" model="['currentCompound':currentCompound]" />
</g:if>






<g:if test="${ currentCompound.citationReferences }">
    
    <h1><strong>reference list:</strong></h1>

    <g:each var="oneReference" in="${currentCompound.citationReferences}">
        <a href="/libraryReferences/showAllCompoundsToAReference/${oneReference.id}" class="btn btn-default"><b>ReferenceID:</b> ${oneReference.id}</a>
                <div id="pending_list" class="onTop">
                        <g:render template="/libraryReferences/ReferenceRendering" model="['citationReference':oneReference]" />
                </div>
                
                <g:each var="oneRIFReference" in="${oneReference.rifs}">
                <div id="pending_list_RIFs" class="onTop">
                    <g:render template="/libraryReferences/RIFReferenceRendering" model="['referenceIntoFunction':oneRIFReference]" />
                </g:each>    
            
            </g:each>
</g:if>
 

<div id="PubChemPane">
    <br/>
    <img src="https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/inchikey/${currentCompound.inChIKey}/PNG" alt="chemical compound" style="width:304px;height:228px;">
    <br/>
    <p></br></p>
    <hr>
</div>


<br/>

<g:if test="${ currentCompound.pubChem }">   
    
    <g:render template="/compound/showPubChemRecord" model="['currentPubChem':currentCompound.pubChem]" />
</g:if>




</body>
</html>
