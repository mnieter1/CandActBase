
<!-- new version loading the data from the database instead of directly querying PubChem -->
<script>
	var htmlcode = "";
// 		htmlcode += "<hr><br>";
        
        htmlcode += '<strong>CID</strong> is ' + "${currentPubChem.cid}";
		htmlcode += ' the link to PubChem record is <a href="https://pubchem.ncbi.nlm.nih.gov/compound/'+ "${currentPubChem.cid}" +'" target="_blank">' + "https://pubchem.ncbi.nlm.nih.gov/compound/${currentPubChem.cid}" + "</a><br>";
        

	document.write(htmlcode);

</script>


</br>
<strong>CID</strong> is <strong>${currentPubChem.cid}</strong></br> 


<strong>synonyms</strong> found at PubChem are: </br> 

<g:each var="synonym" in="${currentPubChem.synonyms}">
            
            ${synonym.synonym};
                            
            
</g:each>
