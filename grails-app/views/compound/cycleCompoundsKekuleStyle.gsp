<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>cycle compounds view with Kekule viewer</title>
    
    
    <asset:javascript src="jquery-3.3.1.js"/>
    <asset:stylesheet src="kekule/stylesheets/kekule.css"/>
    <asset:javascript src="kekule/kekule.min.js"/>

    
        <script>
        Kekule.Indigo.enable();       
        function load_kekule() {
            chemViewer = new Kekule.ChemWidget.Viewer(document.getElementById('kekuleWidget'));
        }
        function display_kekule(smi) {
            var mol = Kekule.IO.loadFormatData(smi, "smi");  
        chemViewer.setChemObj(mol);
        }
        function display() {
            var smi = document.getElementById("smiles2draw").value;
            console.log(smi);
            display_kekule(smi);
        }
        $(window).on('load', function(){
            console.log("OK");
            load_kekule();
            display();
            $('textarea').on('change', display);
        });
        </script>
    
</head>
<body>


<a href="/Compound/cycleCompoundsKekuleStyle/${currentCompound.id -1}" class="btn btn-default">previous compound</a>
<b>ID:</b> ${currentCompound.id}
<a href="/Compound/cycleCompoundsKekuleStyle/${currentCompound.id +1}" class="btn btn-default">next compound</a>

</br>
<b>InChIKey:</b> ${currentCompound.inChIKey}</br> 
<b>SMILES:</b> ${currentCompound.smiles}  



<g:if test="${ currentCompound.biologicalDescriptors }">   
    
    <g:render template="/compound/showBiologicalDescriptorsInfoTags" model="['currentCompound':currentCompound]" />
</g:if>



<textarea rows="10" cols="50" id="smiles2draw"  style="font-size: 12pt" style.display = "none">
${currentCompound.smiles}
</textarea>


    
<div id="kekuleRenderer"> 

<span id="kekuleWidget" style="width:100%;height:650px"
		 data-widget="Kekule.ChemWidget.Viewer2D" data-enable-toolbar="true" data-auto-size="true" data-padding="20"
		 data-toolbar-evoke-modes="[1]" >

</span>

</div>

<script>
    var smilerText = document.getElementById("smiles2draw");
    if (smilerText.style.display === "none") {
        smilerText.style.display = "block";
    } else {
        smilerText.style.display = "none";
    }
       
</script>


<g:if test="${ currentCompound.citationReferences }">
    
    <h1><strong>reference list:</strong></h1>

    <g:each var="oneReference" in="${currentCompound.citationReferences}">
        <a href="/libraryReferences/showAllCompoundsToAReference/${oneReference.id}" class="btn btn-default"><b>ReferenceID:</b> ${oneReference.id}</a>
                <div id="pending_list" class="onTop">
                        <g:render template="/libraryReferences/ReferenceRendering" model="['citationReference':oneReference]" />
                </div>
                
                <g:each var="oneRIFReference" in="${oneReference.rifs}">
                <div id="pending_list_RIFs" class="onTop">
                    <g:render template="/libraryReferences/RIFReferenceRendering" model="['referenceIntoFunction':oneRIFReference]" />
                </g:each>    
            
            </g:each>
</g:if>
 

<br/>

<g:if test="${ currentCompound.pubChem }">   
    
    <g:render template="/compound/showPubChemRecord" model="['currentPubChem':currentCompound.pubChem]" />
</g:if>







</body>
</html>
