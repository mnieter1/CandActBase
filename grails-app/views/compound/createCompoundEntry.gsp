<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Create Molecule</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	
    <script type="text/javascript">
		function getKetcher()
		{
			var frame = null;
			
			if ('frames' in window && 'ketcherFrame' in window.frames){
				frame = window.frames['ketcherFrame'];
			}else{
				return null;
			}
			
			if ('window' in frame)
				return frame.window.ketcher;
		}
		
		function saveCompoundBySMILES(){
				window.location = '/Compound/saveCompoundBySMILES?smiles="' + $('#SMILES').val()  +'"'+ '&synonym="' + $('#Synonym').val() +'"';
		}
		
		function saveCompoundUsingCID(){
				window.location = '/Compound/saveCompoundUsingCID?cid="' + $('#CID').val()  +'"'+ '&synonym="' + $('#Synonym').val() +'"';
		}
		
		function saveCompoundUsingInChIKey(){
				window.location = '/Compound/saveCompoundUsingInChIKey?InChIKey="' + $('#InChIKey').val()  +'"'+ '&synonym="' + $('#Synonym').val() +'"';
		}
		
		function getSmiles()
		{
			var ketcher = getKetcher();
			
			if (ketcher)
                $('#SMILES').val(ketcher.getSmiles());
		}

		function getMolfile()
		{
			var ketcher = getKetcher();
			
			if (ketcher)
				$('#textarea').val(ketcher.getMolfile());
		}

		var row = 1;
		
		function render()
		{
			var molfile = $('textarea').value;
			
			var smiles = molfile.strip();
			
			if (smiles == '' || smiles.indexOf('\n') == -1)
			{
				alert("Please, input Molfile");
				return;
			}
			
			var renderOpts = {
				'autoScale':true,
				'debug':true,
				'autoScaleMargin':20,
				'ignoreMouseEvents':true
			};
			
			var newRow = new Element('tr');
			
			newRow.update('<td id="row' + row + '" style="width:100%;height:100px;padding:0px;"></td>');
			$('table').insert(newRow);
			
			var ketcher = getKetcher();
			
			rowObject = $('row' + row);
			//alert(rowObject['clientWidth']);
			//rowObject.innerHTML = "asdasdf";
			if (ketcher.showMolfileOpts(rowObject, molfile, 20, renderOpts))
				row++;
		}
		
		function loadStructure ()
		{
			var ketcher = getKetcher();
			ketcher.setMolecule($('textarea').value);
		}
		
		function loadFragment ()
		{
			var ketcher = getKetcher();
			ketcher.addFragment($('textarea').value);
		}
		
		function loadMol ()
		{
		
			initialMolecule = 
			[
			
			
                "",
                "5280961 -OEChem-09291716242D",
				"",
                " 30 32  0     0  0  0  0  0  0999 V2000",
                "    5.5301   -1.4279    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0",
                "    3.7817    1.6067    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0",
                "    5.5301    1.5721    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0",
                "    2.0000   -1.4521    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0",
                "    9.8602    2.0721    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0",
                "    4.6641    0.0721    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "    6.3961    0.0721    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "    4.6641   -0.9279    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "    5.5301    0.5721    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "    7.2622    0.5721    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "    3.7702    0.6067    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "    6.3961   -0.9279    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "    3.7702   -1.4626    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "    2.8641    0.0929    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "    2.8641   -0.9487    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "    8.1282    0.0721    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "    7.2622    1.5721    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "    8.9942    0.5721    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "    8.1282    2.0721    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "    8.9942    1.5721    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "    6.9331   -1.2379    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0",
                "    3.7773   -2.0826    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0",
                "    2.3284    0.4050    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0",
                "    8.1282   -0.5479    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0",
                "    6.7252    1.8821    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0",
                "    9.5312    0.2621    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0",
                "    8.1282    2.6921    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0",
                "    3.2484    1.9229    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0",
                "    2.0024   -2.0721    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0",
                "   10.3972    1.7621    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0",
                "  1  8  1  0  0  0  0",
                "  1 12  1  0  0  0  0",
                "  2 11  1  0  0  0  0",
                "  2 28  1  0  0  0  0",
                "  3  9  2  0  0  0  0",
                "  4 15  1  0  0  0  0",
                "  4 29  1  0  0  0  0",
                "  5 20  1  0  0  0  0",
                "  5 30  1  0  0  0  0",
                "  6  8  1  0  0  0  0",
                "  6  9  1  0  0  0  0",
                "  6 11  2  0  0  0  0",
                "  7  9  1  0  0  0  0",
                "  7 10  1  0  0  0  0",
                "  7 12  2  0  0  0  0",
                "  8 13  2  0  0  0  0",
                " 10 16  2  0  0  0  0",
                " 10 17  1  0  0  0  0",
                " 11 14  1  0  0  0  0",
                " 12 21  1  0  0  0  0",
                " 13 15  1  0  0  0  0",
                " 13 22  1  0  0  0  0",
                " 14 15  2  0  0  0  0",
                " 14 23  1  0  0  0  0",
                " 16 18  1  0  0  0  0",
                " 16 24  1  0  0  0  0",
                " 17 19  2  0  0  0  0",
                " 17 25  1  0  0  0  0",
                " 18 20  2  0  0  0  0",
                " 18 26  1  0  0  0  0",
                " 19 20  1  0  0  0  0",
                " 19 27  1  0  0  0  0",
                "M  END",

				
			].join("\n");
			var ketcher = getKetcher();
			ketcher.setMolecule(initialMolecule);
		}
		</script>



</head>
<body>


		<h1><strong>enter data to create a new compound</strong><br /></h1>
        <p>
            the system will check if it can find a matching structure @ PubChem,
            <br />
            if it does not find an entry the specified synonym will be used,
            <br />
            otherwise all the synonyms listed in PubChem will be available.
        </p>
		<br />
            <label>Synonym: </label>
            <g:textField name="Synonym" value="genistein"/>
            <br/>
		<hr>
            <br />
            <h2><strong>enter a PubChem CID to register directly using the structure lookup from PubChem</strong><br /></h2>
            <label>CID: </label>
            <g:textField name="CID" value="5280961"/>
            <input type="button" style="margin:10px" value="register compound by CID" onclick="saveCompoundUsingCID()"></input><br/>
            <h2><strong>or use an InCHIKey to register directly using the structure lookup from PubChem</strong><br /></h2>
            <label>InChIKey: </label>
            <g:textField name="InChIKey" value=""/>
            <input type="button" style="margin:10px" value="register compound by InChIKey" onclick="saveCompoundUsingInChIKey()"></input><br/>
            <br/>
		<h2><strong>enter SMILES query strings to register directly</strong><br /></h2>
            make sure it is an isomeric SMILES if entering isomeric structures;
            <br>
             otherwise canonical smiles might fetch the other isomeric form (e.g. forskolin CID 47936 vs. CID 3413 CHEMBL1358402)
            <br>
            <label>SMILES: </label>
            <g:textField name="SMILES"/>
            <input type="button" style="margin:10px" value="register compound by SMILES" onclick="saveCompoundBySMILES()"></input><br/>
            <br>
		<hr>

				
    
				
<div id="div1" style="width:800px;height:5px;background-color:red;"></div><br>
		<div>
			<div>

				<h1><strong>or transfer SMILES info from ketcher</strong><br /></h1>
            </div>
 				<input type="button" style="margin:10px" value="Get SMILES from Ketcher" onclick="getSmiles()"></input><br/>

			</div>
		</div>
			<h1><strong>Draw a structure using Ketcher</strong></h1>
			<iframe onload="loadMol()" id="ketcherFrame" name="ketcherFrame" style="min-width:310px;min-height:510px;width:100%;border-style:none" src="/ketcher-master/ketcher.html?ketcher_maximize" scrolling="no"></iframe>
</body>
</html>
