<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>CandActBase Compounds Interface</title>
	</head>
	<body>

		<H1>CandActBase Compounds Interface</H1>
<br>
<hr>
The following ${maxCompoundID} structures are a list compounds which have been loaded into the database. If you want inspect what kind of literature references  were loaded as related publications <a href="/LibraryReferences">published</a>.
<br><br>

<hr>

<p>

<p>
<br></p>

<H4>Browse Compounds</H4>
in order for this to work load at least one compound
<ul>
    <li><a href="/Compound/cycleCompounds">Browse Single Entries</a> - browse single entries; depiction of structures will be retrieved from PubChem</li>
    <li><a href="/Compound/cycleCompoundsKekuleStyle">Browse Single Entries - Kekule structure viewer</a> - browse single entries; depiction of structures via Kekule.js; rendered directly in your browser</li>
    <li><a href="/Compound/showFullSet">View Compound List</a> - may take a while to compile </li>
    <li><a href="/Compound/searchCompounds">Search Compounds</a> - a searchpage where you can look for specific compounds by name, SMILES, InChIKey or drawing the structure </li>
</ul>

for plot options supported once data has been loaded properly check out CandActCFTR plot using eCharts @
<ul>    
    
    <li><a href="http://candactcftr.ams.med.uni-goettingen.de/Compound/showFullSetChemSpaceCFTRRelevance">Explore the ChemSpace - CFTR relevance</a> - look at interactive graphs of the distibution of our compounds in chemical space and how they were tagged according to literature</li>
    <li><a href="http://candactcftr.ams.med.uni-goettingen.de/Compound/showFullSetChemSpaceInfluenceOnCFTRFunction">Explore the ChemSpace - Influence on CFTR Function </a> - look at interactive graphs of the distibution of our compounds in chemical space and annotations </li>
    <li><a href="http://candactcftr.ams.med.uni-goettingen.de/Compound/showFullSetChemSpaceOrderOfInteraction">Explore the ChemSpace - Order of Interaction </a> - look at interactive graphs of the distibution of our compounds in chemical space and annotations </li>
    <li><a href="http://candactcftr.ams.med.uni-goettingen.de/Compound/showFullSetChemSpaceSubcellularCompartment">Explore the ChemSpace - Subcellular Compartment </a> - look at interactive graphs of the distibution of our compounds in chemical space and annotations </li>
</ul>



<hr>

</br>

<p>



	</body>
</html>
