<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Search Molecule</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    
    <script type="text/javascript">
		function getKetcher()
		{
			var frame = null;
			
			if ('frames' in window && 'ketcherFrame' in window.frames){
				frame = window.frames['ketcherFrame'];
			}else{
				return null;
			}
			
			if ('window' in frame)
				return frame.window.ketcher;
		}
		
		function searchWithSmiles(){
				window.location = '/Compound/searchCompoundsBySMILES?smiles="' + $('#SMILES').val() +'"';
		}
		
		
		function searchWithInChIKey(){
				window.location = '/Compound/searchCompoundsByInChIKey?InChIKey="' + $('#InChIKey').val() +'"';
		}
		
		function searchWithSynonymes(){
				window.location = '/Compound/searchCompoundsBySynonym?Synonym="' + $('#Synonym').val() +'"';
		}
		
		function getSmiles()
		{
			var ketcher = getKetcher();
			if (ketcher)
				$('#SMILES').val(ketcher.getSmiles());
		}

		function getMolfile()
		{
			var ketcher = getKetcher();
			
			if (ketcher)
				$('#textarea').val(ketcher.getMolfile());
		}

		var row = 1;
		
		function render()
		{
			var molfile = $('#textarea').val();
			
			var smiles = molfile.strip();
			
			if (smiles == '' || smiles.indexOf('\n') == -1)
			{
				alert("Please, input Molfile");
				return;
			}
			
			var renderOpts = {
				'autoScale':true,
				'debug':true,
				'autoScaleMargin':20,
				'ignoreMouseEvents':true
			};
			
			var newRow = new Element('tr');
			
			newRow.update('<td id="row' + row + '" style="width:100%;height:100px;padding:0px;"></td>');
			$('#table').insert(newRow);
			
			var ketcher = getKetcher();
			
			rowObject = $('row' + row);
			//alert(rowObject['clientWidth']);
			//rowObject.innerHTML = "asdasdf";
			if (ketcher.showMolfileOpts(rowObject, molfile, 20, renderOpts))
				row++;
		}
		
		function loadStructure ()
		{
			var ketcher = getKetcher();
			ketcher.setMolecule($('#textarea').val());
		}
		
		function loadFragment ()
		{
			var ketcher = getKetcher();
			ketcher.addFragment($('#textarea').val());
		}
		
		function loadMol ()
		{
			initialMolecule = 
			[

				"",
				"Ketcher 09061714512D 1   1.00000     0.00000     0",
				"",
                " 18 18  0     0  0            999 V2000",
                "   -1.1750    1.7500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "   -0.3090    1.2500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "   -0.3090    0.2500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "   -1.1750   -0.2500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "   -2.0410    0.2500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "   -2.0410    1.2500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "   -1.1750    2.7500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "   -0.3090    3.2500    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0",
                "   -2.0410    3.2500    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0",
                "    0.5570    2.7500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "   -1.1750   -1.2500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "   -2.0410   -1.7500    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0",
                "   -0.3090   -1.7500    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0",
                "   -2.9071   -1.2500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0",
                "    0.5570   -0.2500    0.0000 Cl  0  0  0  0  0  0  0  0  0  0  0  0",
                "    0.5570    1.7500    0.0000 Cl  0  0  0  0  0  0  0  0  0  0  0  0",
                "   -2.9071    1.7500    0.0000 Cl  0  0  0  0  0  0  0  0  0  0  0  0",
                "   -2.9071   -0.2500    0.0000 Cl  0  0  0  0  0  0  0  0  0  0  0  0",
                "  1  2  1  0     0  0",
                "  2  3  2  0     0  0",
                "  3  4  1  0     0  0",
                "  4  5  2  0     0  0",
                "  5  6  1  0     0  0",
                "  6  1  2  0     0  0",
                "  1  7  1  0     0  0",
                "  7  8  1  0     0  0",
                "  7  9  2  0     0  0",
                "  8 10  1  0     0  0",
                "  4 11  1  0     0  0",
                " 11 12  1  0     0  0",
                " 11 13  2  0     0  0",
                " 12 14  1  0     0  0",
                "  3 15  1  0     0  0",
                "  2 16  1  0     0  0",
                "  6 17  1  0     0  0",
                "  5 18  1  0     0  0",
                "M  END",
				
				
			].join("\n");
			var ketcher = getKetcher();
			ketcher.setMolecule(initialMolecule);
		}
		
		
		
		</script>



</head>
<body>
            <% def wasThereSomething = "${previouslyfoundEntryStatus}"%>
            
            <% if(wasThereSomething == "no match found"){%>
            <%= "<strong>no match found for <i>${previousSynonymQuery}</i><br> please try again</strong><br>" %>
            <%}%>
            <% if(wasThereSomething == "no structure matching SMILES found"){%>
            <%= "<strong>no match found for smiles <i>${previousSmilesQuery}</i><br> please try again</strong><br>" %>
            <%}%>
            <% if(wasThereSomething == "no structure matching InChIKey found"){%>
            <%= "<strong>no match found for InChIKey <i>${previousInChIKeyQuery}</i><br> please try again</strong><br>" %>
            <%}%>
            
            
		
            <label>Synonym: </label>
            <g:textField name="Synonym" value="curcumin"/>
            <input type="button" style="margin:10px" value="Search via Name" onclick="searchWithSynonymes()"></input><br/>
            <br/>
		<hr>
		<h1><strong>enter structure query strings directly</strong><br /></h1>

            <label>SMILES: </label>
            <g:textField name="SMILES" />
            <input type="button" style="margin:10px" value="Search via SMILES" onclick="searchWithSmiles()"></input><br/>
            <label>InChIKey: </label>
            <g:textField name="InChIKey"/>
            <input type="button" style="margin:10px" value="Search via InChIKey" onclick="searchWithInChIKey()"></input><br/>

				
    
				
<div id="div1" style="width:800px;height:5px;background-color:red;"></div><br>
		<div>
			<div>

				<h1><strong>or transfer SMILES info from ketcher</strong><br /></h1>
            </div>
 				<button class="btn btn-default" onclick="getSmiles()" >Get SMILES from Ketcher</button>

			</div>
		</div>
			<h1><strong>Draw a structure using Ketcher</strong></h1>
			<iframe onload="loadMol()" id="ketcherFrame" name="ketcherFrame" style="min-width:310px;min-height:510px;width:100%;border-style:none" src="/ketcher-master/ketcher.html?ketcher_maximize" scrolling="no"></iframe>
</body>
</html>
