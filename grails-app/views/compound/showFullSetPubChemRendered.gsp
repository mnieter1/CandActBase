<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>CandActCFTR Seed Content Compound List</title>
	</head>
	<body>

		<H1>CandActCFTR Seed Compounds</H1>

<hr>
The following ${maxCompoundID} structures are a list compounds which have been reported in CFTR related publications <a href="/LibraryReferences">published</a> in the past decade. We collected all structures regardless of an activation of CFTR function or an improvement of CFTR processing, thus also including reported negative compound references or even inhibitors.
<br><br>

<hr>




<g:each var="currentCompound" in="${allCompounds}">
    
    <div id="div2" style="width:800px;height:8px;background-color:green;"></div><br>
    <hr>
    <b>ID:</b> <a href="/Compound/cycleCompoundsKekuleStyle/${currentCompound.id}">${currentCompound.id}</a>
    <br>
    <b>InChIKey:</b> ${currentCompound.inChIKey}</br> 
    <b>SMILES:</b> ${currentCompound.smiles}  
    

<g:if test="${ currentCompound.citationReferences }">
    
    <h1><strong>reference list:</strong></h1>

    <g:each var="oneReference" in="${currentCompound.citationReferences}">
                <div id="pending_list" class="onTop">
                        <g:render template="/libraryReferences/ReferenceRendering" model="['citationReference':oneReference]" />
                </div>
                
                <g:each var="oneRIFReference" in="${oneReference.rifs}">
                <div id="pending_list_RIFs" class="onTop">
                    <g:render template="/libraryReferences/RIFReferenceRendering" model="['referenceIntoFunction':oneRIFReference]" />
                </g:each>    
            
            </g:each>
</g:if>



    <div id="PubChemPane">
        <br/>
        <img src="https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/inchikey/${currentCompound.inChIKey}/PNG" alt="chemical compound" style="width:304px;height:228px;">
        <br/>
        <p></br></p>
        <hr>
    </div>


    <br/>
 <g:if test="${ currentCompound.pubChem }">   
    
    <g:render template="/compound/showPubChemRecord" model="['currentPubChem':currentCompound.pubChem]" />
</g:if>



</g:each>

<br>



	</body>
</html>
