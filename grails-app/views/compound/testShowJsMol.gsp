<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Render Domain with JsMol</title>

<script type="text/javascript" src="https://chemapps.stolaf.edu/jmol/jmol.php?model= &link= "></script>

</head>
<body>
ID: <g:fieldValue bean="${compound1}" field="id"/><br/>
PubChem CID: <g:fieldValue bean="${compound1}" field="pubChemId"/><br/>
smiles: <g:fieldValue bean="${compound1}" field="smiles"/><br/>
retrieved_ID_with_other_method id: ${compound1.id}



<div>                                                                                                       
                                <img src="https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/smiles/${compound1.smiles}/PNG" alt="chemical compound" style="width:304px;height:228px;">                                                                                       
                        </div>


<p></br></p>
<hr>

<H4>JSMol pane</H4>
left click hold and moving mouse will rotate the model
</br>
right click for context menue
<div>

<script type="text/javascript" src="https://chemapps.stolaf.edu/jmol/jmol.php?model=${compound1.smiles}&inline"></script>

</div>


<br/>


</body>
</html>
