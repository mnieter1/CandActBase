

<g:if test="${ currentCompound.biologicalDescriptors }">   
    <h1><strong>biological descriptors:</strong></h1>

    <g:each var="oneReference" in="${currentCompound.biologicalDescriptors}">
        <b>CFTR relevance:</b> ${oneReference.cftrRelevance}
        <br>
        <br>
        <b>Category:</b><br>
        <b>Influence on CFTR function</b> ${oneReference.influenceOnCftrFunction} 
        <br>
        <b>Order of interaction</b> ${oneReference.orderOfInteraction}
        <br>
        <b>subcellular compartment</b> ${oneReference.subcellularCompartment}
        <br>        
        
            </g:each>

</g:if>
