

<g:each in="${currentCompounds}">
    

    </br> 
    <hr>
    <b>ID:</b> <a href="/Compound/cycleCompoundsKekuleStyle/${it.id}">${it.id}</a></br>
<b>InChIKey:</b> ${it.inChIKey}</br> 
<b>SMILES:</b> ${it.smiles}  

<g:if test="${ it.biologicalDescriptors }">   
    
    <g:render template="/compound/showBiologicalDescriptorsInfoTags" model="['currentCompound':it]" />
</g:if>



<div id="PubChemPane">
    <br/>
    <img src="https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/smiles/${it.smiles}/PNG" alt="chemical compound" style="width:304px;height:228px;">
    <br/>
    <p></br></p>
</div>



 <g:if test="${ it.pubChem }">   
    
    <g:render template="/compound/showPubChemRecord" model="['currentPubChem':it.pubChem]" />
</g:if>



</g:each>
