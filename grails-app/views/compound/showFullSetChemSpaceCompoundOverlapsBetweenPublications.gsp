<!DOCTYPE html>
<%! import grails.converters.JSON %>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>CandActCFTR Seed Content ChemSpace</title>
		<asset:javascript src="echarts.min.js"/>
	</head>
	<body>

		<H1>CandActCFTR Seed Compounds ChemSpace</H1>

<hr>


<!-- preparing a DOM with width and height for ECharts -->
    
    
      <p id="intersection">
      <br><br><br><br><br>
      </p>
      <hr>
    <div id="mainCircle" style="width:1200px; height:1200px;"></div>
      
    
    <script>
    //checking out the model to object conversion to load the data from the database
    var theJSONString = '${allCompounds as JSON}';
    var fixedQuotesInStrings = theJSONString.replace(new RegExp('&quot;', 'gi'), '"');
    var compoundList  = JSON.parse(fixedQuotesInStrings);

    //checking out the model to object conversion to load the data from the database
    var theJSONString = '${allBiologicalDescriptors as JSON}';
    var fixedQuotesInStrings = theJSONString.replace(new RegExp('&quot;', 'gi'), '"');
    var biologicalDescriptorsList  = JSON.parse(fixedQuotesInStrings);

    //checking out the model to object conversion to load the data from the database
    var theJSONString = '${allChemicalDescriptors as JSON}';
    var fixedQuotesInStrings = theJSONString.replace(new RegExp('&quot;', 'gi'), '"');
    var chemicalDescriptorsList  = JSON.parse(fixedQuotesInStrings);
   
    
    
 /*   
      str = JSON.stringify(compoundList[0], null, 4); // (Optional) beautiful indented output.
    console.log(str); // Logs output to dev tools console.
    alert(str); // Displays output using window.alert()
    
    
    str = JSON.stringify(biologicalDescriptorsList[0].cftrRelevance, null, 4); // (Optional) beautiful indented output.
    console.log(str); // Logs output to dev tools console.
    alert(str); // Displays output using window.alert()
    
    
    str = JSON.stringify(chemicalDescriptorsList[0].pca1, null, 4); // (Optional) beautiful indented output.
    console.log(str); // Logs output to dev tools console.
    alert(str); // Displays output using window.alert()
    

    document.getElementById("demoToReportJustAValue").innerHTML = "first JSON entry id is " + compoundList[0].id + " and smiles" + compoundList[0].smiles;
*/
    </script>

    <script type="text/javascript">
        // based on prepared DOM, initialize echarts instance


    
    
    
    
    // now make the required columns out of the provided json
   // first remove the entries with a missing entry in the coordinates
   var offsetcounter = 0;
   for (let x = 0; x < compoundList.length; x++) {  
    //strOut = JSON.stringify(compoundList[x], null, 4); // (Optional) beautiful indented output.
    //    console.log(strOut); // Logs output to dev tools console.
    if (chemicalDescriptorsList[x] == undefined){   
     //   str = JSON.stringify(compoundList[x], null, 4); // (Optional) beautiful indented output.
     //   console.log(str); // Logs output to dev tools console.
     //   alert(str); // Displays output using window.alert()
        
        chemicalDescriptorsList.splice((x-offsetcounter), 1);
        biologicalDescriptorsList.splice((x-offsetcounter), 1);
        compoundList.splice((x-offsetcounter), 1);
        offsetcounter = offsetcounter+1;
    }
   }
    
    //find all the different versions of activity classes
    // "orderOfInteraction": "unknown",
    // "cftrRelevance": "CFTR corrector",
    // "influenceOnCftrFunction": "enhances CFTR function",
    // "subcellularCompartment": "several"
    
    
    var uniqueActivityClassesC = [];
    var allActivityClassesC = [];
    for (let x = 0; x < compoundList.length; x++) {
        
        // define unique set
        if(uniqueActivityClassesC.indexOf(biologicalDescriptorsList[x].cftrRelevance.toString()) == -1){
        
            uniqueActivityClassesC.push(biologicalDescriptorsList[x].cftrRelevance.toString());
        }
        // collect them all
        allActivityClassesC.push(biologicalDescriptorsList[x].cftrRelevance.toString());
    }
    
    
    
    //first specify where the entries are for each class creating index arrays
    var indexContainerForSeriesC = [];
    var aSingleIndexSeriesC = [];
    
    for (let y = 0; y < uniqueActivityClassesC.length; y++) {
        aSingleIndexSeriesC = [];
        for (let x = 0; x < allActivityClassesC.length; x++) {
            // check if current class is a match
            if(allActivityClassesC[x] == uniqueActivityClassesC[y]){
                // seems to match so remember the position
                aSingleIndexSeriesC.push(x);
            }
        
        }
        indexContainerForSeriesC.push(aSingleIndexSeriesC);
    }
    
    // now that we got the indices of our series sets we can extract the coodinates and feed them into the echarts required object
    var currentChemSpaceCoordsC = [];
    var onePositionC = [];
    var seriesDefinitionsC = [];
    var singleSeriesC = {};
    
    for (let y = 0; y < uniqueActivityClassesC.length; y++) {
        // based on the class definition we collect the required data and stuff it into the container to hold a single series, afterwards we push this into the collector for all series
        currentChemSpaceCoordsC = [];
    
        for (let x = 0; x < indexContainerForSeriesC[y].length; x++) {
            
            // make 2D array
            onePositionC = [];

            // works but not the one below with indices probably because of missing coordinates?!?
            //onePositionC.push(indexContainerForSeriesC[y][x]);
            //onePositionC.push(indexContainerForSeriesC[y][x]);
            
            onePositionC.push(chemicalDescriptorsList[indexContainerForSeriesC[y][x]].pca1);
            onePositionC.push(chemicalDescriptorsList[indexContainerForSeriesC[y][x]].pca2);
            currentChemSpaceCoordsC.push(onePositionC);
        }   
        // define the container with name and coordinates
        singleSeriesC =
        {
            name:uniqueActivityClassesC[y],
            type:'scatter',
            data: currentChemSpaceCoordsC
            
        }
        seriesDefinitionsC.push(singleSeriesC);
    }
    

    
    </script>


    
    
    
    
    

<br>

<script>
var myChart = echarts.init(document.getElementById('mainCircle'));
myChart.showLoading();

    myChart.hideLoading();


   var graph = {
       "nodes":[ {
  "id" : 1,
  "pmid" : "17766192",
  "name" : "Noël S 2008",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 2,
  "pmid" : "23949358",
  "name" : "Conger BT 2013",
  "count" : 6,
  "symbolSize" : 17.91759469228055
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 3,
  "pmid" : "11015292",
  "name" : "Bachmann A 2000",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 4,
  "pmid" : "21366549",
  "name" : "Hamdaoui N 2011",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 5,
  "pmid" : "17197571",
  "name" : "Dechecchi MC 2007",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 6,
  "pmid" : "18430055",
  "name" : "Xu LN 2008",
  "count" : 6,
  "symbolSize" : 17.91759469228055
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 7,
  "pmid" : "23788656",
  "name" : "Namkung W 2013",
  "count" : 5,
  "symbolSize" : 16.094379124341003
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 8,
  "pmid" : "21730204",
  "name" : "Phuan PW 2011",
  "count" : 12,
  "symbolSize" : 24.849066497880003
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 9,
  "pmid" : "12386156",
  "name" : "Suaud L 2002",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 10,
  "pmid" : "16424149",
  "name" : "Lipecka J 2006",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 11,
  "pmid" : "19535515",
  "name" : "Chao PC 2009",
  "count" : 10,
  "symbolSize" : 23.02585092994046
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 12,
  "pmid" : "15306545",
  "name" : "Baker MJ 2004",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 13,
  "pmid" : "15105504",
  "name" : "Egan ME 2004",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 14,
  "pmid" : "12832418",
  "name" : "Yang H 2003",
  "count" : 12,
  "symbolSize" : 24.849066497880003
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 15,
  "pmid" : "16624886",
  "name" : "Wang Y 2006",
  "count" : 6,
  "symbolSize" : 17.91759469228055
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 16,
  "pmid" : "15582996",
  "name" : "Berger AL 2005",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 17,
  "pmid" : "17178710",
  "name" : "Wang W 2007",
  "count" : 4,
  "symbolSize" : 13.862943611198906
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 18,
  "pmid" : "17178109",
  "name" : "Harada K 2007",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 19,
  "pmid" : "21441077",
  "name" : "Yu YC 2011",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 20,
  "pmid" : "12359632",
  "name" : "Zegarra-Moran O 2002",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 21,
  "pmid" : "24714160",
  "name" : "Zhang Y 2014",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 22,
  "pmid" : "24282612",
  "name" : "Zhang S 2013",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 23,
  "pmid" : "22186243",
  "name" : "Zhang S 2011",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 24,
  "pmid" : "12914781",
  "name" : "Andersson C 2003",
  "count" : 4,
  "symbolSize" : 13.862943611198906
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 25,
  "pmid" : "19886674",
  "name" : "Cartiera MS 2010",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 27,
  "pmid" : "26356422",
  "name" : "Cendret V 2015",
  "count" : 12,
  "symbolSize" : 24.849066497880003
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 29,
  "pmid" : "12080183",
  "name" : "deCarvalho AC 2002",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 30,
  "pmid" : "27081574",
  "name" : "Dey I 2016",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 31,
  "pmid" : "16284361",
  "name" : "Grubb BR 2006",
  "count" : 4,
  "symbolSize" : 13.862943611198906
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 32,
  "pmid" : "11262417",
  "name" : "Galietta LJ 2001",
  "count" : 221,
  "symbolSize" : 53.98162701517752
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 34,
  "pmid" : "15191910",
  "name" : "Lim M 2004",
  "count" : 5,
  "symbolSize" : 16.094379124341003
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 35,
  "pmid" : "27413118",
  "name" : "Lin WY 2016",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 36,
  "pmid" : "15996659",
  "name" : "Moran O 2005",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 37,
  "pmid" : "19131642",
  "name" : "Norez C 2009",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 38,
  "pmid" : "18309088",
  "name" : "Noël S 2008",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 39,
  "pmid" : "16546175",
  "name" : "Norez C 2006",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 40,
  "pmid" : "26863533",
  "name" : "Park J 2016",
  "count" : 18,
  "symbolSize" : 28.903717578961647
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 41,
  "pmid" : "20042712",
  "name" : "Pyle LC 2010",
  "count" : 21,
  "symbolSize" : 30.44522437723423
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 42,
  "pmid" : "20200141",
  "name" : "Robert R 2010",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 43,
  "pmid" : "18223673",
  "name" : "Schmidt A 2008",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 44,
  "pmid" : "25350163",
  "name" : "De Stefano D 2014",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 45,
  "pmid" : "17975008",
  "name" : "Robert R 2008",
  "count" : 7,
  "symbolSize" : 19.45910149055313
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 46,
  "pmid" : "27007499",
  "name" : "Wang W 2016",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 47,
  "pmid" : "27193581",
  "name" : "Carlile GW 2016",
  "count" : 8,
  "symbolSize" : 20.79441541679836
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 48,
  "pmid" : "17056801",
  "name" : "Al-Nakkash L 2006",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 49,
  "pmid" : "15155835",
  "name" : "Ai T 2004",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 50,
  "pmid" : "19740743",
  "name" : "Bernard K 2009",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 51,
  "pmid" : "18595696",
  "name" : "Al-Nakkash L 2008",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 52,
  "pmid" : "24970923",
  "name" : "Boinot C 2014",
  "count" : 5,
  "symbolSize" : 16.094379124341003
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 53,
  "pmid" : "24974227",
  "name" : "Carlile GW 2015",
  "count" : 2632,
  "symbolSize" : 78.75499292445208
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 54,
  "pmid" : "19880323",
  "name" : "Cateni F 2009",
  "count" : 19,
  "symbolSize" : 29.444389791664403
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 55,
  "pmid" : "17497613",
  "name" : "Carlile GW 2007",
  "count" : 30,
  "symbolSize" : 34.011973816621555
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 56,
  "pmid" : "25747701",
  "name" : "Chen L 2015",
  "count" : 5,
  "symbolSize" : 16.094379124341003
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 57,
  "pmid" : "20501743",
  "name" : "Kim Chiaw P 2010",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 58,
  "pmid" : "27288484",
  "name" : "Cui G 2016",
  "count" : 8,
  "symbolSize" : 20.79441541679836
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 59,
  "pmid" : "27160424",
  "name" : "Dekkers JF 2016",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 60,
  "pmid" : "26092868",
  "name" : "Dhooghe B 2015",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 61,
  "pmid" : "19966789",
  "name" : "Hutt DM 2010",
  "count" : 4,
  "symbolSize" : 13.862943611198906
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 62,
  "pmid" : "26342647",
  "name" : "Jai Y 2015",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 63,
  "pmid" : "26515683",
  "name" : "Dong ZW 2015",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 64,
  "pmid" : "22281182",
  "name" : "Leonard A 2012",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 65,
  "pmid" : "17762174",
  "name" : "Lim CH 2007",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 66,
  "pmid" : "19299496",
  "name" : "Lubamba B 2009",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 67,
  "pmid" : "26492939",
  "name" : "Matthes E 2016",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 68,
  "pmid" : "20974851",
  "name" : "Melani R 2010",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 69,
  "pmid" : "20406820",
  "name" : "Miki H 2010",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 71,
  "pmid" : "23331029",
  "name" : "Sohma Y 2013",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 72,
  "pmid" : "20226262",
  "name" : "Rowe SM 2010",
  "count" : 6,
  "symbolSize" : 17.91759469228055
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 73,
  "pmid" : "21753184",
  "name" : "Sondo E 2011",
  "count" : 5,
  "symbolSize" : 16.094379124341003
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 74,
  "pmid" : "15280357",
  "name" : "Song Y 2004",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 75,
  "pmid" : "25635662",
  "name" : "Wang G 2015",
  "count" : 4,
  "symbolSize" : 13.862943611198906
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 76,
  "pmid" : "27035618",
  "name" : "Tosco A 2016",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 77,
  "pmid" : "25867080",
  "name" : "Wang G 2015",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 78,
  "pmid" : "15325250",
  "name" : "Dragomir A 2004",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 79,
  "pmid" : "16127463",
  "name" : "Pedemonte N 2005",
  "count" : 31,
  "symbolSize" : 34.33987204485146
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 80,
  "pmid" : "16443646",
  "name" : "Van Goor F 2006",
  "count" : 7,
  "symbolSize" : 19.45910149055313
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 81,
  "pmid" : "21338920",
  "name" : "Sampson HM 2011",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 82,
  "pmid" : "22988441",
  "name" : "Anjos SM 2012",
  "count" : 6,
  "symbolSize" : 17.91759469228055
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 83,
  "pmid" : "23060796",
  "name" : "Zhang D 2012",
  "count" : 4,
  "symbolSize" : 13.862943611198906
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 84,
  "pmid" : "23102222",
  "name" : "Carlile GW 2012",
  "count" : 6,
  "symbolSize" : 17.91759469228055
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 85,
  "pmid" : "23982976",
  "name" : "Odolczyk N 2013",
  "count" : 14,
  "symbolSize" : 26.390573296152585
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 86,
  "pmid" : "26444971",
  "name" : "Hall JD 2016",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 87,
  "pmid" : "26888287",
  "name" : "Stevers LM 2016",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 88,
  "pmid" : "29251932",
  "name" : "Wang X 2018",
  "count" : 22,
  "symbolSize" : 30.91042453358316
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 } ],
    "links": [ {
  "id" : "1",
  "source" : 1.0,
  "target" : 2.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2",
  "source" : 1.0,
  "target" : 3.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "4",
  "source" : 1.0,
  "target" : 5.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "8",
  "source" : 1.0,
  "target" : 9.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "10",
  "source" : 1.0,
  "target" : 11.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "11",
  "source" : 1.0,
  "target" : 12.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "15",
  "source" : 1.0,
  "target" : 16.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "16",
  "source" : 1.0,
  "target" : 17.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "18",
  "source" : 1.0,
  "target" : 19.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "19",
  "source" : 1.0,
  "target" : 20.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "23",
  "source" : 1.0,
  "target" : 24.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "27",
  "source" : 1.0,
  "target" : 30.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "29",
  "source" : 1.0,
  "target" : 32.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "30",
  "source" : 1.0,
  "target" : 34.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "32",
  "source" : 1.0,
  "target" : 36.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "37",
  "source" : 1.0,
  "target" : 41.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "39",
  "source" : 1.0,
  "target" : 43.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "44",
  "source" : 1.0,
  "target" : 48.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "45",
  "source" : 1.0,
  "target" : 49.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "47",
  "source" : 1.0,
  "target" : 51.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "49",
  "source" : 1.0,
  "target" : 53.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "50",
  "source" : 1.0,
  "target" : 54.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "55",
  "source" : 1.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "61",
  "source" : 1.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "64",
  "source" : 1.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "66",
  "source" : 1.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "67",
  "source" : 1.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "75",
  "source" : 1.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "84",
  "source" : 2.0,
  "target" : 3.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "87",
  "source" : 2.0,
  "target" : 6.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "88",
  "source" : 2.0,
  "target" : 7.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "90",
  "source" : 2.0,
  "target" : 9.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "92",
  "source" : 2.0,
  "target" : 11.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "93",
  "source" : 2.0,
  "target" : 12.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "97",
  "source" : 2.0,
  "target" : 16.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "98",
  "source" : 2.0,
  "target" : 17.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "100",
  "source" : 2.0,
  "target" : 19.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "101",
  "source" : 2.0,
  "target" : 20.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "105",
  "source" : 2.0,
  "target" : 24.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "109",
  "source" : 2.0,
  "target" : 30.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "111",
  "source" : 2.0,
  "target" : 32.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "112",
  "source" : 2.0,
  "target" : 34.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "114",
  "source" : 2.0,
  "target" : 36.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "119",
  "source" : 2.0,
  "target" : 41.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "121",
  "source" : 2.0,
  "target" : 43.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "126",
  "source" : 2.0,
  "target" : 48.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "127",
  "source" : 2.0,
  "target" : 49.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "129",
  "source" : 2.0,
  "target" : 51.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "131",
  "source" : 2.0,
  "target" : 53.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "132",
  "source" : 2.0,
  "target" : 54.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "135",
  "source" : 2.0,
  "target" : 57.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "137",
  "source" : 2.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "143",
  "source" : 2.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "146",
  "source" : 2.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "148",
  "source" : 2.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "149",
  "source" : 2.0,
  "target" : 72.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "157",
  "source" : 2.0,
  "target" : 80.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "171",
  "source" : 3.0,
  "target" : 9.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "173",
  "source" : 3.0,
  "target" : 11.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "174",
  "source" : 3.0,
  "target" : 12.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "178",
  "source" : 3.0,
  "target" : 16.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "179",
  "source" : 3.0,
  "target" : 17.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "181",
  "source" : 3.0,
  "target" : 19.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "182",
  "source" : 3.0,
  "target" : 20.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "186",
  "source" : 3.0,
  "target" : 24.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "190",
  "source" : 3.0,
  "target" : 30.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "192",
  "source" : 3.0,
  "target" : 32.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "193",
  "source" : 3.0,
  "target" : 34.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "195",
  "source" : 3.0,
  "target" : 36.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "200",
  "source" : 3.0,
  "target" : 41.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "202",
  "source" : 3.0,
  "target" : 43.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "207",
  "source" : 3.0,
  "target" : 48.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "208",
  "source" : 3.0,
  "target" : 49.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "210",
  "source" : 3.0,
  "target" : 51.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "212",
  "source" : 3.0,
  "target" : 53.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "213",
  "source" : 3.0,
  "target" : 54.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "218",
  "source" : 3.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "224",
  "source" : 3.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "227",
  "source" : 3.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "229",
  "source" : 3.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "230",
  "source" : 3.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "238",
  "source" : 3.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "264",
  "source" : 4.0,
  "target" : 22.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "270",
  "source" : 4.0,
  "target" : 30.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "292",
  "source" : 4.0,
  "target" : 53.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "299",
  "source" : 4.0,
  "target" : 60.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "301",
  "source" : 4.0,
  "target" : 62.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "341",
  "source" : 5.0,
  "target" : 20.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "449",
  "source" : 6.0,
  "target" : 53.0,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "500",
  "source" : 7.0,
  "target" : 24.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "526",
  "source" : 7.0,
  "target" : 53.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "544",
  "source" : 7.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "638",
  "source" : 9.0,
  "target" : 11.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "639",
  "source" : 9.0,
  "target" : 12.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "643",
  "source" : 9.0,
  "target" : 16.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "644",
  "source" : 9.0,
  "target" : 17.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "646",
  "source" : 9.0,
  "target" : 19.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "647",
  "source" : 9.0,
  "target" : 20.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "651",
  "source" : 9.0,
  "target" : 24.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "655",
  "source" : 9.0,
  "target" : 30.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "657",
  "source" : 9.0,
  "target" : 32.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "658",
  "source" : 9.0,
  "target" : 34.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "660",
  "source" : 9.0,
  "target" : 36.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "665",
  "source" : 9.0,
  "target" : 41.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "667",
  "source" : 9.0,
  "target" : 43.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "672",
  "source" : 9.0,
  "target" : 48.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "673",
  "source" : 9.0,
  "target" : 49.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "675",
  "source" : 9.0,
  "target" : 51.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "677",
  "source" : 9.0,
  "target" : 53.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "678",
  "source" : 9.0,
  "target" : 54.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "683",
  "source" : 9.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "689",
  "source" : 9.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "692",
  "source" : 9.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "694",
  "source" : 9.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "695",
  "source" : 9.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "703",
  "source" : 9.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "714",
  "source" : 10.0,
  "target" : 13.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "717",
  "source" : 10.0,
  "target" : 16.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "718",
  "source" : 10.0,
  "target" : 17.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "719",
  "source" : 10.0,
  "target" : 18.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "720",
  "source" : 10.0,
  "target" : 19.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "726",
  "source" : 10.0,
  "target" : 25.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "729",
  "source" : 10.0,
  "target" : 30.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "730",
  "source" : 10.0,
  "target" : 31.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "744",
  "source" : 10.0,
  "target" : 46.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "748",
  "source" : 10.0,
  "target" : 50.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "751",
  "source" : 10.0,
  "target" : 53.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "757",
  "source" : 10.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "761",
  "source" : 10.0,
  "target" : 63.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "768",
  "source" : 10.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "771",
  "source" : 10.0,
  "target" : 74.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "772",
  "source" : 10.0,
  "target" : 75.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "774",
  "source" : 10.0,
  "target" : 77.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "775",
  "source" : 10.0,
  "target" : 78.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "777",
  "source" : 10.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "786",
  "source" : 11.0,
  "target" : 12.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "790",
  "source" : 11.0,
  "target" : 16.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "791",
  "source" : 11.0,
  "target" : 17.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "793",
  "source" : 11.0,
  "target" : 19.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "794",
  "source" : 11.0,
  "target" : 20.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "798",
  "source" : 11.0,
  "target" : 24.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "802",
  "source" : 11.0,
  "target" : 30.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "804",
  "source" : 11.0,
  "target" : 32.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "805",
  "source" : 11.0,
  "target" : 34.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "807",
  "source" : 11.0,
  "target" : 36.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "812",
  "source" : 11.0,
  "target" : 41.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "814",
  "source" : 11.0,
  "target" : 43.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "819",
  "source" : 11.0,
  "target" : 48.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "820",
  "source" : 11.0,
  "target" : 49.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "822",
  "source" : 11.0,
  "target" : 51.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "824",
  "source" : 11.0,
  "target" : 53.0,
  "overlap" : 4
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "825",
  "source" : 11.0,
  "target" : 54.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "830",
  "source" : 11.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "836",
  "source" : 11.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "839",
  "source" : 11.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "841",
  "source" : 11.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "842",
  "source" : 11.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "850",
  "source" : 11.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "862",
  "source" : 12.0,
  "target" : 16.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "863",
  "source" : 12.0,
  "target" : 17.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "865",
  "source" : 12.0,
  "target" : 19.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "866",
  "source" : 12.0,
  "target" : 20.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "870",
  "source" : 12.0,
  "target" : 24.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "874",
  "source" : 12.0,
  "target" : 30.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "876",
  "source" : 12.0,
  "target" : 32.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "877",
  "source" : 12.0,
  "target" : 34.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "879",
  "source" : 12.0,
  "target" : 36.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "884",
  "source" : 12.0,
  "target" : 41.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "886",
  "source" : 12.0,
  "target" : 43.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "891",
  "source" : 12.0,
  "target" : 48.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "892",
  "source" : 12.0,
  "target" : 49.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "894",
  "source" : 12.0,
  "target" : 51.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "896",
  "source" : 12.0,
  "target" : 53.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "897",
  "source" : 12.0,
  "target" : 54.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "902",
  "source" : 12.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "908",
  "source" : 12.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "911",
  "source" : 12.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "913",
  "source" : 12.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "914",
  "source" : 12.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "922",
  "source" : 12.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "933",
  "source" : 13.0,
  "target" : 16.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "934",
  "source" : 13.0,
  "target" : 17.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "935",
  "source" : 13.0,
  "target" : 18.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "936",
  "source" : 13.0,
  "target" : 19.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "942",
  "source" : 13.0,
  "target" : 25.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "945",
  "source" : 13.0,
  "target" : 30.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "946",
  "source" : 13.0,
  "target" : 31.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "960",
  "source" : 13.0,
  "target" : 46.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "964",
  "source" : 13.0,
  "target" : 50.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "967",
  "source" : 13.0,
  "target" : 53.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "973",
  "source" : 13.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "977",
  "source" : 13.0,
  "target" : 63.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "984",
  "source" : 13.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "987",
  "source" : 13.0,
  "target" : 74.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "988",
  "source" : 13.0,
  "target" : 75.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "990",
  "source" : 13.0,
  "target" : 77.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "991",
  "source" : 13.0,
  "target" : 78.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "993",
  "source" : 13.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1095",
  "source" : 15.0,
  "target" : 42.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1105",
  "source" : 15.0,
  "target" : 52.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1106",
  "source" : 15.0,
  "target" : 53.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1110",
  "source" : 15.0,
  "target" : 57.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1124",
  "source" : 15.0,
  "target" : 72.0,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1125",
  "source" : 15.0,
  "target" : 73.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1131",
  "source" : 15.0,
  "target" : 79.0,
  "overlap" : 4
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1132",
  "source" : 15.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1134",
  "source" : 15.0,
  "target" : 82.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1136",
  "source" : 15.0,
  "target" : 84.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1137",
  "source" : 15.0,
  "target" : 85.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1141",
  "source" : 16.0,
  "target" : 17.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1142",
  "source" : 16.0,
  "target" : 18.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1143",
  "source" : 16.0,
  "target" : 19.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1144",
  "source" : 16.0,
  "target" : 20.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1148",
  "source" : 16.0,
  "target" : 24.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1149",
  "source" : 16.0,
  "target" : 25.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1152",
  "source" : 16.0,
  "target" : 30.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1153",
  "source" : 16.0,
  "target" : 31.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1154",
  "source" : 16.0,
  "target" : 32.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1155",
  "source" : 16.0,
  "target" : 34.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1157",
  "source" : 16.0,
  "target" : 36.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1162",
  "source" : 16.0,
  "target" : 41.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1164",
  "source" : 16.0,
  "target" : 43.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1167",
  "source" : 16.0,
  "target" : 46.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1169",
  "source" : 16.0,
  "target" : 48.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1170",
  "source" : 16.0,
  "target" : 49.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1171",
  "source" : 16.0,
  "target" : 50.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1172",
  "source" : 16.0,
  "target" : 51.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1174",
  "source" : 16.0,
  "target" : 53.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1175",
  "source" : 16.0,
  "target" : 54.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1180",
  "source" : 16.0,
  "target" : 59.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1184",
  "source" : 16.0,
  "target" : 63.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1186",
  "source" : 16.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1189",
  "source" : 16.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1191",
  "source" : 16.0,
  "target" : 71.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1192",
  "source" : 16.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1194",
  "source" : 16.0,
  "target" : 74.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1195",
  "source" : 16.0,
  "target" : 75.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1197",
  "source" : 16.0,
  "target" : 77.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1198",
  "source" : 16.0,
  "target" : 78.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1200",
  "source" : 16.0,
  "target" : 80.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1209",
  "source" : 17.0,
  "target" : 18.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1210",
  "source" : 17.0,
  "target" : 19.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1211",
  "source" : 17.0,
  "target" : 20.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1215",
  "source" : 17.0,
  "target" : 24.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1216",
  "source" : 17.0,
  "target" : 25.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1219",
  "source" : 17.0,
  "target" : 30.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1220",
  "source" : 17.0,
  "target" : 31.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1221",
  "source" : 17.0,
  "target" : 32.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1222",
  "source" : 17.0,
  "target" : 34.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1223",
  "source" : 17.0,
  "target" : 35.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1224",
  "source" : 17.0,
  "target" : 36.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1229",
  "source" : 17.0,
  "target" : 41.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1231",
  "source" : 17.0,
  "target" : 43.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1234",
  "source" : 17.0,
  "target" : 46.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1236",
  "source" : 17.0,
  "target" : 48.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1237",
  "source" : 17.0,
  "target" : 49.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1238",
  "source" : 17.0,
  "target" : 50.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1239",
  "source" : 17.0,
  "target" : 51.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1241",
  "source" : 17.0,
  "target" : 53.0,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1242",
  "source" : 17.0,
  "target" : 54.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1246",
  "source" : 17.0,
  "target" : 58.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1247",
  "source" : 17.0,
  "target" : 59.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1251",
  "source" : 17.0,
  "target" : 63.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1253",
  "source" : 17.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1256",
  "source" : 17.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1258",
  "source" : 17.0,
  "target" : 71.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1259",
  "source" : 17.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1261",
  "source" : 17.0,
  "target" : 74.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1262",
  "source" : 17.0,
  "target" : 75.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1264",
  "source" : 17.0,
  "target" : 77.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1265",
  "source" : 17.0,
  "target" : 78.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1267",
  "source" : 17.0,
  "target" : 80.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1276",
  "source" : 18.0,
  "target" : 19.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1282",
  "source" : 18.0,
  "target" : 25.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1285",
  "source" : 18.0,
  "target" : 30.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1286",
  "source" : 18.0,
  "target" : 31.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1300",
  "source" : 18.0,
  "target" : 46.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1304",
  "source" : 18.0,
  "target" : 50.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1307",
  "source" : 18.0,
  "target" : 53.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1313",
  "source" : 18.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1317",
  "source" : 18.0,
  "target" : 63.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1324",
  "source" : 18.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1327",
  "source" : 18.0,
  "target" : 74.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1328",
  "source" : 18.0,
  "target" : 75.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1330",
  "source" : 18.0,
  "target" : 77.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1331",
  "source" : 18.0,
  "target" : 78.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1333",
  "source" : 18.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1342",
  "source" : 19.0,
  "target" : 20.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1346",
  "source" : 19.0,
  "target" : 24.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1347",
  "source" : 19.0,
  "target" : 25.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1350",
  "source" : 19.0,
  "target" : 30.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1351",
  "source" : 19.0,
  "target" : 31.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1352",
  "source" : 19.0,
  "target" : 32.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1353",
  "source" : 19.0,
  "target" : 34.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1355",
  "source" : 19.0,
  "target" : 36.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1360",
  "source" : 19.0,
  "target" : 41.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1362",
  "source" : 19.0,
  "target" : 43.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1365",
  "source" : 19.0,
  "target" : 46.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1367",
  "source" : 19.0,
  "target" : 48.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1368",
  "source" : 19.0,
  "target" : 49.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1369",
  "source" : 19.0,
  "target" : 50.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1370",
  "source" : 19.0,
  "target" : 51.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1372",
  "source" : 19.0,
  "target" : 53.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1373",
  "source" : 19.0,
  "target" : 54.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1378",
  "source" : 19.0,
  "target" : 59.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1382",
  "source" : 19.0,
  "target" : 63.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1384",
  "source" : 19.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1387",
  "source" : 19.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1389",
  "source" : 19.0,
  "target" : 71.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1390",
  "source" : 19.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1392",
  "source" : 19.0,
  "target" : 74.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1393",
  "source" : 19.0,
  "target" : 75.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1395",
  "source" : 19.0,
  "target" : 77.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1396",
  "source" : 19.0,
  "target" : 78.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1398",
  "source" : 19.0,
  "target" : 80.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1410",
  "source" : 20.0,
  "target" : 24.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1413",
  "source" : 20.0,
  "target" : 29.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1414",
  "source" : 20.0,
  "target" : 30.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1416",
  "source" : 20.0,
  "target" : 32.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1417",
  "source" : 20.0,
  "target" : 34.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1419",
  "source" : 20.0,
  "target" : 36.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1424",
  "source" : 20.0,
  "target" : 41.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1426",
  "source" : 20.0,
  "target" : 43.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1431",
  "source" : 20.0,
  "target" : 48.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1432",
  "source" : 20.0,
  "target" : 49.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1434",
  "source" : 20.0,
  "target" : 51.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1436",
  "source" : 20.0,
  "target" : 53.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1437",
  "source" : 20.0,
  "target" : 54.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1442",
  "source" : 20.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1448",
  "source" : 20.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1451",
  "source" : 20.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1453",
  "source" : 20.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1454",
  "source" : 20.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1462",
  "source" : 20.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1539",
  "source" : 22.0,
  "target" : 30.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1561",
  "source" : 22.0,
  "target" : 53.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1568",
  "source" : 22.0,
  "target" : 60.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1570",
  "source" : 22.0,
  "target" : 62.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1603",
  "source" : 23.0,
  "target" : 34.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1610",
  "source" : 23.0,
  "target" : 41.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1660",
  "source" : 24.0,
  "target" : 30.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1662",
  "source" : 24.0,
  "target" : 32.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1663",
  "source" : 24.0,
  "target" : 34.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1665",
  "source" : 24.0,
  "target" : 36.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1670",
  "source" : 24.0,
  "target" : 41.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1672",
  "source" : 24.0,
  "target" : 43.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1677",
  "source" : 24.0,
  "target" : 48.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1678",
  "source" : 24.0,
  "target" : 49.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1680",
  "source" : 24.0,
  "target" : 51.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1682",
  "source" : 24.0,
  "target" : 53.0,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1683",
  "source" : 24.0,
  "target" : 54.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1688",
  "source" : 24.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1694",
  "source" : 24.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1697",
  "source" : 24.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1699",
  "source" : 24.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1700",
  "source" : 24.0,
  "target" : 72.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1702",
  "source" : 24.0,
  "target" : 74.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1708",
  "source" : 24.0,
  "target" : 80.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1719",
  "source" : 25.0,
  "target" : 30.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1720",
  "source" : 25.0,
  "target" : 31.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1734",
  "source" : 25.0,
  "target" : 46.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1738",
  "source" : 25.0,
  "target" : 50.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1741",
  "source" : 25.0,
  "target" : 53.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1747",
  "source" : 25.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1751",
  "source" : 25.0,
  "target" : 63.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1758",
  "source" : 25.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1761",
  "source" : 25.0,
  "target" : 74.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1762",
  "source" : 25.0,
  "target" : 75.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1764",
  "source" : 25.0,
  "target" : 77.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1765",
  "source" : 25.0,
  "target" : 78.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1767",
  "source" : 25.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1783",
  "source" : 27.0,
  "target" : 37.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1784",
  "source" : 27.0,
  "target" : 38.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1785",
  "source" : 27.0,
  "target" : 39.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1798",
  "source" : 27.0,
  "target" : 52.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1810",
  "source" : 27.0,
  "target" : 64.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1812",
  "source" : 27.0,
  "target" : 66.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1856",
  "source" : 29.0,
  "target" : 53.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1891",
  "source" : 30.0,
  "target" : 31.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1892",
  "source" : 30.0,
  "target" : 32.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1893",
  "source" : 30.0,
  "target" : 34.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1895",
  "source" : 30.0,
  "target" : 36.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1900",
  "source" : 30.0,
  "target" : 41.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1902",
  "source" : 30.0,
  "target" : 43.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1905",
  "source" : 30.0,
  "target" : 46.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1907",
  "source" : 30.0,
  "target" : 48.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1908",
  "source" : 30.0,
  "target" : 49.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1909",
  "source" : 30.0,
  "target" : 50.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1910",
  "source" : 30.0,
  "target" : 51.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1912",
  "source" : 30.0,
  "target" : 53.0,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1913",
  "source" : 30.0,
  "target" : 54.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1918",
  "source" : 30.0,
  "target" : 59.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1919",
  "source" : 30.0,
  "target" : 60.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1921",
  "source" : 30.0,
  "target" : 62.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1922",
  "source" : 30.0,
  "target" : 63.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1924",
  "source" : 30.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1927",
  "source" : 30.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1929",
  "source" : 30.0,
  "target" : 71.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1930",
  "source" : 30.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1932",
  "source" : 30.0,
  "target" : 74.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1933",
  "source" : 30.0,
  "target" : 75.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1935",
  "source" : 30.0,
  "target" : 77.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1936",
  "source" : 30.0,
  "target" : 78.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1938",
  "source" : 30.0,
  "target" : 80.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1955",
  "source" : 31.0,
  "target" : 41.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1960",
  "source" : 31.0,
  "target" : 46.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1964",
  "source" : 31.0,
  "target" : 50.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1967",
  "source" : 31.0,
  "target" : 53.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1973",
  "source" : 31.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1974",
  "source" : 31.0,
  "target" : 60.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1977",
  "source" : 31.0,
  "target" : 63.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1984",
  "source" : 31.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1987",
  "source" : 31.0,
  "target" : 74.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1988",
  "source" : 31.0,
  "target" : 75.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1990",
  "source" : 31.0,
  "target" : 77.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1991",
  "source" : 31.0,
  "target" : 78.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1993",
  "source" : 31.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2002",
  "source" : 32.0,
  "target" : 34.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2004",
  "source" : 32.0,
  "target" : 36.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2009",
  "source" : 32.0,
  "target" : 41.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2011",
  "source" : 32.0,
  "target" : 43.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2016",
  "source" : 32.0,
  "target" : 48.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2017",
  "source" : 32.0,
  "target" : 49.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2019",
  "source" : 32.0,
  "target" : 51.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2021",
  "source" : 32.0,
  "target" : 53.0,
  "overlap" : 5
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2022",
  "source" : 32.0,
  "target" : 54.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2027",
  "source" : 32.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2033",
  "source" : 32.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2036",
  "source" : 32.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2038",
  "source" : 32.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2039",
  "source" : 32.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2047",
  "source" : 32.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2057",
  "source" : 34.0,
  "target" : 36.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2062",
  "source" : 34.0,
  "target" : 41.0,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2064",
  "source" : 34.0,
  "target" : 43.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2069",
  "source" : 34.0,
  "target" : 48.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2070",
  "source" : 34.0,
  "target" : 49.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2072",
  "source" : 34.0,
  "target" : 51.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2074",
  "source" : 34.0,
  "target" : 53.0,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2075",
  "source" : 34.0,
  "target" : 54.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2080",
  "source" : 34.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2086",
  "source" : 34.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2089",
  "source" : 34.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2091",
  "source" : 34.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2092",
  "source" : 34.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2094",
  "source" : 34.0,
  "target" : 74.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2100",
  "source" : 34.0,
  "target" : 80.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2119",
  "source" : 35.0,
  "target" : 46.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2126",
  "source" : 35.0,
  "target" : 53.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2131",
  "source" : 35.0,
  "target" : 58.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2132",
  "source" : 35.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2140",
  "source" : 35.0,
  "target" : 67.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2147",
  "source" : 35.0,
  "target" : 75.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2165",
  "source" : 36.0,
  "target" : 41.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2167",
  "source" : 36.0,
  "target" : 43.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2172",
  "source" : 36.0,
  "target" : 48.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2173",
  "source" : 36.0,
  "target" : 49.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2175",
  "source" : 36.0,
  "target" : 51.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2177",
  "source" : 36.0,
  "target" : 53.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2178",
  "source" : 36.0,
  "target" : 54.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2183",
  "source" : 36.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2189",
  "source" : 36.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2192",
  "source" : 36.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2194",
  "source" : 36.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2195",
  "source" : 36.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2203",
  "source" : 36.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2212",
  "source" : 37.0,
  "target" : 38.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2213",
  "source" : 37.0,
  "target" : 39.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2226",
  "source" : 37.0,
  "target" : 52.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2238",
  "source" : 37.0,
  "target" : 64.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2240",
  "source" : 37.0,
  "target" : 66.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2262",
  "source" : 38.0,
  "target" : 39.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2275",
  "source" : 38.0,
  "target" : 52.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2287",
  "source" : 38.0,
  "target" : 64.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2289",
  "source" : 38.0,
  "target" : 66.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2323",
  "source" : 39.0,
  "target" : 52.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2335",
  "source" : 39.0,
  "target" : 64.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2337",
  "source" : 39.0,
  "target" : 66.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2407",
  "source" : 41.0,
  "target" : 43.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2412",
  "source" : 41.0,
  "target" : 48.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2413",
  "source" : 41.0,
  "target" : 49.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2415",
  "source" : 41.0,
  "target" : 51.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2417",
  "source" : 41.0,
  "target" : 53.0,
  "overlap" : 8
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2418",
  "source" : 41.0,
  "target" : 54.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2419",
  "source" : 41.0,
  "target" : 55.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2423",
  "source" : 41.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2424",
  "source" : 41.0,
  "target" : 60.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2429",
  "source" : 41.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2432",
  "source" : 41.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2434",
  "source" : 41.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2435",
  "source" : 41.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2443",
  "source" : 41.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2462",
  "source" : 42.0,
  "target" : 53.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2466",
  "source" : 42.0,
  "target" : 57.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2480",
  "source" : 42.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2481",
  "source" : 42.0,
  "target" : 73.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2488",
  "source" : 42.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2490",
  "source" : 42.0,
  "target" : 82.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2492",
  "source" : 42.0,
  "target" : 84.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2501",
  "source" : 43.0,
  "target" : 48.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2502",
  "source" : 43.0,
  "target" : 49.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2504",
  "source" : 43.0,
  "target" : 51.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2506",
  "source" : 43.0,
  "target" : 53.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2507",
  "source" : 43.0,
  "target" : 54.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2512",
  "source" : 43.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2518",
  "source" : 43.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2521",
  "source" : 43.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2523",
  "source" : 43.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2524",
  "source" : 43.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2532",
  "source" : 43.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2549",
  "source" : 44.0,
  "target" : 53.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2552",
  "source" : 44.0,
  "target" : 56.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2571",
  "source" : 44.0,
  "target" : 76.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2591",
  "source" : 45.0,
  "target" : 53.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2593",
  "source" : 45.0,
  "target" : 55.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2629",
  "source" : 46.0,
  "target" : 50.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2632",
  "source" : 46.0,
  "target" : 53.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2637",
  "source" : 46.0,
  "target" : 58.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2638",
  "source" : 46.0,
  "target" : 59.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2642",
  "source" : 46.0,
  "target" : 63.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2646",
  "source" : 46.0,
  "target" : 67.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2649",
  "source" : 46.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2652",
  "source" : 46.0,
  "target" : 74.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2653",
  "source" : 46.0,
  "target" : 75.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2655",
  "source" : 46.0,
  "target" : 77.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2656",
  "source" : 46.0,
  "target" : 78.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2658",
  "source" : 46.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2702",
  "source" : 47.0,
  "target" : 84.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2707",
  "source" : 48.0,
  "target" : 49.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2709",
  "source" : 48.0,
  "target" : 51.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2711",
  "source" : 48.0,
  "target" : 53.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2712",
  "source" : 48.0,
  "target" : 54.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2717",
  "source" : 48.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2723",
  "source" : 48.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2726",
  "source" : 48.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2728",
  "source" : 48.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2729",
  "source" : 48.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2737",
  "source" : 48.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2747",
  "source" : 49.0,
  "target" : 51.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2749",
  "source" : 49.0,
  "target" : 53.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2750",
  "source" : 49.0,
  "target" : 54.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2755",
  "source" : 49.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2761",
  "source" : 49.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2764",
  "source" : 49.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2766",
  "source" : 49.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2767",
  "source" : 49.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2775",
  "source" : 49.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2786",
  "source" : 50.0,
  "target" : 53.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2792",
  "source" : 50.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2796",
  "source" : 50.0,
  "target" : 63.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2803",
  "source" : 50.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2806",
  "source" : 50.0,
  "target" : 74.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2807",
  "source" : 50.0,
  "target" : 75.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2809",
  "source" : 50.0,
  "target" : 77.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2810",
  "source" : 50.0,
  "target" : 78.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2812",
  "source" : 50.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2822",
  "source" : 51.0,
  "target" : 53.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2823",
  "source" : 51.0,
  "target" : 54.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2828",
  "source" : 51.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2834",
  "source" : 51.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2837",
  "source" : 51.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2839",
  "source" : 51.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2840",
  "source" : 51.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2848",
  "source" : 51.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2865",
  "source" : 52.0,
  "target" : 61.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2867",
  "source" : 52.0,
  "target" : 63.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2868",
  "source" : 52.0,
  "target" : 64.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2870",
  "source" : 52.0,
  "target" : 66.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2871",
  "source" : 52.0,
  "target" : 67.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2875",
  "source" : 52.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2876",
  "source" : 52.0,
  "target" : 73.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2882",
  "source" : 52.0,
  "target" : 79.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2888",
  "source" : 52.0,
  "target" : 85.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2890",
  "source" : 52.0,
  "target" : 87.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2892",
  "source" : 53.0,
  "target" : 54.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2893",
  "source" : 53.0,
  "target" : 55.0,
  "overlap" : 15
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2894",
  "source" : 53.0,
  "target" : 56.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2895",
  "source" : 53.0,
  "target" : 57.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2896",
  "source" : 53.0,
  "target" : 58.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2897",
  "source" : 53.0,
  "target" : 59.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2898",
  "source" : 53.0,
  "target" : 60.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2900",
  "source" : 53.0,
  "target" : 62.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2901",
  "source" : 53.0,
  "target" : 63.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2903",
  "source" : 53.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2906",
  "source" : 53.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2908",
  "source" : 53.0,
  "target" : 71.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2909",
  "source" : 53.0,
  "target" : 72.0,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2910",
  "source" : 53.0,
  "target" : 73.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2911",
  "source" : 53.0,
  "target" : 74.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2912",
  "source" : 53.0,
  "target" : 75.0,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2913",
  "source" : 53.0,
  "target" : 76.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2914",
  "source" : 53.0,
  "target" : 77.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2915",
  "source" : 53.0,
  "target" : 78.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2917",
  "source" : 53.0,
  "target" : 80.0,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2919",
  "source" : 53.0,
  "target" : 82.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2920",
  "source" : 53.0,
  "target" : 83.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2921",
  "source" : 53.0,
  "target" : 84.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2930",
  "source" : 54.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2936",
  "source" : 54.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2939",
  "source" : 54.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2941",
  "source" : 54.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2942",
  "source" : 54.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2950",
  "source" : 54.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3009",
  "source" : 56.0,
  "target" : 76.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3035",
  "source" : 57.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3036",
  "source" : 57.0,
  "target" : 73.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3043",
  "source" : 57.0,
  "target" : 80.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3045",
  "source" : 57.0,
  "target" : 82.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3047",
  "source" : 57.0,
  "target" : 84.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3052",
  "source" : 58.0,
  "target" : 59.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3060",
  "source" : 58.0,
  "target" : 67.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3067",
  "source" : 58.0,
  "target" : 75.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3084",
  "source" : 59.0,
  "target" : 63.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3086",
  "source" : 59.0,
  "target" : 65.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3088",
  "source" : 59.0,
  "target" : 67.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3089",
  "source" : 59.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3091",
  "source" : 59.0,
  "target" : 71.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3092",
  "source" : 59.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3094",
  "source" : 59.0,
  "target" : 74.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3095",
  "source" : 59.0,
  "target" : 75.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3097",
  "source" : 59.0,
  "target" : 77.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3098",
  "source" : 59.0,
  "target" : 78.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3100",
  "source" : 59.0,
  "target" : 80.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3110",
  "source" : 60.0,
  "target" : 62.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3146",
  "source" : 61.0,
  "target" : 73.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3190",
  "source" : 63.0,
  "target" : 67.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3193",
  "source" : 63.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3196",
  "source" : 63.0,
  "target" : 74.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3197",
  "source" : 63.0,
  "target" : 75.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3199",
  "source" : 63.0,
  "target" : 77.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3200",
  "source" : 63.0,
  "target" : 78.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3202",
  "source" : 63.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3207",
  "source" : 63.0,
  "target" : 85.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3209",
  "source" : 63.0,
  "target" : 87.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3212",
  "source" : 64.0,
  "target" : 66.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3236",
  "source" : 65.0,
  "target" : 68.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3238",
  "source" : 65.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3239",
  "source" : 65.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3247",
  "source" : 65.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3293",
  "source" : 67.0,
  "target" : 85.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3295",
  "source" : 67.0,
  "target" : 87.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3298",
  "source" : 68.0,
  "target" : 71.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3299",
  "source" : 68.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3307",
  "source" : 68.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3334",
  "source" : 71.0,
  "target" : 72.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3336",
  "source" : 71.0,
  "target" : 74.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3337",
  "source" : 71.0,
  "target" : 75.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3339",
  "source" : 71.0,
  "target" : 77.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3340",
  "source" : 71.0,
  "target" : 78.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3342",
  "source" : 71.0,
  "target" : 80.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3351",
  "source" : 72.0,
  "target" : 73.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3357",
  "source" : 72.0,
  "target" : 79.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3358",
  "source" : 72.0,
  "target" : 80.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3360",
  "source" : 72.0,
  "target" : 82.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3362",
  "source" : 72.0,
  "target" : 84.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3363",
  "source" : 72.0,
  "target" : 85.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3372",
  "source" : 73.0,
  "target" : 79.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3373",
  "source" : 73.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3375",
  "source" : 73.0,
  "target" : 82.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3377",
  "source" : 73.0,
  "target" : 84.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3378",
  "source" : 73.0,
  "target" : 85.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3382",
  "source" : 74.0,
  "target" : 75.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3384",
  "source" : 74.0,
  "target" : 77.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3385",
  "source" : 74.0,
  "target" : 78.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3387",
  "source" : 74.0,
  "target" : 80.0,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3397",
  "source" : 75.0,
  "target" : 77.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3398",
  "source" : 75.0,
  "target" : 78.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3400",
  "source" : 75.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3421",
  "source" : 77.0,
  "target" : 78.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3423",
  "source" : 77.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3433",
  "source" : 78.0,
  "target" : 80.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3447",
  "source" : 79.0,
  "target" : 85.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3452",
  "source" : 80.0,
  "target" : 82.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3454",
  "source" : 80.0,
  "target" : 84.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3467",
  "source" : 82.0,
  "target" : 84.0,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3482",
  "source" : 85.0,
  "target" : 87.0,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         } ]
        
    }
    var categories = [];
    for (var i = 0; i < 2; i++) {
        categories[i] = {
            name: 'CandActCFTR' + i
        };
    }
    
    //just one library, later can be set to contain ChEMBL etc.
    categories = [{
            name: 'CandActCFTR'
        }]
    
    graph.nodes.forEach(function (node) {
        node.itemStyle = null;
        
        //node name to be extended to allow link and name
        node.name = [node.name, node.id, node.count];
        
        node.value = node.symbolSize*1.2;
        //node.symbolSize /= 1.5;
        node.symbolSize = node.symbolSize
        node.label = {
            normal: {
                show: true// node.count >= 0
            }
        };
        
        node.category = node.attributes.modularity_class;
        node.library_size = node.attributes.library_size;
        node.id = node.id;
        
    });
    
        graph.links.forEach(function (link) {
        
        //node name to be extended to allow link and name
        link.name = [link.overlap];
        
    });
    
    option = {
        title: {
            text: 'CandActCFTR - overlap of compounds between publications',
            subtext: 'an edge depicts an overlap, the size of the bubble estimates the log(size) of the reported compound library',
            top: 'bottom',
            left: 'right'
        },
          
        //tooltip: {},
         tooltip : {
            trigger: 'item',
            showDelay : 0,
            enterable: true,
            formatter : function (params) {
//                 if (params.value.length > 1) {
                    return params.seriesName + ':<br/>'
                    + params.name[0] + ' <br/>'
                    + '<a href="/libraryReferences/showAllCompoundsToAReference/' + params.name[1] +'" class="btn btn-default">refID =' + params.name[1] + '</a>'
                    + ' <br/>'
                    + 'library size = ' + params.name[2];
//                     + params.attributes.library_size +  ' first <br/>' 
//                     + params.library_size +  ' second <br/>';
//                 }
//                 else {
//                     return params.seriesName + ' :<br/>'
//                     + params.value + ' compound(s)';
//                 }
            }
        },
          
        legend: [{
            // selectedMode: 'single',
            data: categories.map(function (a) {
                return a.name;
            })
        }],
        animationDurationUpdate: 1500,
        animationEasingUpdate: 'quinticInOut',
        series : [
            {
                name: 'CandActCFTR',
                type: 'graph',
                layout: 'circular',
                circular: {
                    rotateLabel: true
                },
                data: graph.nodes,
                links: graph.links,
                
                categories: categories,
                roam: true,
                label: {
                    normal: {
                        position: 'right',
                        formatter : function (params) {
                                return ''+ params.name[0];

                            }
                    }
                },
                lineStyle: {
                    normal: {
                        color: 'source',
                        curveness: 0.3
                    }
                },
                focusNodeAdjacency: true,
                itemStyle: {
                    normal: {
                        borderColor: '#fff',
                        borderWidth: 1,
                        shadowBlur: 10,
                        shadowColor: 'rgba(0, 0, 0, 0.3)'
                    }
                },
                emphasis: {
                    lineStyle: {
                        color: '#000',
                        width: 10,
                        label: {
                    normal: {
                        position: 'right',
                        formatter : function (params) {
                                return ''+ params.name[0];

                            }
                    }
                }
                        
                    }
                }
            }
        ]
    };

    
    

    
    
    myChart.setOption(option);

    </script>




	</body>
</html>
