<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>approve new compound reference link</title>


<script>


	function myJSONQuest_Mod_synonyms_lookup (myQuerySmile) {
				var webQueryPath = 'https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/smiles/' + encodeURIComponent(myQuerySmile) + '/synonyms/JSON';
				
			var Httpreq = new XMLHttpRequest(); // a new request
			Httpreq.open("GET",webQueryPath,false);
			Httpreq.send(null);
			return Httpreq.responseText;
				
				}
				
	function myJSONQuest_Mod_CIDs_lookup (myQuerySmile) {
				var webQueryPath = 'https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/smiles/' + encodeURIComponent(myQuerySmile) + '/cids/JSON';
				
			var Httpreq = new XMLHttpRequest(); // a new request
			Httpreq.open("GET",webQueryPath,false);
			Httpreq.send(null);
			return Httpreq.responseText;
				
				}


				
    function getJSMolScript(myQuerySmile)  {
				var webQueryPath = 'https://chemapps.stolaf.edu/jmol/jmol.php?model=' + myQuerySmile + '&inline';
				
			
			return webQueryPath;
				
				}
				
				
    
    function searchReferenceForJoinWithCompound(){
				window.location = '/LibraryReferences/searchReferenceForJoinWithCompound?compoundID=${currentCompound.id}&newQuery=true';
    }

</script>


</head>
<body>

<h1> <strong>Approve the addition of the following reference </strong></h1>


<g:each var="oneReference" in="${citationReferences}">
            <div id="pending_list" class="onTop">
                    <g:render template="/libraryReferences/ReferenceRendering" model="['citationReference':oneReference]" />
            </div>
            
        </g:each>

        
        
<h1> <strong> to this compound entry </strong></h1>
<div id="PubChemPane">
    <br/>
    <img src="https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/smiles/${currentCompound.smiles}/PNG" alt="chemical compound" style="width:304px;height:228px;">
    <br/>
    <p></br></p>
    <hr>
</div>

<b>ID:</b> ${currentCompound.id}

</br>
<b>InChIKey:</b> ${currentCompound.inChIKey}</br> 
<b>SMILES:</b> ${currentCompound.smiles}  
<br>


<a href="/LibraryReferences/saveApprovedReferenceLinktoCompound" class="btn btn-default">register the reference to compound</a>

<h1><strong>existing reference list:</strong></h1>
 <g:each var="oneReference" in="${currentCompound.citationReferences}">
            <div id="pending_list" class="onTop">
                    <g:render template="/libraryReferences/ReferenceRendering" model="['citationReference':oneReference]" />
            </div>
            
        </g:each>




<br/>

<h1> <strong> compounds PubChem info </strong></h1>

<g:render template="/compound/PubChemRecord" model="['currentCompound':currentCompound]" />



</body>
</html>
