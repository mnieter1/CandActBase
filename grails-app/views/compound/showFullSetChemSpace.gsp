<!DOCTYPE html>
<%! import grails.converters.JSON %>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>CandActCFTR Seed Content ChemSpace</title>
		<asset:javascript src="echarts.min.js"/>
	</head>
	<body>

		<H1>CandActCFTR Seed Compounds ChemSpace</H1>

<hr>


<!-- preparing a DOM with width and height for ECharts -->
    <div id="main" style="width:1200px; height:1200px;"></div>
    
      <p id="intersection">
      <br><br><br><br><br>
      </p>
      <hr>
    <div id="mainCircle" style="width:1000px; height:1000px;"></div>
      
    <p id="demoToReport"></p>
    <p id="demoToReportJustAValue">say something</p>
    <script>
    //checking out the model to object conversion to load the data from the database
    var theJSONString = '${allCompounds as JSON}';
    var fixedQuotesInStrings = theJSONString.replace(new RegExp('&quot;', 'gi'), '"');
    var compoundList  = JSON.parse(fixedQuotesInStrings);

    //checking out the model to object conversion to load the data from the database
    var theJSONString = '${allBiologicalDescriptors as JSON}';
    var fixedQuotesInStrings = theJSONString.replace(new RegExp('&quot;', 'gi'), '"');
    var biologicalDescriptorsList  = JSON.parse(fixedQuotesInStrings);

    //checking out the model to object conversion to load the data from the database
    var theJSONString = '${allChemicalDescriptors as JSON}';
    var fixedQuotesInStrings = theJSONString.replace(new RegExp('&quot;', 'gi'), '"');
    var chemicalDescriptorsList  = JSON.parse(fixedQuotesInStrings);
   
    
    
 /*   
      str = JSON.stringify(compoundList[0], null, 4); // (Optional) beautiful indented output.
    console.log(str); // Logs output to dev tools console.
    alert(str); // Displays output using window.alert()
    
    
    str = JSON.stringify(biologicalDescriptorsList[0].cftrRelevance, null, 4); // (Optional) beautiful indented output.
    console.log(str); // Logs output to dev tools console.
    alert(str); // Displays output using window.alert()
    
    
    str = JSON.stringify(chemicalDescriptorsList[0].pca1, null, 4); // (Optional) beautiful indented output.
    console.log(str); // Logs output to dev tools console.
    alert(str); // Displays output using window.alert()
    

    document.getElementById("demoToReportJustAValue").innerHTML = "first JSON entry id is " + compoundList[0].id + " and smiles" + compoundList[0].smiles;
*/
    </script>

    <script type="text/javascript">
        // based on prepared DOM, initialize echarts instance


    
    
    
    
    // now make the required columns out of the provided json
   // first remove the entries with a missing entry in the coordinates
   var offsetcounter = 0;
   for (let x = 0; x < compoundList.length; x++) {  
    //strOut = JSON.stringify(compoundList[x], null, 4); // (Optional) beautiful indented output.
    //    console.log(strOut); // Logs output to dev tools console.
    if (chemicalDescriptorsList[x] == undefined){   
     //   str = JSON.stringify(compoundList[x], null, 4); // (Optional) beautiful indented output.
     //   console.log(str); // Logs output to dev tools console.
     //   alert(str); // Displays output using window.alert()
        
        chemicalDescriptorsList.splice((x-offsetcounter), 1);
        biologicalDescriptorsList.splice((x-offsetcounter), 1);
        compoundList.splice((x-offsetcounter), 1);
        offsetcounter = offsetcounter+1;
    }
   }
    
    //find all the different versions of activity classes
    // "orderOfInteraction": "unknown",
    // "cftrRelevance": "CFTR corrector",
    // "influenceOnCftrFunction": "enhances CFTR function",
    // "subcellularCompartment": "several"
    
    
    var uniqueActivityClassesC = [];
    var allActivityClassesC = [];
    for (let x = 0; x < compoundList.length; x++) {
        
        // define unique set
        if(uniqueActivityClassesC.indexOf(biologicalDescriptorsList[x].cftrRelevance.toString()) == -1){
        
            uniqueActivityClassesC.push(biologicalDescriptorsList[x].cftrRelevance.toString());
        }
        // collect them all
        allActivityClassesC.push(biologicalDescriptorsList[x].cftrRelevance.toString());
    }
    
    
    
    //first specify where the entries are for each class creating index arrays
    var indexContainerForSeriesC = [];
    var aSingleIndexSeriesC = [];
    
    for (let y = 0; y < uniqueActivityClassesC.length; y++) {
        aSingleIndexSeriesC = [];
        for (let x = 0; x < allActivityClassesC.length; x++) {
            // check if current class is a match
            if(allActivityClassesC[x] == uniqueActivityClassesC[y]){
                // seems to match so remember the position
                aSingleIndexSeriesC.push(x);
            }
        
        }
        indexContainerForSeriesC.push(aSingleIndexSeriesC);
    }
    
    // now that we got the indices of our series sets we can extract the coodinates and feed them into the echarts required object
    var currentChemSpaceCoordsC = [];
    var onePositionC = [];
    var seriesDefinitionsC = [];
    var singleSeriesC = {};
    
    for (let y = 0; y < uniqueActivityClassesC.length; y++) {
        // based on the class definition we collect the required data and stuff it into the container to hold a single series, afterwards we push this into the collector for all series
        currentChemSpaceCoordsC = [];
    
        for (let x = 0; x < indexContainerForSeriesC[y].length; x++) {
            
            // make 2D array
            onePositionC = [];

            // works but not the one below with indices probably because of missing coordinates?!?
            //onePositionC.push(indexContainerForSeriesC[y][x]);
            //onePositionC.push(indexContainerForSeriesC[y][x]);
            
            onePositionC.push(chemicalDescriptorsList[indexContainerForSeriesC[y][x]].pca1);
            onePositionC.push(chemicalDescriptorsList[indexContainerForSeriesC[y][x]].pca2);
            currentChemSpaceCoordsC.push(onePositionC);
        }   
        // define the container with name and coordinates
        singleSeriesC =
        {
            name:uniqueActivityClassesC[y],
            type:'scatter',
            data: currentChemSpaceCoordsC
            
        }
        seriesDefinitionsC.push(singleSeriesC);
    }
    
    
    
    
        var myChart = echarts.init(document.getElementById('main'));
        




var option = {
    title : {
        text: 'CandActCFTR ChemSpace',
        subtext: 'CFTR relevance'
    },
    grid: {
        left: '3%',
        right: '7%',
        bottom: '3%',
        containLabel: true
    },
    tooltip : {
        // trigger: 'axis',
        showDelay : 0,
        formatter : function (params) {
            if (params.value.length > 1) {
                return params.seriesName + ' :<br/>'
                + params.value[0] + ' '
                + params.value[1] + ' ';
            }
            else {
                return params.seriesName + ' :<br/>'
                + params.name + ' : '
                + params.value + ' ';
            }
        },
        axisPointer:{
            show: true,
            type : 'cross',
            lineStyle: {
                type : 'dashed',
                width : 1
            }
        }
    },
    toolbox: {
        feature: {
            dataZoom: {},
            brush: {
                type: ['rect', 'polygon', 'clear']
            }
        }
    },
    brush: {
    },
    legend: {
//         data: ['some','others', 'chemSpace'],
        type:'scroll',
        right: 10,
        top: 20,
        bottom:20,
        orient:'vertical',
        data: uniqueActivityClassesC
    },
    xAxis : [
        {
            type : 'value',
            scale:true,
            axisLabel : {
                formatter: '{value}'
            },
            splitLine: {
                show: false
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            scale:true,
            axisLabel : {
                formatter: '{value}'
            },
            splitLine: {
                show: false
            }
        }
    ],
    series : seriesDefinitionsC
       
};

        
        
        
        // use configuration item and data specified to show chart
        myChart.setOption(option);
    
    
    
    
    </script>


    
    
    
    
    

<br>

<script>
var myChart = echarts.init(document.getElementById('mainCircle'));
myChart.showLoading();

    myChart.hideLoading();


   var graph = {
       "nodes":[ {
  "id" : 1,
  "name" : "Dey I 2016",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 2,
  "name" : "Wang G 2015",
  "count" : 4,
  "symbolSize" : 13.862943611198906
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 3,
  "name" : "Wang W 2016",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 4,
  "name" : "de Carvalho A 2002",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 5,
  "name" : "Chen L 2015",
  "count" : 5,
  "symbolSize" : 16.094379124341003
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 6,
  "name" : "Lin W 2016",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 7,
  "name" : "Robert R 2010",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 8,
  "name" : "Sohma Y 2013",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 9,
  "name" : "Berger A 2005",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 10,
  "name" : "Hall J 2016",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 11,
  "name" : "Phuan P 2011",
  "count" : 12,
  "symbolSize" : 24.849066497880003
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 12,
  "name" : "Robert R 2007",
  "count" : 7,
  "symbolSize" : 19.45910149055313
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 13,
  "name" : "Matthes E 2016",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 14,
  "name" : "Dragomir A 2004",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 15,
  "name" : "Bachmann A 2000",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 16,
  "name" : "Baker M 2004",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 17,
  "name" : "Song Y 2004",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 18,
  "name" : "Dekkers J 2016",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 19,
  "name" : "Hamdaoui N 2011",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 20,
  "name" : "Zhang Y 2014",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 21,
  "name" : "Suaud L 2002",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 22,
  "name" : "Xu L 2008",
  "count" : 6,
  "symbolSize" : 17.91759469228055
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 23,
  "name" : "Cendret V 2015",
  "count" : 12,
  "symbolSize" : 24.849066497880003
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 24,
  "name" : "Chao P 2009",
  "count" : 10,
  "symbolSize" : 23.02585092994046
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 25,
  "name" : "Galietta L 2001",
  "count" : 221,
  "symbolSize" : 53.98162701517752
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 26,
  "name" : "Lubamba B 2009",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 27,
  "name" : "Ai T 2004",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 28,
  "name" : "Melani R 2010",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 29,
  "name" : "Dechecchi M 2007",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 30,
  "name" : "Schmidt B 2016",
  "count" : 24,
  "symbolSize" : 31.780538303479457
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 31,
  "name" : "Namkung W 2013",
  "count" : 5,
  "symbolSize" : 16.094379124341003
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 32,
  "name" : "Zhang S 2013",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 33,
  "name" : "Zhang S 2011",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 34,
  "name" : "Tosco A 2016",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 35,
  "name" : "Norez C 2006",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 36,
  "name" : "Sampson H 2011",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 37,
  "name" : "Conger B 2013",
  "count" : 6,
  "symbolSize" : 17.91759469228055
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 38,
  "name" : "Cui G 2016",
  "count" : 8,
  "symbolSize" : 20.79441541679836
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 39,
  "name" : "Park J 2016",
  "count" : 18,
  "symbolSize" : 28.903717578961647
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 40,
  "name" : "Andersson C 2003",
  "count" : 4,
  "symbolSize" : 13.862943611198906
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 41,
  "name" : "Al-Nakkash L 2006",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 42,
  "name" : "Rowe S 2010",
  "count" : 6,
  "symbolSize" : 17.91759469228055
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 43,
  "name" : "van Goor 2005",
  "count" : 7,
  "symbolSize" : 19.45910149055313
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 44,
  "name" : "Carlile G 2007",
  "count" : 30,
  "symbolSize" : 34.011973816621555
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 45,
  "name" : "Carlile G 2016",
  "count" : 8,
  "symbolSize" : 20.79441541679836
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 46,
  "name" : "Egan M 2004",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 47,
  "name" : "Sondo E 2011",
  "count" : 5,
  "symbolSize" : 16.094379124341003
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 48,
  "name" : "Solomon G 2015",
  "count" : 9,
  "symbolSize" : 21.972245773362197
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 49,
  "name" : "Bernard K 2009",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 50,
  "name" : "Harada K 2007",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 51,
  "name" : "Lim C 2008",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 52,
  "name" : "Noel S 2008",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 53,
  "name" : "Jai Y 2015",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 54,
  "name" : "Al-Nakkash L 2008",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 55,
  "name" : "Wang X 2018",
  "count" : 22,
  "symbolSize" : 30.91042453358316
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 56,
  "name" : "Carlile G 2012",
  "count" : 6,
  "symbolSize" : 17.91759469228055
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 57,
  "name" : "Noel S 2007",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 58,
  "name" : "Norez C 2009",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 59,
  "name" : "Dong Z 2015",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 60,
  "name" : "Anjos S 2012",
  "count" : 6,
  "symbolSize" : 17.91759469228055
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 61,
  "name" : "Yu YC 2011",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 62,
  "name" : "Miki H 2010",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 63,
  "name" : "Zhang D 2012",
  "count" : 4,
  "symbolSize" : 13.862943611198906
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 64,
  "name" : "Boinot C 2014",
  "count" : 5,
  "symbolSize" : 16.094379124341003
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 65,
  "name" : "Pyle L 2009",
  "count" : 21,
  "symbolSize" : 30.44522437723423
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 66,
  "name" : "Hutt D 2010",
  "count" : 4,
  "symbolSize" : 13.862943611198906
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 67,
  "name" : "Cateni F 2009",
  "count" : 19,
  "symbolSize" : 29.444389791664403
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 68,
  "name" : "Wang W 2007",
  "count" : 4,
  "symbolSize" : 13.862943611198906
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 69,
  "name" : "Yang H 2003",
  "count" : 12,
  "symbolSize" : 24.849066497880003
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 70,
  "name" : "Wang G 2015 II",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 71,
  "name" : "Dhooghe B 2015",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 72,
  "name" : "Leonard A 2012",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 73,
  "name" : "Odolczyk N 2013",
  "count" : 14,
  "symbolSize" : 26.390573296152585
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 74,
  "name" : "Pedemonte N 2005",
  "count" : 31,
  "symbolSize" : 34.33987204485146
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 75,
  "name" : "Grubb B 2006",
  "count" : 4,
  "symbolSize" : 13.862943611198906
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 76,
  "name" : "Stefano D 2014",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 77,
  "name" : "Carlile G 2015",
  "count" : 2632,
  "symbolSize" : 78.75499292445208
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 78,
  "name" : "Wang Y 2006",
  "count" : 6,
  "symbolSize" : 17.91759469228055
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 79,
  "name" : "Moran O 2002",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 80,
  "name" : "Schmidt A 2009",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 81,
  "name" : "Cartiera M 2010",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 82,
  "name" : "Lipecka J 2006",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 83,
  "name" : "Moran O 2005",
  "count" : 1,
  "symbolSize" : 0.0
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 84,
  "name" : "Lim M 2004",
  "count" : 5,
  "symbolSize" : 16.094379124341003
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 85,
  "name" : "Chiaw P 2010",
  "count" : 3,
  "symbolSize" : 10.986122886681098
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 }, {
  "id" : 86,
  "name" : "Stevers L 2016",
  "count" : 2,
  "symbolSize" : 6.931471805599453
,   "value" : 50,   "itemStyle": null,             "x": -418.08344,             "y": 446.8853,             "attributes": {                 "modularity_class": 0             },             "label": {                 "normal": {                     "show": false                 }             },             "category": 0 } ],
    "links": [ {
  "id" : "1",
  "source" : 1,
  "target" : 2,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2",
  "source" : 1,
  "target" : 3,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "7",
  "source" : 1,
  "target" : 8,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "8",
  "source" : 1,
  "target" : 9,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "13",
  "source" : 1,
  "target" : 14,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "14",
  "source" : 1,
  "target" : 15,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "15",
  "source" : 1,
  "target" : 16,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "16",
  "source" : 1,
  "target" : 17,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "17",
  "source" : 1,
  "target" : 18,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "18",
  "source" : 1,
  "target" : 19,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "20",
  "source" : 1,
  "target" : 21,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "23",
  "source" : 1,
  "target" : 24,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "24",
  "source" : 1,
  "target" : 25,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "26",
  "source" : 1,
  "target" : 27,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "27",
  "source" : 1,
  "target" : 28,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "29",
  "source" : 1,
  "target" : 30,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "31",
  "source" : 1,
  "target" : 32,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "36",
  "source" : 1,
  "target" : 37,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "39",
  "source" : 1,
  "target" : 40,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "40",
  "source" : 1,
  "target" : 41,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "41",
  "source" : 1,
  "target" : 42,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "42",
  "source" : 1,
  "target" : 43,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "45",
  "source" : 1,
  "target" : 46,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "47",
  "source" : 1,
  "target" : 48,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "48",
  "source" : 1,
  "target" : 49,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "49",
  "source" : 1,
  "target" : 50,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "50",
  "source" : 1,
  "target" : 51,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "52",
  "source" : 1,
  "target" : 53,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "53",
  "source" : 1,
  "target" : 54,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "56",
  "source" : 1,
  "target" : 57,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "58",
  "source" : 1,
  "target" : 59,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "60",
  "source" : 1,
  "target" : 61,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "64",
  "source" : 1,
  "target" : 65,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "66",
  "source" : 1,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "67",
  "source" : 1,
  "target" : 68,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "69",
  "source" : 1,
  "target" : 70,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "70",
  "source" : 1,
  "target" : 71,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "74",
  "source" : 1,
  "target" : 75,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "76",
  "source" : 1,
  "target" : 77,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "78",
  "source" : 1,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "79",
  "source" : 1,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "80",
  "source" : 1,
  "target" : 81,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "81",
  "source" : 1,
  "target" : 82,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "82",
  "source" : 1,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "83",
  "source" : 1,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "86",
  "source" : 2,
  "target" : 3,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "89",
  "source" : 2,
  "target" : 6,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "91",
  "source" : 2,
  "target" : 8,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "92",
  "source" : 2,
  "target" : 9,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "97",
  "source" : 2,
  "target" : 14,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "100",
  "source" : 2,
  "target" : 17,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "101",
  "source" : 2,
  "target" : 18,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "113",
  "source" : 2,
  "target" : 30,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "121",
  "source" : 2,
  "target" : 38,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "126",
  "source" : 2,
  "target" : 43,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "129",
  "source" : 2,
  "target" : 46,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "131",
  "source" : 2,
  "target" : 48,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "132",
  "source" : 2,
  "target" : 49,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "133",
  "source" : 2,
  "target" : 50,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "142",
  "source" : 2,
  "target" : 59,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "144",
  "source" : 2,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "151",
  "source" : 2,
  "target" : 68,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "153",
  "source" : 2,
  "target" : 70,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "158",
  "source" : 2,
  "target" : 75,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "160",
  "source" : 2,
  "target" : 77,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "164",
  "source" : 2,
  "target" : 81,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "165",
  "source" : 2,
  "target" : 82,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "172",
  "source" : 3,
  "target" : 6,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "174",
  "source" : 3,
  "target" : 8,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "175",
  "source" : 3,
  "target" : 9,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "179",
  "source" : 3,
  "target" : 13,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "180",
  "source" : 3,
  "target" : 14,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "183",
  "source" : 3,
  "target" : 17,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "184",
  "source" : 3,
  "target" : 18,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "196",
  "source" : 3,
  "target" : 30,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "204",
  "source" : 3,
  "target" : 38,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "209",
  "source" : 3,
  "target" : 43,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "212",
  "source" : 3,
  "target" : 46,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "214",
  "source" : 3,
  "target" : 48,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "215",
  "source" : 3,
  "target" : 49,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "216",
  "source" : 3,
  "target" : 50,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "225",
  "source" : 3,
  "target" : 59,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "227",
  "source" : 3,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "234",
  "source" : 3,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "236",
  "source" : 3,
  "target" : 70,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "241",
  "source" : 3,
  "target" : 75,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "243",
  "source" : 3,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "247",
  "source" : 3,
  "target" : 81,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "248",
  "source" : 3,
  "target" : 82,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "296",
  "source" : 4,
  "target" : 48,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "325",
  "source" : 4,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "327",
  "source" : 4,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "359",
  "source" : 5,
  "target" : 30,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "363",
  "source" : 5,
  "target" : 34,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "405",
  "source" : 5,
  "target" : 76,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "406",
  "source" : 5,
  "target" : 77,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "422",
  "source" : 6,
  "target" : 13,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "427",
  "source" : 6,
  "target" : 18,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "439",
  "source" : 6,
  "target" : 30,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "447",
  "source" : 6,
  "target" : 38,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "457",
  "source" : 6,
  "target" : 48,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "477",
  "source" : 6,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "486",
  "source" : 6,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "518",
  "source" : 7,
  "target" : 30,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "530",
  "source" : 7,
  "target" : 42,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "531",
  "source" : 7,
  "target" : 43,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "535",
  "source" : 7,
  "target" : 47,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "544",
  "source" : 7,
  "target" : 56,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "548",
  "source" : 7,
  "target" : 60,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "565",
  "source" : 7,
  "target" : 77,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "566",
  "source" : 7,
  "target" : 78,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "573",
  "source" : 7,
  "target" : 85,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "575",
  "source" : 8,
  "target" : 9,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "580",
  "source" : 8,
  "target" : 14,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "581",
  "source" : 8,
  "target" : 15,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "582",
  "source" : 8,
  "target" : 16,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "583",
  "source" : 8,
  "target" : 17,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "584",
  "source" : 8,
  "target" : 18,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "587",
  "source" : 8,
  "target" : 21,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "590",
  "source" : 8,
  "target" : 24,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "591",
  "source" : 8,
  "target" : 25,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "593",
  "source" : 8,
  "target" : 27,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "594",
  "source" : 8,
  "target" : 28,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "596",
  "source" : 8,
  "target" : 30,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "603",
  "source" : 8,
  "target" : 37,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "606",
  "source" : 8,
  "target" : 40,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "607",
  "source" : 8,
  "target" : 41,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "608",
  "source" : 8,
  "target" : 42,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "609",
  "source" : 8,
  "target" : 43,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "612",
  "source" : 8,
  "target" : 46,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "614",
  "source" : 8,
  "target" : 48,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "615",
  "source" : 8,
  "target" : 49,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "616",
  "source" : 8,
  "target" : 50,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "617",
  "source" : 8,
  "target" : 51,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "620",
  "source" : 8,
  "target" : 54,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "623",
  "source" : 8,
  "target" : 57,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "625",
  "source" : 8,
  "target" : 59,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "627",
  "source" : 8,
  "target" : 61,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "631",
  "source" : 8,
  "target" : 65,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "633",
  "source" : 8,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "634",
  "source" : 8,
  "target" : 68,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "636",
  "source" : 8,
  "target" : 70,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "641",
  "source" : 8,
  "target" : 75,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "643",
  "source" : 8,
  "target" : 77,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "645",
  "source" : 8,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "646",
  "source" : 8,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "647",
  "source" : 8,
  "target" : 81,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "648",
  "source" : 8,
  "target" : 82,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "649",
  "source" : 8,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "650",
  "source" : 8,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "657",
  "source" : 9,
  "target" : 14,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "658",
  "source" : 9,
  "target" : 15,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "659",
  "source" : 9,
  "target" : 16,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "660",
  "source" : 9,
  "target" : 17,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "661",
  "source" : 9,
  "target" : 18,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "664",
  "source" : 9,
  "target" : 21,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "667",
  "source" : 9,
  "target" : 24,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "668",
  "source" : 9,
  "target" : 25,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "670",
  "source" : 9,
  "target" : 27,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "671",
  "source" : 9,
  "target" : 28,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "673",
  "source" : 9,
  "target" : 30,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "680",
  "source" : 9,
  "target" : 37,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "683",
  "source" : 9,
  "target" : 40,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "684",
  "source" : 9,
  "target" : 41,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "685",
  "source" : 9,
  "target" : 42,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "686",
  "source" : 9,
  "target" : 43,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "689",
  "source" : 9,
  "target" : 46,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "691",
  "source" : 9,
  "target" : 48,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "692",
  "source" : 9,
  "target" : 49,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "693",
  "source" : 9,
  "target" : 50,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "694",
  "source" : 9,
  "target" : 51,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "697",
  "source" : 9,
  "target" : 54,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "700",
  "source" : 9,
  "target" : 57,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "702",
  "source" : 9,
  "target" : 59,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "704",
  "source" : 9,
  "target" : 61,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "708",
  "source" : 9,
  "target" : 65,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "710",
  "source" : 9,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "711",
  "source" : 9,
  "target" : 68,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "713",
  "source" : 9,
  "target" : 70,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "718",
  "source" : 9,
  "target" : 75,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "720",
  "source" : 9,
  "target" : 77,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "722",
  "source" : 9,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "723",
  "source" : 9,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "724",
  "source" : 9,
  "target" : 81,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "725",
  "source" : 9,
  "target" : 82,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "726",
  "source" : 9,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "727",
  "source" : 9,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "898",
  "source" : 12,
  "target" : 30,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "912",
  "source" : 12,
  "target" : 44,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "916",
  "source" : 12,
  "target" : 48,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "945",
  "source" : 12,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "959",
  "source" : 13,
  "target" : 18,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "971",
  "source" : 13,
  "target" : 30,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "979",
  "source" : 13,
  "target" : 38,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "989",
  "source" : 13,
  "target" : 48,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1000",
  "source" : 13,
  "target" : 59,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1005",
  "source" : 13,
  "target" : 64,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1014",
  "source" : 13,
  "target" : 73,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1027",
  "source" : 13,
  "target" : 86,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1030",
  "source" : 14,
  "target" : 17,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1031",
  "source" : 14,
  "target" : 18,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1043",
  "source" : 14,
  "target" : 30,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1056",
  "source" : 14,
  "target" : 43,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1059",
  "source" : 14,
  "target" : 46,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1061",
  "source" : 14,
  "target" : 48,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1062",
  "source" : 14,
  "target" : 49,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1063",
  "source" : 14,
  "target" : 50,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1072",
  "source" : 14,
  "target" : 59,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1074",
  "source" : 14,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1081",
  "source" : 14,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1083",
  "source" : 14,
  "target" : 70,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1088",
  "source" : 14,
  "target" : 75,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1090",
  "source" : 14,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1094",
  "source" : 14,
  "target" : 81,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1095",
  "source" : 14,
  "target" : 82,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1100",
  "source" : 15,
  "target" : 16,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1102",
  "source" : 15,
  "target" : 18,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1105",
  "source" : 15,
  "target" : 21,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1108",
  "source" : 15,
  "target" : 24,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1109",
  "source" : 15,
  "target" : 25,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1111",
  "source" : 15,
  "target" : 27,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1112",
  "source" : 15,
  "target" : 28,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1114",
  "source" : 15,
  "target" : 30,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1121",
  "source" : 15,
  "target" : 37,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1124",
  "source" : 15,
  "target" : 40,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1125",
  "source" : 15,
  "target" : 41,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1126",
  "source" : 15,
  "target" : 42,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1127",
  "source" : 15,
  "target" : 43,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1135",
  "source" : 15,
  "target" : 51,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1138",
  "source" : 15,
  "target" : 54,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1141",
  "source" : 15,
  "target" : 57,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1145",
  "source" : 15,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1149",
  "source" : 15,
  "target" : 65,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1151",
  "source" : 15,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1152",
  "source" : 15,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1161",
  "source" : 15,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1163",
  "source" : 15,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1164",
  "source" : 15,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1167",
  "source" : 15,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1168",
  "source" : 15,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1172",
  "source" : 16,
  "target" : 18,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1175",
  "source" : 16,
  "target" : 21,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1178",
  "source" : 16,
  "target" : 24,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1179",
  "source" : 16,
  "target" : 25,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1181",
  "source" : 16,
  "target" : 27,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1182",
  "source" : 16,
  "target" : 28,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1184",
  "source" : 16,
  "target" : 30,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1191",
  "source" : 16,
  "target" : 37,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1194",
  "source" : 16,
  "target" : 40,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1195",
  "source" : 16,
  "target" : 41,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1196",
  "source" : 16,
  "target" : 42,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1197",
  "source" : 16,
  "target" : 43,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1205",
  "source" : 16,
  "target" : 51,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1208",
  "source" : 16,
  "target" : 54,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1211",
  "source" : 16,
  "target" : 57,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1215",
  "source" : 16,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1219",
  "source" : 16,
  "target" : 65,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1221",
  "source" : 16,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1222",
  "source" : 16,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1231",
  "source" : 16,
  "target" : 77,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1233",
  "source" : 16,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1234",
  "source" : 16,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1237",
  "source" : 16,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1238",
  "source" : 16,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1241",
  "source" : 17,
  "target" : 18,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1253",
  "source" : 17,
  "target" : 30,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1263",
  "source" : 17,
  "target" : 40,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1266",
  "source" : 17,
  "target" : 43,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1269",
  "source" : 17,
  "target" : 46,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1271",
  "source" : 17,
  "target" : 48,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1272",
  "source" : 17,
  "target" : 49,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1273",
  "source" : 17,
  "target" : 50,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1282",
  "source" : 17,
  "target" : 59,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1284",
  "source" : 17,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1291",
  "source" : 17,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1293",
  "source" : 17,
  "target" : 70,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1298",
  "source" : 17,
  "target" : 75,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1300",
  "source" : 17,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1304",
  "source" : 17,
  "target" : 81,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1305",
  "source" : 17,
  "target" : 82,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1307",
  "source" : 17,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1312",
  "source" : 18,
  "target" : 21,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1315",
  "source" : 18,
  "target" : 24,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1316",
  "source" : 18,
  "target" : 25,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1318",
  "source" : 18,
  "target" : 27,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1319",
  "source" : 18,
  "target" : 28,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1321",
  "source" : 18,
  "target" : 30,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1328",
  "source" : 18,
  "target" : 37,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1329",
  "source" : 18,
  "target" : 38,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1331",
  "source" : 18,
  "target" : 40,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1332",
  "source" : 18,
  "target" : 41,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1333",
  "source" : 18,
  "target" : 42,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1334",
  "source" : 18,
  "target" : 43,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1337",
  "source" : 18,
  "target" : 46,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1339",
  "source" : 18,
  "target" : 48,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1340",
  "source" : 18,
  "target" : 49,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1341",
  "source" : 18,
  "target" : 50,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1342",
  "source" : 18,
  "target" : 51,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1345",
  "source" : 18,
  "target" : 54,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1348",
  "source" : 18,
  "target" : 57,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1350",
  "source" : 18,
  "target" : 59,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1352",
  "source" : 18,
  "target" : 61,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1356",
  "source" : 18,
  "target" : 65,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1358",
  "source" : 18,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1359",
  "source" : 18,
  "target" : 68,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1361",
  "source" : 18,
  "target" : 70,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1366",
  "source" : 18,
  "target" : 75,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1368",
  "source" : 18,
  "target" : 77,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1370",
  "source" : 18,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1371",
  "source" : 18,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1372",
  "source" : 18,
  "target" : 81,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1373",
  "source" : 18,
  "target" : 82,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1374",
  "source" : 18,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1375",
  "source" : 18,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1388",
  "source" : 19,
  "target" : 30,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1390",
  "source" : 19,
  "target" : 32,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1411",
  "source" : 19,
  "target" : 53,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1429",
  "source" : 19,
  "target" : 71,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1435",
  "source" : 19,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1513",
  "source" : 21,
  "target" : 24,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1514",
  "source" : 21,
  "target" : 25,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1516",
  "source" : 21,
  "target" : 27,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1517",
  "source" : 21,
  "target" : 28,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1519",
  "source" : 21,
  "target" : 30,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1526",
  "source" : 21,
  "target" : 37,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1529",
  "source" : 21,
  "target" : 40,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1530",
  "source" : 21,
  "target" : 41,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1531",
  "source" : 21,
  "target" : 42,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1532",
  "source" : 21,
  "target" : 43,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1540",
  "source" : 21,
  "target" : 51,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1543",
  "source" : 21,
  "target" : 54,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1546",
  "source" : 21,
  "target" : 57,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1550",
  "source" : 21,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1554",
  "source" : 21,
  "target" : 65,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1556",
  "source" : 21,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1557",
  "source" : 21,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1566",
  "source" : 21,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1568",
  "source" : 21,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1569",
  "source" : 21,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1572",
  "source" : 21,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1573",
  "source" : 21,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1590",
  "source" : 22,
  "target" : 37,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1630",
  "source" : 22,
  "target" : 77,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1642",
  "source" : 23,
  "target" : 26,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1646",
  "source" : 23,
  "target" : 30,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1651",
  "source" : 23,
  "target" : 35,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1668",
  "source" : 23,
  "target" : 52,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1674",
  "source" : 23,
  "target" : 58,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1680",
  "source" : 23,
  "target" : 64,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1688",
  "source" : 23,
  "target" : 72,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1703",
  "source" : 24,
  "target" : 25,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1705",
  "source" : 24,
  "target" : 27,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1706",
  "source" : 24,
  "target" : 28,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1708",
  "source" : 24,
  "target" : 30,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1715",
  "source" : 24,
  "target" : 37,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1718",
  "source" : 24,
  "target" : 40,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1719",
  "source" : 24,
  "target" : 41,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1720",
  "source" : 24,
  "target" : 42,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1721",
  "source" : 24,
  "target" : 43,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1729",
  "source" : 24,
  "target" : 51,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1732",
  "source" : 24,
  "target" : 54,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1735",
  "source" : 24,
  "target" : 57,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1739",
  "source" : 24,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1743",
  "source" : 24,
  "target" : 65,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1745",
  "source" : 24,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1746",
  "source" : 24,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1755",
  "source" : 24,
  "target" : 77,
  "overlap" : 4
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1757",
  "source" : 24,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1758",
  "source" : 24,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1761",
  "source" : 24,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1762",
  "source" : 24,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1766",
  "source" : 25,
  "target" : 27,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1767",
  "source" : 25,
  "target" : 28,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1769",
  "source" : 25,
  "target" : 30,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1776",
  "source" : 25,
  "target" : 37,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1779",
  "source" : 25,
  "target" : 40,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1780",
  "source" : 25,
  "target" : 41,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1781",
  "source" : 25,
  "target" : 42,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1782",
  "source" : 25,
  "target" : 43,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1790",
  "source" : 25,
  "target" : 51,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1793",
  "source" : 25,
  "target" : 54,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1796",
  "source" : 25,
  "target" : 57,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1800",
  "source" : 25,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1804",
  "source" : 25,
  "target" : 65,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1806",
  "source" : 25,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1807",
  "source" : 25,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1816",
  "source" : 25,
  "target" : 77,
  "overlap" : 5
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1818",
  "source" : 25,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1819",
  "source" : 25,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1822",
  "source" : 25,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1823",
  "source" : 25,
  "target" : 84,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1829",
  "source" : 26,
  "target" : 30,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1834",
  "source" : 26,
  "target" : 35,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1851",
  "source" : 26,
  "target" : 52,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1857",
  "source" : 26,
  "target" : 58,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1863",
  "source" : 26,
  "target" : 64,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1871",
  "source" : 26,
  "target" : 72,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1886",
  "source" : 27,
  "target" : 28,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1888",
  "source" : 27,
  "target" : 30,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1895",
  "source" : 27,
  "target" : 37,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1898",
  "source" : 27,
  "target" : 40,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1899",
  "source" : 27,
  "target" : 41,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1900",
  "source" : 27,
  "target" : 42,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1901",
  "source" : 27,
  "target" : 43,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1909",
  "source" : 27,
  "target" : 51,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1912",
  "source" : 27,
  "target" : 54,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1915",
  "source" : 27,
  "target" : 57,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1919",
  "source" : 27,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1923",
  "source" : 27,
  "target" : 65,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1925",
  "source" : 27,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1926",
  "source" : 27,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1935",
  "source" : 27,
  "target" : 77,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1937",
  "source" : 27,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1938",
  "source" : 27,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1941",
  "source" : 27,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1942",
  "source" : 27,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1946",
  "source" : 28,
  "target" : 30,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1953",
  "source" : 28,
  "target" : 37,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1956",
  "source" : 28,
  "target" : 40,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1957",
  "source" : 28,
  "target" : 41,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1958",
  "source" : 28,
  "target" : 42,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1959",
  "source" : 28,
  "target" : 43,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1967",
  "source" : 28,
  "target" : 51,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1970",
  "source" : 28,
  "target" : 54,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1973",
  "source" : 28,
  "target" : 57,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1977",
  "source" : 28,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1981",
  "source" : 28,
  "target" : 65,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1983",
  "source" : 28,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1984",
  "source" : 28,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1993",
  "source" : 28,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1995",
  "source" : 28,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1996",
  "source" : 28,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "1999",
  "source" : 28,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2000",
  "source" : 28,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2030",
  "source" : 29,
  "target" : 57,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2052",
  "source" : 29,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2061",
  "source" : 30,
  "target" : 32,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2063",
  "source" : 30,
  "target" : 34,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2064",
  "source" : 30,
  "target" : 35,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2066",
  "source" : 30,
  "target" : 37,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2067",
  "source" : 30,
  "target" : 38,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2069",
  "source" : 30,
  "target" : 40,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2070",
  "source" : 30,
  "target" : 41,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2071",
  "source" : 30,
  "target" : 42,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2072",
  "source" : 30,
  "target" : 43,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2073",
  "source" : 30,
  "target" : 44,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2075",
  "source" : 30,
  "target" : 46,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2076",
  "source" : 30,
  "target" : 47,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2077",
  "source" : 30,
  "target" : 48,
  "overlap" : 8
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2078",
  "source" : 30,
  "target" : 49,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2079",
  "source" : 30,
  "target" : 50,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2080",
  "source" : 30,
  "target" : 51,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2081",
  "source" : 30,
  "target" : 52,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2082",
  "source" : 30,
  "target" : 53,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2083",
  "source" : 30,
  "target" : 54,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2085",
  "source" : 30,
  "target" : 56,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2086",
  "source" : 30,
  "target" : 57,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2087",
  "source" : 30,
  "target" : 58,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2088",
  "source" : 30,
  "target" : 59,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2089",
  "source" : 30,
  "target" : 60,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2090",
  "source" : 30,
  "target" : 61,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2093",
  "source" : 30,
  "target" : 64,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2094",
  "source" : 30,
  "target" : 65,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2096",
  "source" : 30,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2097",
  "source" : 30,
  "target" : 68,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2099",
  "source" : 30,
  "target" : 70,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2100",
  "source" : 30,
  "target" : 71,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2101",
  "source" : 30,
  "target" : 72,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2102",
  "source" : 30,
  "target" : 73,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2103",
  "source" : 30,
  "target" : 74,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2104",
  "source" : 30,
  "target" : 75,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2105",
  "source" : 30,
  "target" : 76,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2106",
  "source" : 30,
  "target" : 77,
  "overlap" : 10
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2107",
  "source" : 30,
  "target" : 78,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2108",
  "source" : 30,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2109",
  "source" : 30,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2110",
  "source" : 30,
  "target" : 81,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2111",
  "source" : 30,
  "target" : 82,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2112",
  "source" : 30,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2113",
  "source" : 30,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2114",
  "source" : 30,
  "target" : 85,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2115",
  "source" : 30,
  "target" : 86,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2121",
  "source" : 31,
  "target" : 37,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2124",
  "source" : 31,
  "target" : 40,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2126",
  "source" : 31,
  "target" : 42,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2161",
  "source" : 31,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2191",
  "source" : 32,
  "target" : 53,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2209",
  "source" : 32,
  "target" : 71,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2215",
  "source" : 32,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2256",
  "source" : 33,
  "target" : 65,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2275",
  "source" : 33,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2319",
  "source" : 34,
  "target" : 76,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2320",
  "source" : 34,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2346",
  "source" : 35,
  "target" : 52,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2352",
  "source" : 35,
  "target" : 58,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2358",
  "source" : 35,
  "target" : 64,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2366",
  "source" : 35,
  "target" : 72,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2433",
  "source" : 37,
  "target" : 40,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2434",
  "source" : 37,
  "target" : 41,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2435",
  "source" : 37,
  "target" : 42,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2436",
  "source" : 37,
  "target" : 43,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2444",
  "source" : 37,
  "target" : 51,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2447",
  "source" : 37,
  "target" : 54,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2450",
  "source" : 37,
  "target" : 57,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2454",
  "source" : 37,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2458",
  "source" : 37,
  "target" : 65,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2460",
  "source" : 37,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2461",
  "source" : 37,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2470",
  "source" : 37,
  "target" : 77,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2472",
  "source" : 37,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2473",
  "source" : 37,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2476",
  "source" : 37,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2477",
  "source" : 37,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2478",
  "source" : 37,
  "target" : 85,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2489",
  "source" : 38,
  "target" : 48,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2509",
  "source" : 38,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2518",
  "source" : 38,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2575",
  "source" : 40,
  "target" : 41,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2576",
  "source" : 40,
  "target" : 42,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2577",
  "source" : 40,
  "target" : 43,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2585",
  "source" : 40,
  "target" : 51,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2588",
  "source" : 40,
  "target" : 54,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2591",
  "source" : 40,
  "target" : 57,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2595",
  "source" : 40,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2599",
  "source" : 40,
  "target" : 65,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2601",
  "source" : 40,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2602",
  "source" : 40,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2611",
  "source" : 40,
  "target" : 77,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2613",
  "source" : 40,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2614",
  "source" : 40,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2617",
  "source" : 40,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2618",
  "source" : 40,
  "target" : 84,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2621",
  "source" : 41,
  "target" : 42,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2622",
  "source" : 41,
  "target" : 43,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2630",
  "source" : 41,
  "target" : 51,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2633",
  "source" : 41,
  "target" : 54,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2636",
  "source" : 41,
  "target" : 57,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2640",
  "source" : 41,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2644",
  "source" : 41,
  "target" : 65,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2646",
  "source" : 41,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2647",
  "source" : 41,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2656",
  "source" : 41,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2658",
  "source" : 41,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2659",
  "source" : 41,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2662",
  "source" : 41,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2663",
  "source" : 41,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2666",
  "source" : 42,
  "target" : 43,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2670",
  "source" : 42,
  "target" : 47,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2674",
  "source" : 42,
  "target" : 51,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2677",
  "source" : 42,
  "target" : 54,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2679",
  "source" : 42,
  "target" : 56,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2680",
  "source" : 42,
  "target" : 57,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2683",
  "source" : 42,
  "target" : 60,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2684",
  "source" : 42,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2687",
  "source" : 42,
  "target" : 64,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2688",
  "source" : 42,
  "target" : 65,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2690",
  "source" : 42,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2691",
  "source" : 42,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2696",
  "source" : 42,
  "target" : 73,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2697",
  "source" : 42,
  "target" : 74,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2700",
  "source" : 42,
  "target" : 77,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2701",
  "source" : 42,
  "target" : 78,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2702",
  "source" : 42,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2703",
  "source" : 42,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2706",
  "source" : 42,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2707",
  "source" : 42,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2708",
  "source" : 42,
  "target" : 85,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2712",
  "source" : 43,
  "target" : 46,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2713",
  "source" : 43,
  "target" : 47,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2714",
  "source" : 43,
  "target" : 48,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2715",
  "source" : 43,
  "target" : 49,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2716",
  "source" : 43,
  "target" : 50,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2717",
  "source" : 43,
  "target" : 51,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2720",
  "source" : 43,
  "target" : 54,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2722",
  "source" : 43,
  "target" : 56,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2723",
  "source" : 43,
  "target" : 57,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2725",
  "source" : 43,
  "target" : 59,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2726",
  "source" : 43,
  "target" : 60,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2727",
  "source" : 43,
  "target" : 61,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2731",
  "source" : 43,
  "target" : 65,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2733",
  "source" : 43,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2734",
  "source" : 43,
  "target" : 68,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2736",
  "source" : 43,
  "target" : 70,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2741",
  "source" : 43,
  "target" : 75,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2743",
  "source" : 43,
  "target" : 77,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2744",
  "source" : 43,
  "target" : 78,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2745",
  "source" : 43,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2746",
  "source" : 43,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2747",
  "source" : 43,
  "target" : 81,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2748",
  "source" : 43,
  "target" : 82,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2749",
  "source" : 43,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2750",
  "source" : 43,
  "target" : 84,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2751",
  "source" : 43,
  "target" : 85,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2756",
  "source" : 44,
  "target" : 48,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2773",
  "source" : 44,
  "target" : 65,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2785",
  "source" : 44,
  "target" : 77,
  "overlap" : 15
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2805",
  "source" : 45,
  "target" : 56,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2837",
  "source" : 46,
  "target" : 48,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2838",
  "source" : 46,
  "target" : 49,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2839",
  "source" : 46,
  "target" : 50,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2848",
  "source" : 46,
  "target" : 59,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2850",
  "source" : 46,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2857",
  "source" : 46,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2859",
  "source" : 46,
  "target" : 70,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2864",
  "source" : 46,
  "target" : 75,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2866",
  "source" : 46,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2870",
  "source" : 46,
  "target" : 81,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2871",
  "source" : 46,
  "target" : 82,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2884",
  "source" : 47,
  "target" : 56,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2888",
  "source" : 47,
  "target" : 60,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2892",
  "source" : 47,
  "target" : 64,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2894",
  "source" : 47,
  "target" : 66,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2901",
  "source" : 47,
  "target" : 73,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2902",
  "source" : 47,
  "target" : 74,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2905",
  "source" : 47,
  "target" : 77,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2906",
  "source" : 47,
  "target" : 78,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2913",
  "source" : 47,
  "target" : 85,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2915",
  "source" : 48,
  "target" : 49,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2916",
  "source" : 48,
  "target" : 50,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2925",
  "source" : 48,
  "target" : 59,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2927",
  "source" : 48,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2930",
  "source" : 48,
  "target" : 64,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2934",
  "source" : 48,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2936",
  "source" : 48,
  "target" : 70,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2939",
  "source" : 48,
  "target" : 73,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2941",
  "source" : 48,
  "target" : 75,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2943",
  "source" : 48,
  "target" : 77,
  "overlap" : 4
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2945",
  "source" : 48,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2947",
  "source" : 48,
  "target" : 81,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2948",
  "source" : 48,
  "target" : 82,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2952",
  "source" : 48,
  "target" : 86,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2953",
  "source" : 49,
  "target" : 50,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2962",
  "source" : 49,
  "target" : 59,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2964",
  "source" : 49,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2971",
  "source" : 49,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2973",
  "source" : 49,
  "target" : 70,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2978",
  "source" : 49,
  "target" : 75,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2980",
  "source" : 49,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2984",
  "source" : 49,
  "target" : 81,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2985",
  "source" : 49,
  "target" : 82,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "2998",
  "source" : 50,
  "target" : 59,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3000",
  "source" : 50,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3007",
  "source" : 50,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3009",
  "source" : 50,
  "target" : 70,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3014",
  "source" : 50,
  "target" : 75,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3016",
  "source" : 50,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3020",
  "source" : 50,
  "target" : 81,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3021",
  "source" : 50,
  "target" : 82,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3028",
  "source" : 51,
  "target" : 54,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3031",
  "source" : 51,
  "target" : 57,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3035",
  "source" : 51,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3039",
  "source" : 51,
  "target" : 65,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3041",
  "source" : 51,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3042",
  "source" : 51,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3051",
  "source" : 51,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3053",
  "source" : 51,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3054",
  "source" : 51,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3057",
  "source" : 51,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3058",
  "source" : 51,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3066",
  "source" : 52,
  "target" : 58,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3072",
  "source" : 52,
  "target" : 64,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3080",
  "source" : 52,
  "target" : 72,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3112",
  "source" : 53,
  "target" : 71,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3118",
  "source" : 53,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3130",
  "source" : 54,
  "target" : 57,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3134",
  "source" : 54,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3138",
  "source" : 54,
  "target" : 65,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3140",
  "source" : 54,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3141",
  "source" : 54,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3150",
  "source" : 54,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3152",
  "source" : 54,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3153",
  "source" : 54,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3156",
  "source" : 54,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3157",
  "source" : 54,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3194",
  "source" : 56,
  "target" : 60,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3211",
  "source" : 56,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3212",
  "source" : 56,
  "target" : 78,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3219",
  "source" : 56,
  "target" : 85,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3224",
  "source" : 57,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3228",
  "source" : 57,
  "target" : 65,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3230",
  "source" : 57,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3231",
  "source" : 57,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3240",
  "source" : 57,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3242",
  "source" : 57,
  "target" : 79,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3243",
  "source" : 57,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3246",
  "source" : 57,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3247",
  "source" : 57,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3255",
  "source" : 58,
  "target" : 64,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3263",
  "source" : 58,
  "target" : 72,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3279",
  "source" : 59,
  "target" : 61,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3282",
  "source" : 59,
  "target" : 64,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3286",
  "source" : 59,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3288",
  "source" : 59,
  "target" : 70,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3291",
  "source" : 59,
  "target" : 73,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3293",
  "source" : 59,
  "target" : 75,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3295",
  "source" : 59,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3299",
  "source" : 59,
  "target" : 81,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3300",
  "source" : 59,
  "target" : 82,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3304",
  "source" : 59,
  "target" : 86,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3321",
  "source" : 60,
  "target" : 77,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3322",
  "source" : 60,
  "target" : 78,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3329",
  "source" : 60,
  "target" : 85,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3334",
  "source" : 61,
  "target" : 65,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3336",
  "source" : 61,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3337",
  "source" : 61,
  "target" : 68,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3339",
  "source" : 61,
  "target" : 70,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3344",
  "source" : 61,
  "target" : 75,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3346",
  "source" : 61,
  "target" : 77,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3348",
  "source" : 61,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3349",
  "source" : 61,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3350",
  "source" : 61,
  "target" : 81,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3351",
  "source" : 61,
  "target" : 82,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3352",
  "source" : 61,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3353",
  "source" : 61,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3393",
  "source" : 63,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3404",
  "source" : 64,
  "target" : 66,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3410",
  "source" : 64,
  "target" : 72,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3411",
  "source" : 64,
  "target" : 73,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3412",
  "source" : 64,
  "target" : 74,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3416",
  "source" : 64,
  "target" : 78,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3424",
  "source" : 64,
  "target" : 86,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3426",
  "source" : 65,
  "target" : 67,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3427",
  "source" : 65,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3430",
  "source" : 65,
  "target" : 71,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3434",
  "source" : 65,
  "target" : 75,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3436",
  "source" : 65,
  "target" : 77,
  "overlap" : 8
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3438",
  "source" : 65,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3439",
  "source" : 65,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3442",
  "source" : 65,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3443",
  "source" : 65,
  "target" : 84,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3466",
  "source" : 67,
  "target" : 68,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3475",
  "source" : 67,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3477",
  "source" : 67,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3478",
  "source" : 67,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3481",
  "source" : 67,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3482",
  "source" : 67,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3486",
  "source" : 68,
  "target" : 70,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3491",
  "source" : 68,
  "target" : 75,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3493",
  "source" : 68,
  "target" : 77,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3495",
  "source" : 68,
  "target" : 79,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3496",
  "source" : 68,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3497",
  "source" : 68,
  "target" : 81,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3498",
  "source" : 68,
  "target" : 82,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3499",
  "source" : 68,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3500",
  "source" : 68,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3524",
  "source" : 70,
  "target" : 75,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3526",
  "source" : 70,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3530",
  "source" : 70,
  "target" : 81,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3531",
  "source" : 70,
  "target" : 82,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3539",
  "source" : 71,
  "target" : 75,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3541",
  "source" : 71,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3565",
  "source" : 73,
  "target" : 74,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3569",
  "source" : 73,
  "target" : 78,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3577",
  "source" : 73,
  "target" : 86,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3581",
  "source" : 74,
  "target" : 78,
  "overlap" : 4
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3591",
  "source" : 75,
  "target" : 77,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3595",
  "source" : 75,
  "target" : 81,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3596",
  "source" : 75,
  "target" : 82,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3601",
  "source" : 76,
  "target" : 77,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3611",
  "source" : 77,
  "target" : 78,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3612",
  "source" : 77,
  "target" : 79,
  "overlap" : 2
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3613",
  "source" : 77,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3614",
  "source" : 77,
  "target" : 81,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3615",
  "source" : 77,
  "target" : 82,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3616",
  "source" : 77,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3617",
  "source" : 77,
  "target" : 84,
  "overlap" : 3
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3618",
  "source" : 77,
  "target" : 85,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3626",
  "source" : 78,
  "target" : 85,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3628",
  "source" : 79,
  "target" : 80,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3631",
  "source" : 79,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3632",
  "source" : 79,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3637",
  "source" : 80,
  "target" : 83,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3638",
  "source" : 80,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3641",
  "source" : 81,
  "target" : 82,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         }, {
  "id" : "3650",
  "source" : 83,
  "target" : 84,
  "overlap" : 1
,             "name": null,             "lineStyle": {                 "normal": {}             }         } ]
        
    }
    var categories = [];
    for (var i = 0; i < 2; i++) {
        categories[i] = {
            name: 'CandActCFTR' + i
        };
    }
    graph.nodes.forEach(function (node) {
        node.itemStyle = null;
        node.value = node.symbolSize;
        node.symbolSize /= 1.5;
        node.label = {
            normal: {
                show: node.symbolSize > 10
            }
        };
        node.category = node.attributes.modularity_class;
    });
    option = {
        title: {
            text: 'CandActCFTR',
            subtext: 'Circular layout',
            top: 'bottom',
            left: 'right'
        },
        tooltip: {},
        legend: [{
            // selectedMode: 'single',
            data: categories.map(function (a) {
                return a.name;
            })
        }],
        animationDurationUpdate: 1500,
        animationEasingUpdate: 'quinticInOut',
        series : [
            {
                name: 'CandActCFTR',
                type: 'graph',
                layout: 'circular',
                circular: {
                    rotateLabel: true
                },
                data: graph.nodes,
                links: graph.links,
                categories: categories,
                roam: true,
                label: {
                    normal: {
                        position: 'right',
                        formatter: '{b}'
                    }
                },
                lineStyle: {
                    normal: {
                        color: 'source',
                        curveness: 0.3
                    }
                }
            }
        ]
    };

    myChart.setOption(option);

    </script>




	</body>
</html>
