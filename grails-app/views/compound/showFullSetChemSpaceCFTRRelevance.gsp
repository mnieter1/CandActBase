<!DOCTYPE html>
<%! import grails.converters.JSON %>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>CandActCFTR Seed Content ChemSpace</title>
		<asset:javascript src="echarts.min.js"/>
	</head>
	<body>

		<H1>CandActCFTR Seed Compounds ChemSpace</H1>

<hr>


<!-- preparing a DOM with width and height for ECharts -->
    <div id="main" style="width:1200px; height:1200px;"></div>
    
      <p id="intersection">
      <br><br><br><br><br>
      </p>
      <hr>
    <div id="mainCircle" style="width:1000px; height:1000px;"></div>

    
    <script>
    //checking out the model to object conversion to load the data from the database
    var theJSONString = '${allCompounds as JSON}';
    var fixedQuotesInStrings = theJSONString.replace(new RegExp('&quot;', 'gi'), '"');
    var compoundList  = JSON.parse(fixedQuotesInStrings);

    //checking out the model to object conversion to load the data from the database
    var theJSONString = '${allBiologicalDescriptors as JSON}';
    var fixedQuotesInStrings = theJSONString.replace(new RegExp('&quot;', 'gi'), '"');
    var biologicalDescriptorsList  = JSON.parse(fixedQuotesInStrings);

    //checking out the model to object conversion to load the data from the database
    var theJSONString = '${allChemicalDescriptors as JSON}';
    var fixedQuotesInStrings = theJSONString.replace(new RegExp('&quot;', 'gi'), '"');
    var chemicalDescriptorsList  = JSON.parse(fixedQuotesInStrings);
   
    
    
 /*   
      str = JSON.stringify(compoundList[0], null, 4); // (Optional) beautiful indented output.
    console.log(str); // Logs output to dev tools console.
    alert(str); // Displays output using window.alert()
    
    
    str = JSON.stringify(biologicalDescriptorsList[0].cftrRelevance, null, 4); // (Optional) beautiful indented output.
    console.log(str); // Logs output to dev tools console.
    alert(str); // Displays output using window.alert()
    
    
    str = JSON.stringify(chemicalDescriptorsList[0].pca1, null, 4); // (Optional) beautiful indented output.
    console.log(str); // Logs output to dev tools console.
    alert(str); // Displays output using window.alert()
    

    document.getElementById("demoToReportJustAValue").innerHTML = "first JSON entry id is " + compoundList[0].id + " and smiles" + compoundList[0].smiles;
*/
    </script>

    <script type="text/javascript">
        // based on prepared DOM, initialize echarts instance


// prepare the sorting and merging of the lists into one data table joined by id
    // for each id
    var currentID = 0;
    var combinedCompoundChemAndBio = [];
    var indexlist = [];
    for (let x = 0; x < compoundList.length; x++) {
        currentID = compoundList[x].id;
        // find position of that chemical descriptor object
        indexlist.push(currentID);
    }

   
    var sortByThisIDOrder = indexlist;
    
    
    var sortedSeriesDefinitionsChem = [];
    for (let y = 0; y < chemicalDescriptorsList.length; y++) {
    
        var currentID = chemicalDescriptorsList[y].id;
        // check to find the according index of the name tag
        var foundAt = sortByThisIDOrder.findIndex(function(element) {
            return element == currentID;
        });
        console.log(foundAt);
        sortedSeriesDefinitionsChem[foundAt] = chemicalDescriptorsList[y];
    }

    var sortedSeriesDefinitionsBio = [];
    for (let y = 0; y < biologicalDescriptorsList.length; y++) {
    
        var currentID = biologicalDescriptorsList[y].id;
        // check to find the according index of the name tag
        var foundAt = sortByThisIDOrder.findIndex(function(element) {
            return element == currentID;
        });
        console.log(foundAt);
        sortedSeriesDefinitionsBio[foundAt] = biologicalDescriptorsList[y];
    }
   
   
   // now replace the previously unsorted with the newly sorted lists
   chemicalDescriptorsList = sortedSeriesDefinitionsChem;
   biologicalDescriptorsList = sortedSeriesDefinitionsBio;
   
   /*
   faulty removal or was it unsorted in the first place?
    // now make the required columns out of the provided json
   // first remove the entries with a missing entry in the coordinates
   var offsetcounter = -1;
   for (let x = 0; x < compoundList.length; x++) {  
    //strOut = JSON.stringify(compoundList[x], null, 4); // (Optional) beautiful indented output.
    //    console.log(strOut); // Logs output to dev tools console.
    if (chemicalDescriptorsList[x] == undefined){   
     //   str = JSON.stringify(compoundList[x], null, 4); // (Optional) beautiful indented output.
     //   console.log(str); // Logs output to dev tools console.
     //   alert(str); // Displays output using window.alert()
        
        chemicalDescriptorsList.splice((x-offsetcounter), 1);
        biologicalDescriptorsList.splice((x-offsetcounter), 1);
        compoundList.splice((x-offsetcounter), 1);
        offsetcounter = offsetcounter+1;
    }
   }
   */
    
    //find all the different versions of activity classes
    // "orderOfInteraction": "unknown",
    // "cftrRelevance": "CFTR corrector",
    // "influenceOnCftrFunction": "enhances CFTR function",
    // "subcellularCompartment": "several"
    
    
    var uniqueActivityClassesC = [];
    var allActivityClassesC = [];
    for (let x = 0; x < compoundList.length; x++) {
        
        // define unique set
        if(uniqueActivityClassesC.indexOf(biologicalDescriptorsList[x].cftrRelevance.toString()) == -1){
        
            uniqueActivityClassesC.push(biologicalDescriptorsList[x].cftrRelevance.toString());
        }
        // collect them all
        allActivityClassesC.push(biologicalDescriptorsList[x].cftrRelevance.toString());
    }
    
    
    
    //first specify where the entries are for each class creating index arrays
    var indexContainerForSeriesC = [];
    var aSingleIndexSeriesC = [];
    
    for (let y = 0; y < uniqueActivityClassesC.length; y++) {
        aSingleIndexSeriesC = [];
        for (let x = 0; x < allActivityClassesC.length; x++) {
            // check if current class is a match
            if(allActivityClassesC[x] == uniqueActivityClassesC[y]){
                // seems to match so remember the position
                aSingleIndexSeriesC.push(x);
            }
        
        }
        indexContainerForSeriesC.push(aSingleIndexSeriesC);
    }
    
    // now that we got the indices of our series sets we can extract the coodinates and feed them into the echarts required object
    var currentChemSpaceCoordsC = [];
    var onePositionC = [];
    var seriesDefinitionsC = [];
    var singleSeriesC = {};
    
     for (let y = 0; y < uniqueActivityClassesC.length; y++) {
        // based on the class definition we collect the required data and stuff it into the container to hold a single series, afterwards we push this into the collector for all series
        currentChemSpaceCoordsC = [];
        allIDsForASeries = [];
        
        for (let x = 0; x < indexContainerForSeriesC[y].length; x++) {
            
            // make 2D array
            onePositionC = [];

            // works but not the one below with indices probably because of missing coordinates?!?
            //onePositionC.push(indexContainerForSeriesC[y][x]);
            //onePositionC.push(indexContainerForSeriesC[y][x]);
            
            onePositionC.push(chemicalDescriptorsList[indexContainerForSeriesC[y][x]].pca1);
            onePositionC.push(chemicalDescriptorsList[indexContainerForSeriesC[y][x]].pca2);
            onePositionC.push(chemicalDescriptorsList[indexContainerForSeriesC[y][x]].id);
            currentChemSpaceCoordsC.push(onePositionC);
            allIDsForASeries.push(chemicalDescriptorsList[indexContainerForSeriesC[y][x]].id);
            console.log(chemicalDescriptorsList[indexContainerForSeriesC[y][x]].id);
        }   
        // define the container with name and coordinates
        singleSeriesC =
        {
            //
            itemids: allIDsForASeries,
            name:uniqueActivityClassesC[y],
            type:'scatter',
            data: currentChemSpaceCoordsC
            
        }
        seriesDefinitionsC.push(singleSeriesC);
    }
    
    
    
    
        var myChart = echarts.init(document.getElementById('main'));
        




var option = {
    title : {
        text: 'CandActCFTR ChemSpace',
        subtext: 'CFTR relevance'
    },
    grid: {
        left: '3%',
        right: '7%',
        bottom: '3%',
        containLabel: true
    },
    tooltip : {
        // trigger: 'axis',
        showDelay : 0,
        enterable: true,
        formatter : function (params) {
            if (params.value.length > 1) {
                return params.seriesName + ' :<br/>'
                + 'x= ' + params.value[0] + ' <br/>'
                + 'y= ' + params.value[1] + ' <br/>'
                + '<a href="/Compound/cycleCompoundsKekuleStyle/' + params.value[2] + '" class="btn btn-default">compoundID = ' + params.value[2] + '</a>';
            }
            else {
                return params.seriesName + ' :<br/>'
                + params.name + ' : '
                + params.value + ' ';
            }
        },
        axisPointer:{
            show: true,
            type : 'cross',
            lineStyle: {
                type : 'dashed',
                width : 1
            }
        }
    },
    toolbox: {
        feature: {
            dataZoom: {},
            brush: {
                type: ['rect', 'polygon', 'clear']
            }
        }
    },
    brush: {
    },
    legend: {
//         data: ['some','others', 'chemSpace'],
        type:'scroll',
        right: 10,
        top: 20,
        bottom:20,
        orient:'vertical',
        data: uniqueActivityClassesC
    },
    xAxis : [
        {
            type : 'value',
            scale:true,
            axisLabel : {
                formatter: '{value}'
            },
            splitLine: {
                show: false
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            scale:true,
            axisLabel : {
                formatter: '{value}'
            },
            splitLine: {
                show: false
            }
        }
    ],
    series : seriesDefinitionsC
       
};

        
        
        
        // use configuration item and data specified to show chart
        myChart.setOption(option);
    
    
    
    
    </script>




	</body>
</html>
