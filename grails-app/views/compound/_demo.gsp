<script>
	var htmlcode = "";
		htmlcode += "<hr><br>";
        //	htmlcode += "<strong>SMILES</strong> " + "<%=currentCompound.smiles%>" + "<br>";

	

		htmlcode +='<br><strong>PubChem automatically assigned</strong><br>';
       		// try to find entry in PubChem
        	var CIDS = myJSONQuest_Mod_CIDs_lookup("<%=currentCompound.smiles%>");
        	var synonyms = myJSONQuest_Mod_synonyms_lookup("<%=currentCompound.smiles%>");
		// adding picture
		//htmlcode += '<img src="https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/smiles/' + '<%=currentCompound.smiles%>' + '/PNG" alt="chemical compound" style="width:304px;height:228px;">';
		
		//htmlcode += CIDS;
		//htmlcode += synonyms;
		//htmlcode += "<br>";
		
		// add PubChem info where it can be found
//	       	if(JSON.parse(CIDS)["IdentifierList"] == undefined){
//	        	htmlcode += '<br><strong>CID</strong> is ' + "was not found due to error in SMILES format";        				
//		}else 
		if(JSON.parse(CIDS).IdentifierList.CID[0] == 0){
	        	htmlcode += '<br><strong>CID</strong> is ' + "unknown";        				
	       	} else {
			htmlcode += '<br><strong>CID</strong> is ' + JSON.parse(CIDS).IdentifierList.CID[0];
			htmlcode += ' the link to PubChem record is <a href="https://pubchem.ncbi.nlm.nih.gov/compound/'+JSON.parse(CIDS).IdentifierList.CID[0]+'" target="_blank">' + 'https://pubchem.ncbi.nlm.nih.gov/compound/' +JSON.parse(CIDS).IdentifierList.CID[0]+ "</a><br>";
//	        	//synonyms
			if(synonyms.search("Fault") == -1){	        	
				htmlcode = htmlcode + '<br><strong>synonyms are</strong><br> ' + JSON.parse(synonyms).InformationList.Information[0].Synonym.join(", ");
        		}		
        	}
		htmlcode += "<br>";


// 
        //document.getElementById("demo").innerHTML = htmlcode;
	document.write(htmlcode);

</script>
