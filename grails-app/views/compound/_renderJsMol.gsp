<p id="jmolscript">

    <script type="text/javascript" src="https://chemapps.stolaf.edu/jmol/jmol.php?model=${currentCompound.smiles}&inline"></script>
    
</p>
