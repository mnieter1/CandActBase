<!DOCTYPE html>
<html>
<head>

<meta name="layout" content="mainManuMod"/>
    <title>cycle compounds view with Kekule viewer</title>


<asset:javascript src="jquery-3.3.1.js"/>
<asset:stylesheet src="kekule/kekule.css"/>

<asset:javascript src="kekule/kekule.min.js"/>

		
<script>
Kekule.Indigo.enable();
function load_kekule() {
  chemViewer = new Kekule.ChemWidget.Viewer(document.getElementById('kekule'));
}
function display_kekule(smi) {
  var mol = Kekule.IO.loadFormatData(smi, "smi");  
  chemViewer.setChemObj(mol);
}
function display() {
  var smi = document.getElementById("smiles").value;
  console.log(smi);
  display_kekule(smi);
}
$(window).on('load', function(){
      console.log("OK");
      load_kekule();
      display();
      $('textarea').on('change', display);
});
</script>
</head>
<body>

<h2> SMILES </h2>
<textarea rows="10" cols="50" id="smiles"  style="font-size: 12pt">
C1=CC(=CC=C1C2=COC3=CC(=CC(=C3C2=O)O)O)O
</textarea>

<div id="div2"> 
<h2> Kekule </h2>
<div id="kekule" style="width:100%;height:650px"
		 data-widget="Kekule.ChemWidget.Viewer2D" data-enable-toolbar="true" data-auto-size="true" data-padding="20"
		 data-toolbar-evoke-modes="[0]" >
</div>
</div>




</body>
</html>
