<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>CandActCFTR references</title>
	</head>
	<body>
		<p><H3>CandActCFTR used public reference list</H3></p>

        
        <g:each var="oneReference" in="${citationReferences}">
            <div id="pending_list" class="onTop">
                            <g:render template="ReferenceRendering" model="['citationReference':oneReference]" />
            </div>
            <g:each var="oneRIFReference" in="${oneReference.rifs}">
                <div id="pending_list_RIFs" class="onTop">
                    <g:render template="/libraryReferences/RIFReferenceRendering" model="['referenceIntoFunction':oneRIFReference]" />
            </g:each>
            <div id="div2" style="width:800px;height:8px;background-color:green;"></div><br>
        </g:each>
        
          <p><br></p>


	</body>
</html>
