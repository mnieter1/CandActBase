<script>
	var htmlcode = "";
		htmlcode += "<hr><br>";

        htmlcode += "<strong><%=citationReference.originalAuthor%> et. al. <%=citationReference.yearSuffix%></strong> (<%=citationReference.refType%>)";
        htmlcode += "<br>";
        htmlcode += "<strong><%=citationReference.refTitle%></strong></br>";
        htmlcode += "<i><%=citationReference.containerTitle%></i>, ";
        htmlcode += "page <%=citationReference.pageString%>, ";
        htmlcode += "volume <%=citationReference.volume%>, ";
        htmlcode += "issue <%=citationReference.issue%>, ";
        htmlcode += "year <%=citationReference.yearSuffix%> ";
        

        htmlcode += "<br>";
        

	document.write(htmlcode);

</script>

        <g:each var="author" in="${citationReference.authors}">
            
            "${author.given} ${author.family}"
                            
            
        </g:each>
            </br>

<script>
	var htmlcode = "";
        htmlcode += "DOI: <a href=\"http://dx.doi.org/<%=citationReference.doi%>\"> <%=citationReference.doi%> </a>";
        htmlcode += "<br>";
        htmlcode += "PMID: <a href=\"https://www.ncbi.nlm.nih.gov/pubmed/?term=<%=citationReference.pmid%>\"> <%=citationReference.pmid%> </a>";
        htmlcode += "<br>";
// 		htmlcode += "<br>";

	document.write(htmlcode);

</script>
