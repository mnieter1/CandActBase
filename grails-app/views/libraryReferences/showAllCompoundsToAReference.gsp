<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>CandActCFTR references</title>
	</head>
	<body>
	<a href="/libraryReferences/showAllCompoundsToAReference/${Integer.valueOf(currentCitationReferenceID)-1}" class="btn btn-default">previous reference</a>
    <b>ID:</b> ${currentCitationReferenceID}
    <a href="/libraryReferences/showAllCompoundsToAReference/${Integer.valueOf(currentCitationReferenceID)+1}" class="btn btn-default">next reference</a>

		<p><H3>CandActCFTR used public reference</H3></p>
        The following publication reference has been reported to contain CFTR related structural information.</br>
        
        
        <g:each var="oneReference" in="${citationReferences}">
            <div id="pending_list" class="onTop">
                            <g:render template="ReferenceRendering" model="['citationReference':oneReference]" />
            </div>
            <g:each var="oneRIFReference" in="${oneReference.rifs}">
                <div id="pending_list_RIFs" class="onTop">
                    <g:render template="/libraryReferences/RIFReferenceRendering" model="['referenceIntoFunction':oneRIFReference]" />
            </g:each>
            <div id="div2" style="width:800px;height:8px;background-color:green;"></div><br>
        </g:each>
        
        <p><br></p>
        Below you will find all the structures identified in the publication. Be warned that this does not imply any activity. <strong>n=${compoundsReferenced.count()}</strong>
        
        <g:render template="/compound/showCompoundHitListPlain" model="['currentCompounds':compoundsReferenced]" />
        
          <p><br></p>


	</body>
</html>
