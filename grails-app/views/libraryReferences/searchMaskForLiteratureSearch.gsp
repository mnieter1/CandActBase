<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Search Reference</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    
    <script type="text/javascript">
		
		function searchWithTitle(){
				window.location = '/LibraryReferences/searchResultsByTitle?refTitle="' + $('#refTitle').val() +'"';
		}
		
		
		</script>



</head>
<body>
            <% def wasThereSomething = "${previouslyfoundEntryStatus}"%>
            
            <% if(wasThereSomething == "no match found"){%>
            <%= "<strong>no match found for <i>${previousSynonymQuery}</i><br> please try again</strong><br>" %>
            <%}%>
            <% if(wasThereSomething == "no structure matching SMILES found"){%>
            <%= "<strong>no match found for smiles <i>${previousSmilesQuery}</i><br> please try again</strong><br>" %>
            <%}%>
            <% if(wasThereSomething == "no structure matching InChIKey found"){%>
            <%= "<strong>no match found for InChIKey <i>${previousInChIKeyQuery}</i><br> please try again</strong><br>" %>
            <%}%>
            
            <h1><strong>search for literature reference by title</strong></h1>
		
            <label>Title: </label>
            <g:textField name="refTitle" value="Curcumin ain't a cucri"/>
            <input type="button" style="margin:10px" value="Search via Title" onclick="searchWithTitle()"></input><br/>
            <br/>
		<hr>
		
</body>
</html>
