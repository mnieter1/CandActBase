<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Search Reference</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    
    <script type="text/javascript">
		
		function searchWithTitle(){
				window.location = '/LibraryReferences/searchResultsByTitleForJoin?refTitle="' + $('#refTitle').val() +'"&compoundID=${params.compoundID}';
		}
		
		
		</script>



</head>
<body>
            <% if(citationReferences){ %>
            <%= "<H1>Multiple Matching Entries for <strong>${refTitleQuery}</strong> in CandActCFTR Seed Compounds: <strong>${refTitleQuery}</strong> found <strong>${citationReferences.size()}x</strong></H1>" %>
            <%= "<H1><strong>please refine you query to be more precise pointing to just one citation reference</strong></H1>" %>
            <%}%>
            <% if(wasThereSomething == "no structure matching SMILES found"){%>
            <%= "<strong>no match found for smiles <i>${previousSmilesQuery}</i><br> please try again</strong><br>" %>
            <%}%>
            <% if(wasThereSomething == "no structure matching InChIKey found"){%>
            <%= "<strong>no match found for InChIKey <i>${previousInChIKeyQuery}</i><br> please try again</strong><br>" %>
            <%}%>
            
            
            <g:each var="oneReference" in="${citationReferences}">
            <div id="pending_list" class="onTop">
                            <g:render template="ReferenceRendering" model="['citationReference':oneReference]" />
            </div>
            
            </g:each>
            
            
            <label>Title: </label>
            <g:textField name="refTitle" value="${refTitleQuery}"/>
            <input type="button" style="margin:10px" value="Search via Title" onclick="searchWithTitle()"></input><br/>
            <br/>
		<hr>
		
</body>
</html>
