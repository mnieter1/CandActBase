<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>CandActCFTR references</title>
	</head>
	<body>
		<p><H3>CandActCFTR used public reference list</H3></p>
        The following ${citationReferencesCounts} publications references have been reported to contain CFTR related structural information <br><br>
        
        <hr>
        use the seach page function of your browser to see the information highlighted, which is of special interest to you: e.g. authors, ...
        </br>
        generally it's activated with the shortcut "Ctrl+F", where you can start and type ahead and the search term gets highlighed within the page
        </br>
        
        <g:each var="oneReference" in="${citationReferences}">
            <a href="/libraryReferences/showAllCompoundsToAReference/${oneReference.id}" class="btn btn-default"><b>ReferenceID:</b> ${oneReference.id}</a>
            <div id="pending_list" class="onTop">
                            <g:render template="ReferenceRendering" model="['citationReference':oneReference]" />
            </div>
            <g:each var="oneRIFReference" in="${oneReference.rifs}">
                <div id="pending_list_RIFs" class="onTop">
                    <g:render template="/libraryReferences/RIFReferenceRendering" model="['referenceIntoFunction':oneRIFReference]" />
            </g:each>
            <div id="div2" style="width:800px;height:8px;background-color:green;"></div><br>
        </g:each>
        
          <p><br></p>


	</body>
</html>
