<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>CandActCFTR Reference List</title>
	</head>
	<body>

		<H1>CandActCFTR Initialize Reference List</H1>
<br>
<hr>
The following structures are a list compounds for which an activation of CFTR function or an improvement of CFTR processing have been <a href="../libraryReferences">published</a> in the past decade.
<br><br></p>

<hr>

<p>


<hr>

</br>

<p>
Also take a look at the <a href="https://pubchem.ncbi.nlm.nih.gov/target/gene/1080" target="_blank">PubChem CFTR Reference Page</a></p>




	</body>
</html>
