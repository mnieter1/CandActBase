<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>showing some interactive graphs for the data</title>

    <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
    <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>

</head>
<body>



<h1>testing vizualisation methods to be integrated into grails system</h1>

<div class="ct-chart1 ct-perfect-fourth"></div>
<div class="ct-chart2 ct-perfect-fourth"></div>

<script>

var data = {
  // A labels array that can contain any sort of values
  labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri'],
  // Our series array that contains series objects or in this case series data arrays
  series: [
    [5, 2, 4, 2, 0]
  ]
};

// Create a new line chart object where as first parameter we pass in a selector
// that is resolving to our chart container element. The Second parameter
// is the actual data object.
new Chartist.Line('.ct-chart1', data);





var times = function(n) {
  return Array.apply(null, new Array(n));
};

var data2 = times(52).map(Math.random).reduce(function(data2, rnd, index) {
  data.labels.push(index + 1);
  data.series.forEach(function(series) {
    series.push(Math.random() * 100)
  });

  return data;
}, {
  labels: [],
  series: times(4).map(function() { return new Array() })
});

var options = {
  showLine: false,
  axisX: {
    labelInterpolationFnc: function(value, index) {
      return index % 13 === 0 ? 'W' + value : null;
    }
  }
};

var responsiveOptions = [
  ['screen and (min-width: 640px)', {
    axisX: {
      labelInterpolationFnc: function(value, index) {
        return index % 4 === 0 ? 'W' + value : null;
      }
    }
  }]
];

new Chartist.Line('.ct-chart2', data2, options, responsiveOptions);





</script>




</body>
</html>
