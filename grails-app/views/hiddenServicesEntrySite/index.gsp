<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>entry for "hidden" services</title>


</head>
<body>



<h1>a page to collect and present links to "hidden" development services</h1>

<a href="appInfo" class="btn btn-default">appInfo page with links to all controllers</a><br>

<a href="/Compound/createCompoundEntry" class="btn btn-default">create Compound Entry</a><br>

<a href="/Compound/showCompoundAndAddReferenceLinks" class="btn btn-default">show compound and references and optionally add reference links</a><br>

<a href="/LibraryReferences/searchMaskForLiteratureSearch" class="btn btn-default">search Mask For Literature Search</a><br>

<a href="/testPubMedQuery/updatePMIDsBasedOnTitleQueryingPubMed" class="btn btn-default">update PMIDs for Literature Searching PubMed By Title</a><br>



</body>
</html>
