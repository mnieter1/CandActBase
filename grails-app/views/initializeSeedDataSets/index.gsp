<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Initialize the Database</title>

<script type="text/javascript" src="https://chemapps.stolaf.edu/jmol/jmol.php?model= &link= "></script>


<script>


	function myJSONQuest_Mod_synonyms_lookup (myQuerySmile) {
				var webQueryPath = 'https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/smiles/' + encodeURIComponent(myQuerySmile) + '/synonyms/JSON';
				
			var Httpreq = new XMLHttpRequest(); // a new request
			Httpreq.open("GET",webQueryPath,false);
			Httpreq.send(null);
			return Httpreq.responseText;
				
				}
				
	function myJSONQuest_Mod_CIDs_lookup (myQuerySmile) {
				var webQueryPath = 'https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/smiles/' + encodeURIComponent(myQuerySmile) + '/cids/JSON';
				
			var Httpreq = new XMLHttpRequest(); // a new request
			Httpreq.open("GET",webQueryPath,false);
			Httpreq.send(null);
			return Httpreq.responseText;
				
				}




</script>


</head>
<body>



<% def nth = 3 %>
<% def subjson2 = myJson.getAt(nth) %>


<b>smiles:</b> <%=subjson2.SMILES%>  </br>
<b>PubChemLink:</b><a href="<%=subjson2.PubChem%>"> <%=subjson2.PubChem%>  </a></br>
<b>referenc:</b> <%=subjson2.reference%>  </br>


<br/>


<div>                                                                                                       
	<img src="https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/smiles/${subjson2.SMILES}/PNG" alt="chemical compound" style="width:304px;height:228px;">
<br/>                                                                                       
                        </div>

<p></br></p>
<hr>

<H4>JSMol pane</H4>
left click hold and moving mouse will rotate the model
</br>
right click for context menue
<div>
<script type="text/javascript" src="https://chemapps.stolaf.edu/jmol/jmol.php?model=${subjson2.SMILES}&inline"></script>
<%--
--%>
</div>

<br/>


<script>
	var htmlcode = "";
		htmlcode += "<hr><br>";
        	htmlcode += "<strong>SMILES</strong> " + "<%=subjson2.SMILES%>" + "<br>";
		htmlcode += "<strong>reference source</strong> " + "<%=subjson2.reference%>" + "<br>";
		htmlcode +='<strong>PubChem manually assigned</strong> <a href="' + "<%=subjson2.PubChem%>" + '" target="_blank">' + "<%=subjson2.PubChem%>" + "</a>" + "<br>";
	

		htmlcode +='<br><strong>PubChem automatically assigned</strong><br>';
       		// try to find entry in PubChem
        	var CIDS = myJSONQuest_Mod_CIDs_lookup("<%=subjson2.SMILES%>");
        	var synonyms = myJSONQuest_Mod_synonyms_lookup("<%=subjson2.SMILES%>");
		// adding picture
		htmlcode += '<img src="https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/smiles/' + '<%=subjson2.SMILES%>' + '/PNG" alt="chemical compound" style="width:304px;height:228px;">';
		
		//htmlcode += CIDS;
		//htmlcode += synonyms;
		htmlcode += "<br>";
		
		// add PubChem info where it can be found
//	       	if(JSON.parse(CIDS)["IdentifierList"] == undefined){
//	        	htmlcode += '<br><strong>CID</strong> is ' + "was not found due to error in SMILES format";        				
//		}else 
		if(JSON.parse(CIDS).IdentifierList.CID[0] == 0){
	        	htmlcode += '<br><strong>CID</strong> is ' + "unknown";        				
	       	} else {
			htmlcode += '<br><strong>CID</strong> is ' + JSON.parse(CIDS).IdentifierList.CID[0];
			htmlcode += ' the link to PubChem record is <a href="https://pubchem.ncbi.nlm.nih.gov/compound/'+JSON.parse(CIDS).IdentifierList.CID[0]+'" target="_blank">' + 'https://pubchem.ncbi.nlm.nih.gov/compound/' +JSON.parse(CIDS).IdentifierList.CID[0]+ "</a><br>";
//	        	//synonyms
			if(synonyms.search("Fault") == -1){	        	
				htmlcode = htmlcode + '<br><strong>synonyms are</strong><br> ' + JSON.parse(synonyms).InformationList.Information[0].Synonym.join(", ");
        		}		
        	}
		htmlcode += "<br>";


// 
        //document.getElementById("demo").innerHTML = htmlcode;
	document.write(htmlcode);

</script>


</body>
</html>
