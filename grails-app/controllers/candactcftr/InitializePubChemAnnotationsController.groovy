 
package candactcftr

import grails.converters.*


class InitializePubChemAnnotationsController {

    def index() { 
    
    def synonyms = []
    String InChIKey = "Some InChIKey"
    
    PubChemQueryService pubChemQueryService = new PubChemQueryService()


   def cid = -1
        

    
    
    
    def lookUpCID
    
    
    def cids = []

    // get all of the compound list
    def allCompounds = Compound.getAll()
    
    // for each entry get the InChIKey and retrieve CID and if CID was found list of synonymes
    allCompounds.each(){oneCompound ->
        
        synonyms = null
        // check if there can be a CID found using the InChIKey
        cid = pubChemQueryService.lookUpCID(oneCompound.inChIKey) 
    
        if(cid){
        // get the CID if it exist we can save this information and look for synonyms
            println "the CID @ PubChem is " + cid
            PubChemRecord currentPubChemRecord = new PubChemRecord('cid':cid)
            // getting the synonyms
            def pubChemSynonymsJSON = pubChemQueryService.lookUpSynonyms(oneCompound.inChIKey)
                
        //     println pubChemSynonymsJSON.InformationList.Information.Synonym[0][0]
            
            if(pubChemSynonymsJSON){
                synonyms = pubChemSynonymsJSON.InformationList.Information.Synonym[0]
                synonyms.each {
                    println "a.k.a ${it}"
                    currentPubChemRecord.addToSynonyms('synonym':"${it}")
                }
            
            }
            def myID = currentPubChemRecord.save(flush:true)    
            println "saved PubChem Entry as " + myID
        
            def attachThisPubChemInfo = PubChemRecord.findByCid(cid)
            println "this is the CID recovered " + attachThisPubChemInfo.cid + " @position " + attachThisPubChemInfo.id
            // save the info to the pubchem record attaching it to the
            oneCompound.setPubChem(attachThisPubChemInfo)
            oneCompound.save(flush:true)
                
                
            }else{
                println "no entry found"
            }
        
    }
    

	
	[cids:cids]
    
 
    }
}
