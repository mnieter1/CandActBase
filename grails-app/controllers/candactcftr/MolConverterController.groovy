package candactcftr

class MolConverterController {

    def index() { }
    
	def smilesToInChI() {

        def maxCompoundID = Compound.count()
        String feedback = "" + "currently there are ${maxCompoundID} entries in the database and we will look for compound with this smiles: ${params.smiles}"
        println feedback
        
        params.id = (params.id == null)?1:params.id
        // if it is to big set it back to first entry
        if(Integer.valueOf(params.id) > maxCompoundID | Integer.valueOf(params.id) <= 0){
            params.id = 1
        }
        
        def currentCompoundID = params.id
        
//         println Compound.list()
    
        String smilesQuery = "${params.smiles.drop(1)}"
        smilesQuery = smilesQuery.take(smilesQuery.length() - 1)
        println smilesQuery
        
    
        //def currentCompound = Compound.findBySmiles("${smilesQuery}")

        
        
    // call openbabel to translate the smiles to an InChI key
    // first create the in file with the smiles
    def tempIn = File.createTempFile("tempIn",".tmp")
    tempIn.write("${smilesQuery}")
//     println tempIn.absolutePath
    // create a blank one for the InChI key and SMILES
    def tempOut = File.createTempFile('tempOut', '.txt') 
    tempOut.write('moloko')  
//     println tempOut.absolutePath    
    // call openbabel
//     println "babel -ismi ${tempIn.absolutePath} -oinchikey ${tempOut.absolutePath}"
    def command = "babel -ismi ${tempIn.absolutePath} -oinchikey ${tempOut.absolutePath}"
    println command.execute().text
        
        def InChIKey = tempOut.text
        
        println InChIKey
        
        def currentCompound = new Compound('smiles': smilesQuery, 'inChIKey': InChIKey)
        
        if(currentCompound != null){
            println currentCompound.smiles
            println params
        
            render(view: "smilesToInChI", model: [maxCompoundID:maxCompoundID, currentCompound:currentCompound])
        }else{
            render "no match found"
            render(view: "smilesToInChI")
        }
        
        
	
	}
	
	def PubChemSynonymsLookUpByInChI () {
        String feedback = "" + "we will look for the synonymys of the compound with this InChIKey: ${params.InChIKey}"
        println feedback
        
        def synonyms = []
        PubChemQueryService pubChemQueryService = new PubChemQueryService()

        String InChIKey = "${params.InChIKey.drop(1)}"
        InChIKey = InChIKey.take(InChIKey.length() - 1)
        
        
        def cid = pubChemQueryService.lookUpCID(InChIKey as String)
        if(cid){
            // get the CID if it exist we can save this information and look for synonyms
            println "the CID @ PubChem is " + cid
            // getting the synonyms
            def pubChemSynonymsJSON = pubChemQueryService.lookUpSynonyms(InChIKey as String)
        
//             println pubChemSynonymsJSON.InformationList.Information.Synonym[0][0]
                
            pubChemSynonymsJSON.InformationList.Information.Synonym[0].each {
                println "a.k.a ${it}"
            }
        
        
        
        }else{
            println "no entry found"
        }
        
    
        
        
    
        def currentCompound = Compound.findByInChIKey("${InChIKey}")
//         
        if(currentCompound != null){
//             println currentCompound.smiles
//             println currentCompound.inChIKey
//             println params
        render found entries
//             render(view: "cycleCompounds", model: [maxCompoundID:maxCompoundID, currentCompound:currentCompound])
        }else{
            render "no match found"
//             render(view: "searchCompounds")
            
        }
	}

}
