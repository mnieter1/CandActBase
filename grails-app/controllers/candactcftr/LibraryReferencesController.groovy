package candactcftr

class LibraryReferencesController {

    def index() {
    
    // get all the registered references from the database
        def citationReferences = CitationReference.getAll()
        def citationReferencesCounts = CitationReference.count()
       // render citationReferences.size()
        [citationReferences:citationReferences, citationReferencesCounts:citationReferencesCounts]
    }
    
    
    def literatureListShowingRIFs(){
    // get all the registered references from the database
        def citationReferences = CitationReference.getAll()
        
       // render citationReferences.size()
        [citationReferences:citationReferences]
    }
    
    
    def searchResultsByTitle(){
        // test example
//         params.refTitle = "Cystic Fibrosis Respiratory Epithelial Cell Chronically Treated"
//         params.refTitle = "cystic"
        def refTitleQuery = params.refTitle.drop(1)
        refTitleQuery = refTitleQuery.take(refTitleQuery.length() - 1)
        
        ReferenceLookUpService referenceLookUpService = new ReferenceLookUpService()
        
        def citationReferences = referenceLookUpService.lookUpByTitle(refTitleQuery)
        
       // render citationReferences.size()
        render(view: "index", model:[citationReferences:citationReferences])
        
        
    }
    def searchReferenceForJoinWithCompound(){
        
        def compoundIDQuery = params.compoundID
        def newQuery = params.newQuery
        if(newQuery){
        println "was it a new query call " + newQuery
        }
        if(newQuery.equals("true") == true){
            session.compoundID = compoundIDQuery
            render(view: "searchReferenceForJoinWithCompound", model: [compoundIDQuery:compoundIDQuery])
        }else{
            def previousHitsWhere = session.citationReferences
            def refTitleQuery = session.refTitleQuery
            def searchedBefore = true
           render(view: 'searchReferenceForJoinWithCompound' , model: [compoundIDQuery:compoundIDQuery, citationReferences:previousHitsWhere, refTitleQuery:refTitleQuery, searchedBefore:searchedBefore])

        }
        
       
    }
    
    def saveApprovedReferenceLinktoCompound(){
        def compoundID = session.compoundID
        Compound currentCompound = Compound.get(compoundID)
        
        def thisCitation = session.citationReferences.take(1)
        currentCompound.addToCitationReferences(thisCitation)
        
        currentCompound.save(flush:true)
        
        chain(controller: "Compound", action:"showCompoundAndAddReferenceLinks", model: [currentCompound : currentCompound], params: [id:compoundID])
        
        
    }
    
    def searchResultsByTitleForJoin(){
        def refTitleQuery = params.refTitle.drop(1)
        refTitleQuery = refTitleQuery.take(refTitleQuery.length() - 1)
        
        def compoundIDQuery = session.compoundID
        session.compoundID = compoundIDQuery
        
        ReferenceLookUpService referenceLookUpService = new ReferenceLookUpService()
        
        def citationReferences = referenceLookUpService.lookUpByTitle(refTitleQuery)
        
        
        def howManyHits = citationReferences.size()
        // see how many hits there are if just one ask if correct and add it if so to the compound
        if(howManyHits == 1){
            // call approve page
            session.citationReferences = citationReferences
            session.refTitleQuery = refTitleQuery
            Compound currentCompound = Compound.get(Integer.valueOf(compoundIDQuery)) 
            
            render(view: '/compound/approveReferenceToStructureJoin', model: [citationReferences:citationReferences, compoundIDQuery:compoundIDQuery, currentCompound:currentCompound])
            
        }else if(howManyHits > 1){
            // use the session memory to pass the variables
            session.citationReferences = citationReferences
            session.refTitleQuery = refTitleQuery
        
            // if more than one reference was found ask to nail it down to one and redirect to the search form
            chain(controller: 'LibraryReferences', action: 'searchReferenceForJoinWithCompound' , model: [compoundIDQuery:compoundIDQuery])
        }else{
            // use the session memory to pass the variables
            session.citationReferences = citationReferences
            session.refTitleQuery = refTitleQuery
            // if more than one reference was found ask to nail it down to one and redirect to the search form
            chain(controller: 'LibraryReferences', action: 'searchReferenceForJoinWithCompound' , model: [compoundIDQuery:compoundIDQuery])
        }
        
        
        
        
    
    }
    
    def addReferenceToCompoundByTitle(){
    
        // take the currentCompoundInformation and add the specified library reference
        
        // prepare parameters //
        String compoundIDQuery = "${params.compoundID.drop(1)}"
        compoundIDQuery = compoundIDQuery.take(compoundIDQuery.length() - 1)
        
        
        String publicationIDQuery = "${params.publicationID.drop(1)}"
        publicationIDQuery = publicationIDQuery.take(publicationIDQuery.length() - 1)
        
        // get the compound entry to modify
        def currentCompound = Compound.get(Integer.valueOf(compoundIDQuery))
        // get the citation entry to add
        def thisCitation = CitationReference.get(Integer.valueOf(publicationIDQuery))

        // join the records
        currentCompound.addToCitationReferences(thisCitation)
        // save the joined connection
        currentCompound.save(flush:true)
    
        
    render(view:"/Compound/showCompoundAndAddReferenceLinks/${compoundIDQuery}", model: [currentCompound : currentCompound])
        
    
    
        
    }
    
    def searchMaskForLiteratureSearch(){
        // this is the controller sending the user to a search mask for searching in the reference library
        // as a start looking for the title like with the synonym search will be implemented
        // 
        
    }
    
    def defineByReferenceTitleSearch(){
    
    }
    
    
    def showAllCompoundsToAReference(){
        def maxCitationReferenceID = CitationReference.count()
        String feedback = "" + "currently there are ${maxCitationReferenceID} entries in the database"
        println feedback
        
        params.id = (params.id == null)?1:params.id
        // if it is to big set it back to first entry
        if(Integer.valueOf(params.id) > maxCitationReferenceID | Integer.valueOf(params.id) <= 0){
            params.id = 1
        }
        
        def currentCitationReferenceID = params.id
    
    
    // get all the registered references from the database
        def citationReferences = CitationReference.get(currentCitationReferenceID)
        println "" + citationReferences
        println "" + citationReferences.id
        println "" + citationReferences.refTitle
        LocateAllCompoundsLinkedToCitationReferenceService locateAllCompoundsLinkedToCitationReferenceService = new LocateAllCompoundsLinkedToCitationReferenceService()
        
        def compoundsReferenced = locateAllCompoundsLinkedToCitationReferenceService.locateAllCompoundsLinkedToCitationReference(citationReferences.id)
        println "nr of identified compounds " + compoundsReferenced.count()
       // render citationReferences.size()
        render(view: "showAllCompoundsToAReference", model:[citationReferences:citationReferences, compoundsReferenced:compoundsReferenced, currentCitationReferenceID:Integer.valueOf(currentCitationReferenceID)])
        
    }
    
    
}
