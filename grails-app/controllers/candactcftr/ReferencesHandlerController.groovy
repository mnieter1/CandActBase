package candactcftr

class ReferencesHandlerController {

    def index() { 
    // get all the registered references from the database
        def citationReferences = CitationReference.getAll()
        
       // render citationReferences.size()
        [citationReferences:citationReferences]
    }
}
