package candactcftr

class CompoundController {

	def index() {
        def maxCompoundID = Compound.count()
        
       // render maxCompoundID to show summary
        [maxCompoundID:maxCompoundID]
	}

	def _renderJsMol() {
	}

	
	def testshowCompoundAndAddReferenceLinks() {
		// to test just take the first compound in the DB later navigate to correct structure to start with
        def currentCompound = Compound.get(1)
        
        // for testing purposes add a reference to this compound
        def titleOfReferenceToAdd = "Curcumin Cross-links Cystic Fibrosis Transmembrane Conductance Regulator (CFTR) Polypeptides and Potentiates CFTR Channel Activity by Distinct Mechanisms"
        def thisCitation = CitationReference.findByRefTitle(titleOfReferenceToAdd)
        currentCompound.addToCitationReferences(thisCitation)
        
        titleOfReferenceToAdd = "A novel treatment of cystic fibrosis acting on-target: cysteamine plus epigallocatechin gallate for the autophagy-dependent rescue of class II-mutated CFTR"
        thisCitation = CitationReference.findByRefTitle(titleOfReferenceToAdd)
        currentCompound.addToCitationReferences(thisCitation)
        
        currentCompound.save(flush:true)
        
        
        [currentCompound : currentCompound]
			
	}
	
	
	def showCompoundAndAddReferenceLinks() {
	
        def maxCompoundID = Compound.count()
        String feedback = "" + "currently there are ${maxCompoundID} entries in the database"
        println feedback
        
        
        params.id = (params.id == null)?1:params.id
        // if it is to big set it back to first entry
        if(Integer.valueOf(params.id) > maxCompoundID | Integer.valueOf(params.id) <= 0){
            params.id = 1
        }
        
        def currentCompoundID = params.id
        
		// to test just take the first compound in the DB later navigate to correct structure to start with
        def currentCompound = Compound.get(Integer.valueOf(currentCompoundID))
        
        
        
        [currentCompound : currentCompound]
			
	}
	
	
	def searchCompounds() {
	}
	
	def searchCompoundsPrototypeView() {
	}
	
	def searchCompoundsBySynonym() {
        
        
        def maxCompoundID = Compound.count()
        String feedback = "" + "currently there are ${maxCompoundID} entries in the database and we will look for compound with this name: ${params.Synonym}"
        println feedback
        

        String synonymQuery = "${params.Synonym.drop(1)}"
        synonymQuery = synonymQuery.take(synonymQuery.length() - 1)
        String feedbackforQueryPart = "" + "checking to find this here" + "->${synonymQuery}<-"
        println feedbackforQueryPart
        
//     Compound['pubChem']

//         SELECT PUB_CHEM_RECORD.ID, PUB_CHEM_RECORD.CID, PUB_CHEM_RECORD_SYNONYMS.SYNONYMS_STRING
// FROM PUB_CHEM_RECORD 
// INNER JOIN PUB_CHEM_RECORD_SYNONYMS  ON PUB_CHEM_RECORD.ID =PUB_CHEM_RECORD_SYNONYMS.SYNONYMS WHERE PUB_CHEM_RECORD_SYNONYMS.SYNONYMS_STRING LIKE '%chlor%';

//         def c = PubChemRecord.createCriteria()
//         def results = c.list {
//             like("cid", 2943)
//             maxResults(10)
//             order("holderLastName", "desc")
//         }
//         println "what cid " + results.cid
//         println "what synonyms " + results.synonyms
        
        
        // define the search strategy
        // look into each PubMedRecord for the Synonyms
        // no need to go first through the compound
        // store hits of Synonym likely in return list
//         def compoundHits = PubChemRecord.findAll("from PubChemRecord b where :synonyms in elements(synonyms)", [synonyms: "${synonymQuery}"])
        
//         def compoundHits = PubChemSynonym.findBySynonymILike("%${synonymQuery}%")
//         def compoundHits = PubChemSynonym.findBySynonym("${synonymQuery}")

//         def compoundHits = PubChemSynonym.where {synonym =~ "%${synonymQuery}%"}
        def compoundHits = PubChemRecord.where {synonyms.synonym =~ "%${synonymQuery}%"}
        
        int pubChemCID = -1
        int counter = 0
        compoundHits.each(){pubChem ->
//             println "we found this compound " + pubChem.id + " with this string here " + pubChem.synonym
                println "we found this compound " + pubChem.id +  " with this cid here " + pubChem.cid
                counter += 1
               pubChemCID = pubChem.cid
            }
           
           // here we can check if there where multiple hits in the synonym list
//            println compoundHits.count()
//            println compoundHits.size()
        def currentCompound = null
           Compound queryCompound = null
           
        if(counter == 1){
            println "found a single hit, so take them to the result page"
            // find the compound entry which has this cid
            if(pubChemCID != -1){
                currentCompound = Compound.where {pubChem.cid == pubChemCID}
                currentCompound = currentCompound.take(1)
                println currentCompound.smiles.join(', ')
                println currentCompound.inChIKey.join(', ')
                
                String smiles = currentCompound.smiles.join(', ')
                String InChIKey = currentCompound.inChIKey.join(', ')
                
                queryCompound = Compound.findByInChIKey("${InChIKey}")
                if(queryCompound != null){
        //             println currentCompound.smiles
        //             println currentCompound.inChIKey
        //             println params
                
                    render(view: "cycleCompounds", model: [maxCompoundID:maxCompoundID, currentCompound:queryCompound])
                }
            }
        }else if(counter > 1){
        
            // multiple hits possible so show a list of hits
            println "will render full hit list with matching entries"
            // create a list to contain the IDs to look up and pass on
            def indexlist = []
            
            counter = 0
            compoundHits.each(){pubChem ->
//             println "we found this compound " + pubChem.id + " with this string here " + pubChem.synonym
                println "we found this compound " + pubChem.id +  " with this cid here " + pubChem.cid
               pubChemCID = pubChem.cid
               
               // now find the Compound.id
                currentCompound = Compound.where {pubChem.cid == pubChemCID}
                currentCompound = currentCompound.take(1)
                
               int currentCompoundID = Integer.valueOf(currentCompound.id.join(', '))
               indexlist[counter] = currentCompoundID
                counter += 1
               
            }
            
            def currentCompounds = Compound.getAll(indexlist)
            
            
            render(view: "showCompoundHitList", model: [maxCompoundID:maxCompoundID, currentCompounds:currentCompounds, queryString:synonymQuery, nrOfHits:counter])
        
        }else{
            def previouslyfoundEntryStatus = "no match found"
            render(view: "searchCompounds", model: [previouslyfoundEntryStatus:previouslyfoundEntryStatus, previousSynonymQuery:synonymQuery])
        }
    

        
	}
	
	def searchCompoundsBySMILES() {

        def maxCompoundID = Compound.count()
        String feedback = "" + "currently there are ${maxCompoundID} entries in the database and we will look for compound with this smiles: ${params.smiles}"
        println feedback
        

        String smilesQuery = "${params.smiles.drop(1)}"
        smilesQuery = smilesQuery.take(smilesQuery.length() - 1)
        
//         println smilesQuery
        def InChIKey = "Some InChIKey"
    
        def openBabelSmilesToInChIKeyService = new OpenBabelSmilesToInChIKeyService()
        
        InChIKey = openBabelSmilesToInChIKeyService.smilesToInChIKey(smilesQuery)
        
        println "now we use a service to find this here " + smilesQuery
        println "and get " + InChIKey 
        

    
        def currentCompound = Compound.findByInChIKey("${InChIKey}")
//         
        if(currentCompound != null){
//             println currentCompound.smiles
//             println currentCompound.inChIKey
//             println params
        
            render(view: "cycleCompounds", model: [maxCompoundID:maxCompoundID, currentCompound:currentCompound])
        }else{
            def previouslyfoundEntryStatus = "no structure matching SMILES found"
            render(view: "searchCompounds", model: [previouslyfoundEntryStatus:previouslyfoundEntryStatus, previousSmilesQuery:smilesQuery])
        }
        
        
	
	}
	
	def searchCompoundsByInChIKey() {

        def maxCompoundID = Compound.count()
        String feedback = "" + "currently there are ${maxCompoundID} entries in the database and we will look for compound with this InChIKey: ${params.InChIKey}"
        println feedback
        println params
  
        String InChIKeyQuery = "${params.InChIKey.drop(1)}"
        InChIKeyQuery = InChIKeyQuery.take(InChIKeyQuery.length() - 1)
        
        def currentCompound = Compound.findByInChIKey("${InChIKeyQuery}")
//         
        if(currentCompound != null){
//             println currentCompound.smiles
//             println currentCompound.inChIKey
//             println params
        
            render(view: "cycleCompounds", model: [maxCompoundID:maxCompoundID, currentCompound:currentCompound])
        }else{
             def previouslyfoundEntryStatus = "no structure matching InChIKey found"
            render(view: "searchCompounds", model: [previouslyfoundEntryStatus:previouslyfoundEntryStatus, previousInChIKeyQuery:InChIKeyQuery])
        }
        
        
	
	}
	
	
	def cycleCompounds(){
        def maxCompoundID = Compound.count()
        String feedback = "" + "currently there are ${maxCompoundID} entries in the database"
        println feedback
        
        params.id = (params.id == null)?1:params.id
        // if it is to big set it back to first entry
        if(Integer.valueOf(params.id) > maxCompoundID | Integer.valueOf(params.id) <= 0){
            params.id = 1
        }
        
        def currentCompoundID = params.id
    
        
    
        def currentCompound = Compound.get(currentCompoundID)
        
        
        println currentCompound.smiles
        println params
        
        [maxCompoundID:maxCompoundID, currentCompound:currentCompound]
	}

	
	def cycleCompoundsKekuleStyle(){
        def maxCompoundID = Compound.count()
        String feedback = "" + "currently there are ${maxCompoundID} entries in the database"
        println feedback
        
        params.id = (params.id == null)?1:params.id
        // if it is to big set it back to first entry
        if(Integer.valueOf(params.id) > maxCompoundID | Integer.valueOf(params.id) <= 0){
            params.id = 1
        }
        
        def currentCompoundID = params.id
    
    
        def currentCompound = Compound.get(currentCompoundID)
        
        
        println currentCompound.smiles
        println params
        
        [maxCompoundID:maxCompoundID, currentCompound:currentCompound]
	}
	
	
    def goToCompoundWithID(){
        def maxCompoundID = Compound.count()
        println "" + params.compoundID + " out of " + maxCompoundID
//         println compoundIDForCreatedRecord
        def currentCompound = Compound.get(params.compoundID)
        
        
        println currentCompound.smiles
        println params
        
        chain(controller: "Compound", action:"cycleCompounds", model: [maxCompoundID:maxCompoundID, currentCompound:currentCompound], , params: [id:params.compoundID])
	}
	
	def showFullSet () {
		// render the full list of all structures with additional info coming from pubmed (the picture of the comcound is cross loaded from pubchem)
		
		// get the Compound List
		def allCompounds = Compound.getAll()
		def maxCompoundID = Compound.count()
		render(view: "showFullSet", model: [maxCompoundID:maxCompoundID, allCompounds:allCompounds])
	}

	
	def showFullSetWithRIFDigest () {
		// render the full list of all structures with additional info coming from pubmed (the picture of the comcound is cross loaded from pubchem)
		
		// get the Compound List
		def allCompounds = Compound.getAll()
		def maxCompoundID = Compound.count()
		render(view: "showFullSetWithRIFDigest", model: [maxCompoundID:maxCompoundID, allCompounds:allCompounds])
	}
	
	def showFullSetWithRIFDigestWithKekuleRenderer () {
		// render the full list of all structures with additional info coming from pubmed (the picture of the comcound is cross loaded from pubchem)
		
		// get the Compound List
		def allCompounds = Compound.getAll()
		def maxCompoundID = Compound.count()
		render(view: "showFullSetWithRIFDigestWithKekuleRenderer", model: [maxCompoundID:maxCompoundID, allCompounds:allCompounds])
	}
	

	def showFullSetChemSpace (){
	
		def allChemicalDescriptors = ChemicalDescriptors.getAll()
		def allBiologicalDescriptors = BiologicalDescriptors.getAll()
		
		// get the Compound List
		def allCompounds = Compound.getAll()
		def maxCompoundID = Compound.count()
		render(view: "showFullSetChemSpace", model: [maxCompoundID:maxCompoundID, allCompounds:allCompounds, allChemicalDescriptors:allChemicalDescriptors, allBiologicalDescriptors:allBiologicalDescriptors])	
	}

	
	def showFullSetChemSpaceCompoundOverlapsBetweenPublications (){
	
		def allChemicalDescriptors = ChemicalDescriptors.getAll()
		def allBiologicalDescriptors = BiologicalDescriptors.getAll()
		
		// get the Compound List
		def allCompounds = Compound.getAll()
		def maxCompoundID = Compound.count()
		render(view: "showFullSetChemSpaceCompoundOverlapsBetweenPublications", model: [maxCompoundID:maxCompoundID, allCompounds:allCompounds, allChemicalDescriptors:allChemicalDescriptors, allBiologicalDescriptors:allBiologicalDescriptors])	
	}
	
	def showFullSetChemSpaceCFTRRelevance (){
	
		def allChemicalDescriptors = ChemicalDescriptors.getAll()
		def allBiologicalDescriptors = BiologicalDescriptors.getAll()
		
		// get the Compound List
		def allCompounds = Compound.getAll()
		def maxCompoundID = Compound.count()
		render(view: "showFullSetChemSpaceCFTRRelevance", model: [maxCompoundID:maxCompoundID, allCompounds:allCompounds, allChemicalDescriptors:allChemicalDescriptors, allBiologicalDescriptors:allBiologicalDescriptors])	
	}
	
	
	
	
	
    def showFullSetChemSpaceInfluenceOnCFTRFunction (){
	
		def allChemicalDescriptors = ChemicalDescriptors.getAll()
		def allBiologicalDescriptors = BiologicalDescriptors.getAll()
		
		// get the Compound List
		def allCompounds = Compound.getAll()
		def maxCompoundID = Compound.count()
		render(view: "showFullSetChemSpaceInfluenceOnCFTRFunction", model: [maxCompoundID:maxCompoundID, allCompounds:allCompounds, allChemicalDescriptors:allChemicalDescriptors, allBiologicalDescriptors:allBiologicalDescriptors])	
	}
	
    def showFullSetChemSpaceOrderOfInteraction (){
	
		def allChemicalDescriptors = ChemicalDescriptors.getAll()
		def allBiologicalDescriptors = BiologicalDescriptors.getAll()
		
		// get the Compound List
		def allCompounds = Compound.getAll()
		def maxCompoundID = Compound.count()
		render(view: "showFullSetChemSpaceOrderOfInteraction", model: [maxCompoundID:maxCompoundID, allCompounds:allCompounds, allChemicalDescriptors:allChemicalDescriptors, allBiologicalDescriptors:allBiologicalDescriptors])	
	}
	
	def showFullSetChemSpaceSubcellularCompartment (){
	
		def allChemicalDescriptors = ChemicalDescriptors.getAll()
		def allBiologicalDescriptors = BiologicalDescriptors.getAll()
		
		// get the Compound List
		def allCompounds = Compound.getAll()
		def maxCompoundID = Compound.count()
		render(view: "showFullSetChemSpaceSubcellularCompartment", model: [maxCompoundID:maxCompoundID, allCompounds:allCompounds, allChemicalDescriptors:allChemicalDescriptors, allBiologicalDescriptors:allBiologicalDescriptors])	
	}
	
	
	def testShow() {
		// let us show a single compound for testing the view building
		Compound compound1 = new Compound('id': 1, 'pubChemId': 2943, smiles: 'ClC1=C(C(OC([H])([H])[H])=O)C(Cl)=C(C(C(OC([H])([H])[H])=O)=C1Cl)Cl')

		compound1.save(flush:true)

		Compound compound2 = new Compound('pubChemId': 2944, smiles: 'ClC1=C(C(OC([H])([H])[H])=O)C(Cl)=C(C(C(OC([H])([H])[H])=O)=C1Cl)Cl')

		compound2.save()
		println Compound.count()


        [ compound1:compound1, compound2:compound2 ]
			
	}

def testShowJsMol() {
		// let us show a single compound for testing the view building
		Compound compound1 = new Compound('id': 1, 'pubChemId': 2943, smiles: 'ClC1=C(C(OC([H])([H])[H])=O)C(Cl)=C(C(C(OC([H])([H])[H])=O)=C1Cl)Cl')

		compound1.save()

		Compound compound2 = new Compound('pubChemId': 2944, smiles: 'ClC1=C(C(OC([H])([H])[H])=O)C(Cl)=C(C(C(OC([H])([H])[H])=O)=C1Cl)Cl')

		compound2.save()


        [ compound1:compound1, compound2:compound2 ]
			
	}
	
    // below are the parts required for compound creation: we will need a viewer where we can enter new compounds like with the search field AND we will need a controller to be called doing the saving part
    def createCompoundEntry() {
		
		
		
		
		def maxCompoundID = Compound.count()
        String feedback = "" + "currently there are ${maxCompoundID} entries in the database"
        println feedback
        
        params.smiles = (params.smiles == null)?"smiles not specified":params.smiles
        
        params.InChIKey = (params.InChIKey == null)?"InChiKey not specified":params.InChIKey
        
        params.id = (params.id == null)?0:params.id
        
        
        // if it is to big set it back to first entry
        if(Integer.valueOf(params.id) > maxCompoundID | Integer.valueOf(params.id) <= 0){
            params.id = 1
        }
        
        def currentCompoundID = params.id
        
        println Compound.list()
    
        
    
        def currentCompound = new Compound('id': params.id, 'inChIKey':params.InChIKey, smiles: params.smiles)
        
        
        println currentCompound.smiles
        println params
        
        [maxCompoundID:maxCompoundID, currentCompound:currentCompound]
		
		
	} 
	
    def defineCompoundEntry() {
		// let us show a single compound for testing the view building
		
			
	}

	def saveCompoundBySMILES() {

        def maxCompoundID = Compound.count()
        String feedback = "" + "currently there are ${maxCompoundID} entries in the database and we will look for compound with this SMILES: ${params.smiles}"
        println feedback
        
        if(params.synonym == "\"\""){
            params.synonym = null
        }
        
        println "synonym was " + params.synonym
        
        String smilesQuery = "${params.smiles.drop(1)}"
        smilesQuery = smilesQuery.take(smilesQuery.length() - 1)
        
//         println smilesQuery
        def InChIKey = "Some InChIKey"
    
        def openBabelSmilesToInChIKeyService = new OpenBabelSmilesToInChIKeyService()
        
        InChIKey = openBabelSmilesToInChIKeyService.smilesToInChIKey(smilesQuery)
        
        println "now we use a service to find this here " + smilesQuery
        println "and get " + InChIKey 
        

    
        def currentCompound = Compound.findByInChIKey("${InChIKey}")
//         
        if(currentCompound != null){
//             println currentCompound.smiles
//             println currentCompound.inChIKey
//             println params
        
            render(view: "cycleCompounds", model: [maxCompoundID:maxCompoundID, currentCompound:currentCompound])
        }else{
            //create the new entry
            // requires a new Compound
            Compound compoundToBeCreated = new Compound('smiles':smilesQuery, 'inChIKey':InChIKey)
            
            
            // in case that a synonym was provided store it in the synonym list for that compound
            if(params.synonym){
                String synonymQuery = "${params.synonym.drop(1)}"
                synonymQuery = synonymQuery.take(synonymQuery.length() - 1)
                
                compoundToBeCreated.addToCompoundSynonym('synonym':"${synonymQuery}")
            
            }
            
            
            
            def synonyms = []
    
            PubChemQueryService pubChemQueryService = new PubChemQueryService()

            def cid = -1
        
            def lookUpCID
    
    
            def cids = []
    
            // for the new entry get the InChIKey and retrieve CID and if CID was found list of synonymes
            
            synonyms = null
            // check if there can be a CID found using the InChIKey
            cid = pubChemQueryService.lookUpCID(compoundToBeCreated.inChIKey) 
        
            if(cid){
            // get the CID if it exist we can save this information and look for synonyms
                println "the CID @ PubChem is " + cid
                PubChemRecord currentPubChemRecord = new PubChemRecord('cid':cid)
                // getting the synonyms
                def pubChemSynonymsJSON = pubChemQueryService.lookUpSynonyms(compoundToBeCreated.inChIKey)
                    
            //     println pubChemSynonymsJSON.InformationList.Information.Synonym[0][0]
                
                if(pubChemSynonymsJSON){
                    synonyms = pubChemSynonymsJSON.InformationList.Information.Synonym[0]
                    synonyms.each {
                        println "a.k.a ${it}"
                        currentPubChemRecord.addToSynonyms('synonym':"${it}")
                        // add it also to generall synonym list
                        
                        compoundToBeCreated.addToCompoundSynonym('synonym':"${it}")
                        
                    }
                
                }
                def myID = currentPubChemRecord.save(flush:true)    
                println "saved PubChem Entry as " + myID
            
                def attachThisPubChemInfo = PubChemRecord.findByCid(cid)
                println "this is the CID recovered " + attachThisPubChemInfo.cid + " @position " + attachThisPubChemInfo.id
                // save the info to the pubchem record attaching it to the
                compoundToBeCreated.setPubChem(attachThisPubChemInfo)
                    
                    
            }else{
                    println "no pubchem entry found"
            }
            
            compoundToBeCreated.save(flush:true)
            
            // now point to the resulting compound page which was just created
            def compoundIDForCreatedRecord = compoundToBeCreated.id
            println "we just created record #" + compoundIDForCreatedRecord
//             render "no match found, so we created this entry in our database"
            redirect(controller: "compound", action: "goToCompoundWithID", params: [compoundID:compoundIDForCreatedRecord])
        }
	
	}
	
	
	def saveCompoundUsingInChIKey() {

        def maxCompoundID = Compound.count()
        String feedback = "" + "currently there are ${maxCompoundID} entries in the database and we will look for compound with this PubChem InChIKey: ${params.InChIKey}"
        println feedback
        
        
        String inChIKeyQuery = "${params.InChIKey}".drop(1)
        inChIKeyQuery = inChIKeyQuery.take(inChIKeyQuery.length() - 1)
        
        PubChemQueryService pubChemQueryService = new PubChemQueryService()
        def cid = pubChemQueryService.lookUpCID(inChIKeyQuery)
        
        if(params.synonym == "\"\""){
            params.synonym = null
        }
        
        
//         println smilesQuery
        def InChIKey = inChIKeyQuery
    
        def smilesFromPubChem = pubChemQueryService.lookUpIsomericSMILESByCID(Integer.valueOf(cid))
        
        println "now we use a service to find this here " + smilesFromPubChem
        println "and get " + InChIKey 
        

    
        def currentCompound = Compound.findByInChIKey("${InChIKey}")
//         
        if(currentCompound != null){
//             println currentCompound.smiles
//             println currentCompound.inChIKey
//             println params
        
            render(view: "cycleCompounds", model: [maxCompoundID:maxCompoundID, currentCompound:currentCompound])
        }else{
            //create the new entry
            // requires a new Compound
            Compound compoundToBeCreated = new Compound('smiles':smilesFromPubChem, 'inChIKey':InChIKey)
            
            
            // in case that a synonym was provided store it in the synonym list for that compound
            if(params.synonym){
                String synonymQuery = "${params.synonym.drop(1)}"
                synonymQuery = synonymQuery.take(synonymQuery.length() - 1)
                compoundToBeCreated.addToCompoundSynonym('synonym':"${synonymQuery}")
            
            }
            
            
            def synonyms = []
    
        
            def lookUpCID
    
    
            def cids = []
    
            // for the new entry get the InChIKey and retrieve CID and if CID was found list of synonymes
            
            synonyms = null
            // check if there can be a CID found using the InChIKey
            cid = pubChemQueryService.lookUpCID(compoundToBeCreated.inChIKey) 
        
            if(cid){
            // get the CID if it exist we can save this information and look for synonyms
                println "the CID @ PubChem is " + cid
                PubChemRecord currentPubChemRecord = new PubChemRecord('cid':cid)
                // getting the synonyms
                def pubChemSynonymsJSON = pubChemQueryService.lookUpSynonyms(compoundToBeCreated.inChIKey)
                    
            //     println pubChemSynonymsJSON.InformationList.Information.Synonym[0][0]
                
                if(pubChemSynonymsJSON){
                    synonyms = pubChemSynonymsJSON.InformationList.Information.Synonym[0]
                    synonyms.each {
                        println "a.k.a ${it}"
                        currentPubChemRecord.addToSynonyms('synonym':"${it}")
                        // add it also to generall synonym list
                        compoundToBeCreated.addToCompoundSynonym('synonym':"${it}")
                    }
                
                }
                def myID = currentPubChemRecord.save(flush:true)    
                println "saved PubChem Entry as " + myID
            
                def attachThisPubChemInfo = PubChemRecord.findByCid(cid)
                println "this is the CID recovered " + attachThisPubChemInfo.cid + " @position " + attachThisPubChemInfo.id
                // save the info to the pubchem record attaching it to the
                compoundToBeCreated.setPubChem(attachThisPubChemInfo)
                    
                    
            }else{
                    println "no pubchem entry found"
            }
        
    
            
            
            
            compoundToBeCreated.save(flush:true)
            
            // now point to the resulting compound page which was just created
            int compoundIDForCreatedRecord = compoundToBeCreated.id
            println "we just created record #" + compoundIDForCreatedRecord
//             render "no match found, so we created this entry in our database"
            redirect(controller: "compound", action: "goToCompoundWithID", params: [compoundID:compoundIDForCreatedRecord])
        }
        
	}
	
	
	
    def saveCompoundUsingCID() {

        def maxCompoundID = Compound.count()
        String feedback = "" + "currently there are ${maxCompoundID} entries in the database and we will look for compound with this PubChem CID: ${params.cid}"
        println feedback
        
        if(params.synonym == "\"\""){
            params.synonym = null
        }
        
        
        String cidQuery = "${params.cid.drop(1)}"
        cidQuery = cidQuery.take(cidQuery.length() - 1)
        
        PubChemQueryService pubChemQueryService = new PubChemQueryService()
        def smilesFromPubChem = pubChemQueryService.lookUpIsomericSMILESByCID(Integer.valueOf(cidQuery))
        
        
        
//         println smilesQuery
        def InChIKey = "Some InChIKey"
    
        def openBabelSmilesToInChIKeyService = new OpenBabelSmilesToInChIKeyService()
        
        InChIKey = openBabelSmilesToInChIKeyService.smilesToInChIKey(smilesFromPubChem)
        
        println "now we use a service to find this here " + smilesFromPubChem
        println "and get " + InChIKey 
        

    
        def currentCompound = Compound.findByInChIKey("${InChIKey}")
//         
        if(currentCompound != null){
//             println currentCompound.smiles
//             println currentCompound.inChIKey
//             println params
        
            render(view: "cycleCompounds", model: [maxCompoundID:maxCompoundID, currentCompound:currentCompound])
        }else{
            //create the new entry
            // requires a new Compound
            Compound compoundToBeCreated = new Compound('smiles':smilesFromPubChem, 'inChIKey':InChIKey)
            
            // in case that a synonym was provided store it in the synonym list for that compound
            if(params.synonym){
                String synonymQuery = "${params.synonym.drop(1)}"
                synonymQuery = synonymQuery.take(synonymQuery.length() - 1)
                compoundToBeCreated.addToCompoundSynonym('synonym':"${synonymQuery}")
            
            }
            
            
            def synonyms = []
    

            def cid = -1
        
            def lookUpCID
    
    
            def cids = []
    
            // for the new entry get the InChIKey and retrieve CID and if CID was found list of synonymes
            
            synonyms = null
            // check if there can be a CID found using the InChIKey
            cid = pubChemQueryService.lookUpCID(compoundToBeCreated.inChIKey) 
        
            if(cid){
            // get the CID if it exist we can save this information and look for synonyms
                println "the CID @ PubChem is " + cid
                PubChemRecord currentPubChemRecord = new PubChemRecord('cid':cid)
                // getting the synonyms
                def pubChemSynonymsJSON = pubChemQueryService.lookUpSynonyms(compoundToBeCreated.inChIKey)
                    
            //     println pubChemSynonymsJSON.InformationList.Information.Synonym[0][0]
                
                if(pubChemSynonymsJSON){
                    synonyms = pubChemSynonymsJSON.InformationList.Information.Synonym[0]
                    synonyms.each {
                        println "a.k.a ${it}"
                        currentPubChemRecord.addToSynonyms('synonym':"${it}")
                        // add it also to generall synonym list
                        compoundToBeCreated.addToCompoundSynonym('synonym':"${it}")
                    }
                
                }
                def myID = currentPubChemRecord.save(flush:true)    
                println "saved PubChem Entry as " + myID
            
                def attachThisPubChemInfo = PubChemRecord.findByCid(cid)
                println "this is the CID recovered " + attachThisPubChemInfo.cid + " @position " + attachThisPubChemInfo.id
                // save the info to the pubchem record attaching it to the
                compoundToBeCreated.setPubChem(attachThisPubChemInfo)
                    
                    
            }else{
                    println "no pubchem entry found"
            }
        
    
            
            
            
            compoundToBeCreated.save(flush:true)
            
            // now point to the resulting compound page which was just created
            int compoundIDForCreatedRecord = compoundToBeCreated.id
            println "we just created record #" + compoundIDForCreatedRecord
//             render "no match found, so we created this entry in our database"
            redirect(controller: "compound", action: "goToCompoundWithID", params: [compoundID:compoundIDForCreatedRecord])
        }
        
	}
}
