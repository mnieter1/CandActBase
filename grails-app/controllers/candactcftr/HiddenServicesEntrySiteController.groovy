package candactcftr

class HiddenServicesEntrySiteController {

    def index() { }
    
    def checkingOutChartViszualizations() {
        render(view: "checkingOutChartViszualizations")
    }
    
     def checkingOutChartViszualizations_usingECharts() {
        render(view: "checkingOutChartViszualizations_usingECharts")
    }
}
