package candactcftr

import groovy.json.JsonSlurper
import grails.converters.*

class InitializeZoteroReferenceImportController {

    def index() {
    
    File f = new File("grails-app/assets/json/items_top_v1.csljson")
	def slurper = new JsonSlurper()
	def jsonText = f.getText()
	def myJson = slurper.parseText( jsonText )
	//render myJson

	URL apiUrl = new URL("https://api.zotero.org/groups/1632179/items/top?v=1&format=csljson&limit=100")
	myJson = slurper.parse( apiUrl )
	
	
	def subjson = myJson.items

	//render subjson.SMILES

	// for each compound save it to the database
	//def json = '[{"name":"bla","alter":10},{"name":"blubb","alter":22}]'
 	//def slurper = new groovy.json.JsonSlurper()
 	//def result = slurper.parseText(json)
 
	def counter = 0
    def PMID = null
	subjson.each(){chunkreference-> 
		counter += 1
		println chunkreference
		println ""
		println "just the DOI is: " + chunkreference['DOI']
		println "just the title is: " + chunkreference['title']
		println "just the type is: " + chunkreference['type']
		println "just the abstract is: " + chunkreference['abstract']
		println "just the author is: " + chunkreference['author']
		println "just the URL is: " + chunkreference['URL']
		println "just the container-title is: " + chunkreference['container-title']
		println "just the page is: " + chunkreference['page']
		println "just the volume is: " + chunkreference['volume']
		println "just the issue is: " + chunkreference['issue']
		println "just the PMID is: " + chunkreference['note']
		def PMIDNote = chunkreference['note']
 		if(PMIDNote != null){
            println PMIDNote.drop(6)
            PMID = PMIDNote.drop(6)}
		
		// retrieve all the authors and print them one by one
		def htmlchunk = ""
		chunkreference['author'].each(){chunkAuthor-> 
            println "" + chunkAuthor.given + " " + chunkAuthor.family
            htmlchunk += "" + chunkAuthor.given + " " + chunkAuthor.family + "</br>"
		}
        //render htmlchunk
        
        
        // create the authors one by one and add the entries to the references
        def authorsIdList = []
        def authorsIdCounter = 0
        
        chunkreference['author'].each(){chunkAuthor-> 
            Author author = new Author('given': chunkAuthor['given'], 'family': chunkAuthor['family'])
            // only save if there was not a similar entry before
            if(Author.findByFamilyAndGiven(author.family, author.given)){
                author.id = Author.findByFamilyAndGiven(author.family, author.given).id
            }else{
                author.save(flush:true)
            }
            authorsIdList[authorsIdCounter] = author.id
            authorsIdCounter += 1
		}

		println authorsIdList
        
        
        
        CitationReference publication = new CitationReference('refTitle': chunkreference['title'],
                                                                'doi': chunkreference['DOI'],
                                                                'type': chunkreference['type'],
                                                                'refAbstract': chunkreference['abstract'],
                                                                'refURL': chunkreference['URL'],
                                                                'containerTitle': chunkreference['container-title'],
                                                                'pageString': chunkreference['page'],
                                                                'volume': chunkreference['volume'],
                                                                'issue': chunkreference['issue'],

//                                                                 'pmid' : PMID,
                                                                )
                            // adding all the author links
                            authorsIdList.each(){
                            
                                publication.addToAuthors(Author.get(it))
                            }
           
            println "current document entry is nr " + counter
			
		// check if this is already in the database if so DO NOT SAVE AGAIN
		if(CitationReference.findByRefTitle(publication['refTitle'])){
                publication.id = CitationReference.findByRefTitle(publication['refTitle']).id
            }else{
                // no document with the same title so assume save to save
                publication.save(flush:true)
            }
        println "current publication database entry is nr " + publication.id
		}
	
	[myJson:myJson]
    
    
    }
}
