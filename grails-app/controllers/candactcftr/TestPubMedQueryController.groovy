package candactcftr



class TestPubMedQueryController {

    def index() {
    
    PubMedQueryService pubMedQueryService = new PubMedQueryService()
    
    def pmid = -1

    // get all of the compound list
    def allCitationReference = CitationReference.getAll()
    
    def whichOnesCouldNotBefound = ["those were missing"]
    
    // for each entry get the InChIKey and retrieve CID and if CID was found list of synonymes
    allCitationReference.each(){oneCitationReference ->
        

        // check if there can be a CID found using the InChIKey
        pmid = pubMedQueryService.lookUpPMID(oneCitationReference.refTitle) 
    
        if(pmid){
        // get the CID if it exist we can save this information and look for synonyms
            println "the PMID @ PubMed is " + pmid
//             PubChemRecord currentPubChemRecord = new PubChemRecord('cid':cid)
//             // getting the synonyms
//             def pubChemSynonymsJSON = pubMedQueryService.lookUpSynonyms(oneCitationReference.inChIKey)
//                 
// 
// 
//             def myID = currentPubChemRecord.save(flush:true)    
//             println "saved PubChem Entry as " + myID
//         
//             def attachThisPubChemInfo = PubChemRecord.findByCid(cid)
//             println "this is the CID recovered " + attachThisPubChemInfo.cid + " @position " + attachThisPubChemInfo.id
//             // save the info to the pubchem record attaching it to the
//             oneCompound.setPubChem(attachThisPubChemInfo)
//             oneCompound.save(flush:true)
                
                
            }else{
                println "no entry found"
                // adding to the not matching list
                whichOnesCouldNotBefound += oneCitationReference.refTitle
            }
    }
    
    whichOnesCouldNotBefound.each(){
        println it
    }
	[pmid:pmid]
    
    }
    
    
    def updatePMIDsBasedOnTitleQueryingPubMed() {
    
    PubMedQueryService pubMedQueryService = new PubMedQueryService()
    
    def pmid = -1

    // get all of the compound list
    def allCitationReference = CitationReference.getAll()
    
    def whichOnesCouldNotBefound = ["those were missing"]
    
    // for each entry get the InChIKey and retrieve CID and if CID was found list of synonymes
    allCitationReference.each(){oneCitationReference ->
        

        // check if there can be a CID found using the InChIKey
        pmid = pubMedQueryService.lookUpPMID(oneCitationReference.refTitle) 
    
        if(pmid){
        // get the CID if it exist we can save this information and look for synonyms
            println "the PMID @ PubMed is " + pmid
//             PubChemRecord currentPubChemRecord = new PubChemRecord('cid':cid)
//             // getting the synonyms
//             def pubChemSynonymsJSON = pubMedQueryService.lookUpSynonyms(oneCitationReference.inChIKey)
//                 
// 
// 
//             def myID = currentPubChemRecord.save(flush:true)    
//             println "saved PubChem Entry as " + myID
//         
//             def attachThisPubChemInfo = PubChemRecord.findByCid(cid)
//             println "this is the CID recovered " + attachThisPubChemInfo.cid + " @position " + attachThisPubChemInfo.id
//             // save the info to the pubchem record attaching it to the
//             oneCompound.setPubChem(attachThisPubChemInfo)
//             oneCompound.save(flush:true)
                oneCitationReference.pmid = pmid
                oneCitationReference.save(flush:true)
                
            }else{
                println "no entry found"
                // adding to the not matching list
                whichOnesCouldNotBefound += oneCitationReference.refTitle
            }
    }
    
    whichOnesCouldNotBefound.each(){
        println it
    }
	render(view: "index", model: [whichOnesCouldNotBefound:whichOnesCouldNotBefound])
    
    }
    
    
}
