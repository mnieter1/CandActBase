package candactcftr

import groovy.json.JsonSlurper
// import grails.converters.*

class InitializeReferenceIntoFunctionController {

    def index() {
    
//         loading some reference into function
        ReferenceIntoFunction aRIF = new ReferenceIntoFunction('substanceReferenceIntoFunction': "all possible combinations of VX-770, genistein and curcumin synergistically repaired CFTR-dependent forskolin-induced swelling of organoids with CFTR-S1251N or CFTR-G551D")
        
//         aRIF.addToCompound(Author.get(it))
        
        aRIF.save()
        
        
        aRIF = new ReferenceIntoFunction('substanceReferenceIntoFunction': "genistein and curcumin also enhanced forskolin-induced swelling of F508del homozygous organoids that were treated with VX-770 and the prototypical CFTR corrector VX-809")

        aRIF.save()

        
        
        aRIF = new ReferenceIntoFunction('substanceReferenceIntoFunction': "W1282X-CFTR channels were stimulated by two CFTR modulators, the FDA-approved VX-770 and the dietary compound curcumin")

        aRIF.save()
        

        aRIF = new ReferenceIntoFunction('substanceReferenceIntoFunction': "in vitro or in vivo treatment with curcumin, a natural phenolic compound, significantly enhanced CFTR expression and reversed the heat-induced increases in COX-2/PGE2/IL-8, neutrophil infiltration and tissue damage in the airway")

        aRIF.save()

        
        aRIF = new ReferenceIntoFunction('substanceReferenceIntoFunction': "Curcumin may potentiate CFTR activity not only by removing inhibitory Fe(3+) to release the R domain from ICL3 but also by stabilizing the stimulatory R-ICL1/ICL4 interactions")

        aRIF.save()


        aRIF = new ReferenceIntoFunction('substanceReferenceIntoFunction': "by focusing on neutrophils and CF airway epithelial cells, we identified curcumin as a potent inhibitor of TLR2-mediated inflammatory responses")

        aRIF.save()
        
        aRIF = new ReferenceIntoFunction('substanceReferenceIntoFunction': "colocalization of CFTR and K18 in the vicinity of the endoplasmic reticulum, although this is reversed by treating cells with curcumin, resulting in the rescue of F508del-CFTR")

        aRIF.save()
        
        
        
        render "done loading Reference Into Function"
    
    }
    
    
    
    def loadRIFFromJSONFILE(){
    	File f = new File("grails-app/assets/json/PMID_RIF_DOI.json")
        def slurper = new JsonSlurper()
        def jsonText = f.getText()
        def myJson = slurper.parseText( jsonText )
	
	
        def couldNotMatchThesePMIDs = []
        
        
        // for each RIF save it to the database
        myJson.each(){chunkcompound-> 
	
            // check the components of RIF annotation PMID RIF and DOI optional
        
            ReferenceIntoFunction aRIF = new ReferenceIntoFunction('substanceReferenceIntoFunction': chunkcompound.RIF)
            
    //         aRIF.addToCompound(Author.get(it))
            
            aRIF.save()
            
            def currentPMID = chunkcompound.PMID
            
            println "current PMID to look for is: " + currentPMID
            
            // check which citation reference belongs to this PMID
            CitationReference currentCitation = CitationReference.findByPmid(currentPMID)
            
            // check if something was found if so attach the RIF, otherwise skip and record the fail
            if(currentCitation){
            
                currentCitation.addToRifs(aRIF)
                currentCitation.save(flush:true)
                println chunkcompound.RIF
            }else{
                // failed to find match record te entry
                couldNotMatchThesePMIDs += currentPMID
            }
            
            

		}

        // show all the failed one
        println "could not match these PMIDs"
            couldNotMatchThesePMIDs.each(){
            println it
        }
        
		
        //println Compound.list()

        render "done loading Reference Into Function"// [myJson:myJson]
        

        // load in the zotero reference library


        // link the entries of compound list and literature reference list


	
    }
}
