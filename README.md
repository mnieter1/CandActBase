# CandActBase

This is the code base to setup a [**CandActCFTR**](http://candactcftr.ams.med.uni-goettingen.de/)**-like** Grails server, capable to handle literature and chemical structures.

It contains the necessary grails modules and is set to work directly with an in memory database. You can change this in the settings to use other database systems to store the information of the datasets, e.g. MariaDB.

We use Grails mainly to define the data models to be instantiated within the DB and to create some views to have direct access to overviews without involving external tools.
Mayor maintance processing, merging updloading is done with external tools like KNIME using the provided interfaces (either webinterface or direct DB-backend).

CandActBase is not specific to CFTR data, thus it can be repurposed for other use cases. Even replacing the compound centered design to an e.g. gene identifier centered version can be achieved fairly easy. We would be happy to assist in such repurposing endevours.
So get in contact if you are interested: [Group of Applied Bio(chem)informatics and Image Analysis](http://www.bioinf.med.uni-goettingen.de/research/cheminformaticsimaging/)

# [CandActCFTR](http://candactcftr.ams.med.uni-goettingen.de/)

[**CandActCFTR**](http://candactcftr.ams.med.uni-goettingen.de/) is a **curated compound database** which **annotates** the **chemical structure library** with information on **where and how** in the protein life cycle a **compound** likely **interacts**, thus comprising a good starting point for **modelling the disease** and enhancing ligand based approaches (e.g. via eu-openscreen). In the upcoming extension of [**CandActCFTR**](http://candactcftr.ams.med.uni-goettingen.de/), this ligand-based approach will be complemented by structure-based annotations, including the means to predict the interactions between [**CandActCFTR**](http://candactcftr.ams.med.uni-goettingen.de/) substances and CFTR by using existing molecular dynamics trajectories, and by adding more organisation and annotation modules. This approach will help ranking putative therapeutic substances according to their potential to bind CFTR. Moreover, it will be further extended by integrating results from high-throughput screens from collaborators, helping to rank putative therapeutic substances.

Furthermore [**CandActCFTR**](http://candactcftr.ams.med.uni-goettingen.de/) will use public gene expression data to assess transcriptome profiles of [**CandActCFTR**](http://candactcftr.ams.med.uni-goettingen.de/) substances and compare differentially expressed genes to gene sets with known relevance for CFTR function, helping to rank putative therapeutic substances according to their potential to modify the cellular transcriptome in favor of CFTR function via our Göttingen [institutes](http://www.bioinf.med.uni-goettingen.de/) curated TRANScription FACtor database - **TRANSFAC**.

# Citation
To cite our database please refer to our related publication:
Nietert M.; Vinhoven L.; Auer F.; Hafkemeyer S.; Stanke F.:<br>
<i>Comprehensive analysis of chemical structures that have been tested as CFTR activating substances in a publicly available database CandActCFTR</i><br>
Frontiers in Pharmacology,2021. <a style="color: #009900" href="https://www.frontiersin.org/articles/10.3389/fphar.2021.689205">https://www.frontiersin.org/articles/10.3389/fphar.2021.689205</a>
